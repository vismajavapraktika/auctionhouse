package com.visma.seli.service.auction;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static com.visma.seli.service.enums.AuctionCurrencyEnum.EUR;
import static com.visma.seli.service.enums.AuctionCurrencyEnum.KUDOS;
import static junit.framework.Assert.assertEquals;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(MockitoJUnitRunner.class)
public class AuctionsFilterServiceTest {

    @InjectMocks
    private AuctionsFilterService auctionsFilterService;

    @Before
    public void init() {
        initMocks(this);
    }

    @Test
    public void shouldReturnTrueIfAfterFilteringAuctionsAreOnlyEuros() throws Exception {
        // given
        List<AuctionBody> listOfAuctions = getListOfAuctionsWithDifferentCurrency("both");
        List<AuctionBody> resultList = auctionsFilterService.filterAuctions(listOfAuctions, "", getListOfCurrency("euro")/*, new PriceRange(1, 10)*/);
        // when
        List<AuctionBody> expectedResult = getListOfAuctionsWithDifferentCurrency("euro");
        // then
        assertEquals(expectedResult, resultList);
    }

    @Test
    public void shouldReturnTrueIfAfterFilteringAuctionsAreOnlyKudos() throws Exception {
        // given
        List<AuctionBody> listOfAuctions = getListOfAuctionsWithDifferentCurrency("both");
        List<AuctionBody> resultList = auctionsFilterService.filterAuctions(listOfAuctions,"", getListOfCurrency("kudos")/*, new PriceRange(1,10)*/);
        // when
        List<AuctionBody> expectedResult = getListOfAuctionsWithDifferentCurrency("kudos");
        // then
        assertEquals(expectedResult,resultList);
    }

    @Test
    public void shouldReturnTrueIfAfterFilteringAuctionsAreBothKudosAndEuros() throws Exception {
        // given
        List<AuctionBody> listOfAuctions = getListOfAuctionsWithDifferentCurrency("both");
        List<AuctionBody> resultList = auctionsFilterService.filterAuctions(listOfAuctions,"", getListOfCurrency("both")/*, new PriceRange(1,10)*/);
        // when
        List<AuctionBody> expectedResult = getListOfAuctionsWithDifferentCurrency("both in line");
        // then
        assertEquals(expectedResult,resultList);
    }

    private List<AuctionBody> getListOfAuctionsWithDifferentCurrency(String currency) {
        List<AuctionBody> listOfAuctions = new ArrayList<>();
        AuctionBody auction = new AuctionBody(), auctionTwo = new AuctionBody(), auctionThree = new AuctionBody(), auctionFour = new AuctionBody();
        switch (currency) {
            case "both":
                auction.setTitle("First auction");
                auction.setCurrency(EUR);
                auction.setStartingPrice(BigDecimal.valueOf(10));
                auction.setOwnerId(1);
                auctionTwo.setTitle("Second auction");
                auctionTwo.setCurrency(KUDOS);
                auctionTwo.setStartingPrice(BigDecimal.valueOf(5));
                auctionTwo.setOwnerId(1);
                auctionThree.setTitle("Third auction");
                auctionThree.setCurrency(EUR);
                auctionThree.setStartingPrice(BigDecimal.valueOf(1));
                auctionThree.setOwnerId(1);
                auctionFour.setTitle("Fourth auction");
                auctionFour.setCurrency(KUDOS);
                auctionFour.setStartingPrice(BigDecimal.valueOf(2));
                auctionFour.setOwnerId(1);
                listOfAuctions.add(auction);
                listOfAuctions.add(auctionTwo);
                listOfAuctions.add(auctionThree);
                listOfAuctions.add(auctionFour);
                break;
            case "euro":
                auction.setTitle("First auction");
                auction.setCurrency(EUR);
                auction.setStartingPrice(BigDecimal.valueOf(10));
                auction.setOwnerId(1);
                auctionThree.setTitle("Third auction");
                auctionThree.setCurrency(EUR);
                auctionThree.setStartingPrice(BigDecimal.valueOf(1));
                auctionThree.setOwnerId(1);
                listOfAuctions.add(auction);
                listOfAuctions.add(auctionThree);
                break;
            case "kudos":
                auctionTwo.setTitle("Second auction");
                auctionTwo.setCurrency(KUDOS);
                auctionTwo.setStartingPrice(BigDecimal.valueOf(5));
                auctionTwo.setOwnerId(1);
                auctionFour.setTitle("Fourth auction");
                auctionFour.setCurrency(KUDOS);
                auctionFour.setStartingPrice(BigDecimal.valueOf(2));
                auctionFour.setOwnerId(1);
                listOfAuctions.add(auctionTwo);
                listOfAuctions.add(auctionFour);
                break;
            case "both in line":
                auction.setTitle("First auction");
                auction.setCurrency(EUR);
                auction.setStartingPrice(BigDecimal.valueOf(10));
                auction.setOwnerId(1);
                auctionTwo.setTitle("Second auction");
                auctionTwo.setCurrency(KUDOS);
                auctionTwo.setStartingPrice(BigDecimal.valueOf(5));
                auctionTwo.setOwnerId(1);
                auctionThree.setTitle("Third auction");
                auctionThree.setCurrency(EUR);
                auctionThree.setStartingPrice(BigDecimal.valueOf(1));
                auctionThree.setOwnerId(1);
                auctionFour.setTitle("Fourth auction");
                auctionFour.setCurrency(KUDOS);
                auctionFour.setStartingPrice(BigDecimal.valueOf(2));
                auctionFour.setOwnerId(1);
                listOfAuctions.add(auctionTwo);
                listOfAuctions.add(auctionFour);
                listOfAuctions.add(auction);
                listOfAuctions.add(auctionThree);
                break;
        }
        return listOfAuctions;
    }

    private List<String> getListOfCurrency(String currency) {
        List<String> listOfCurrency = new ArrayList<>();
        switch (currency) {
            case "both": {
                String currencyKudos = String.valueOf(KUDOS);
                String currencyEuro = String.valueOf(EUR);
                listOfCurrency.add(currencyKudos);
                listOfCurrency.add(currencyEuro);
                break;
            }
            case "euro": {
                String currencyEuro = String.valueOf(EUR);
                listOfCurrency.add(currencyEuro);
                break;
            }
            case "kudos": {
                String currencyKudos = String.valueOf(KUDOS);
                listOfCurrency.add(currencyKudos);
                break;
            }
        }
        return listOfCurrency;
    }
}