package com.visma.seli.service.auction;

import com.visma.seli.server.data.access.repository.database.user.logedInUser.UserRepository;
import com.visma.seli.service.enums.AuctionCurrencyEnum;
import com.visma.seli.service.enums.AuctionTypeEnum;
import com.visma.seli.service.user.UserBody;
import com.visma.seli.service.user.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(MockitoJUnitRunner.class)
public class AuctionFilterServiceTest {

    @InjectMocks
    private AuctionsFilterService auctionsFilterService;

    @Mock
    private UserRepository userRepository;

    public List<AuctionBody> listAuctions;
    public List<AuctionBody> filteredList;


    @Before
    public void init(){
        initMocks(this);

        UserBody userBody1 = new UserBody(1, "name", "surName", "email", "phone", "password");
        UserBody userBody2 = new UserBody(2, "name2", "surName2", "email", "phone", "password");
        UserBody userBody3 = new UserBody(3, "name", "surName", "email", "phone", "password");

        AuctionBody auction1 = getAuction(userBody1.getId(), AuctionTypeEnum.PRIVATE, BigDecimal.TEN, AuctionCurrencyEnum.EUR);
        AuctionBody auction2 = getAuction(userBody2.getId(), AuctionTypeEnum.PRIVATE, new BigDecimal(100), AuctionCurrencyEnum.EUR);
        AuctionBody auction3 = getAuction(userBody3.getId(), AuctionTypeEnum.PRIVATE, BigDecimal.TEN, AuctionCurrencyEnum.EUR);

        listAuctions = new ArrayList<AuctionBody>();
        listAuctions.add(auction1);
        listAuctions.add(auction2);
        listAuctions.add(auction3);

        when(userRepository.getUserById(1)).thenReturn(userBody1);
        when(userRepository.getUserById(2)).thenReturn(userBody2);
        when(userRepository.getUserById(3)).thenReturn(userBody3);

        auctionsFilterService.setUserService(new UserService());
    }

    private AuctionBody getAuction(int ownerId, AuctionTypeEnum type, BigDecimal startingPrice, AuctionCurrencyEnum currency) {
        AuctionBody auction = new AuctionBody();
        auction.setOwnerId(ownerId);
        auction.setType(type);
        auction.setStartingPrice(startingPrice);
        auction.setCurrency(currency);
        return auction;
    }

    @Test
    public void filterAuctionsByPriceTest() {
        filteredList = auctionsFilterService.filterAuctions(listAuctions, "", Arrays.asList("EUR")/*, new PriceRange(10, 100)*/);
        assertEquals(3, filteredList.size());
    }

    @Test
    public void filterAuctionsByAuctionOwnerNameAndSurnameTest() {
        filteredList = auctionsFilterService.filterAuctions(listAuctions, "name2 surName2", Arrays.asList("EUR")/*, new PriceRange(10, 100)*/);
        assertEquals(1, filteredList.size());
    }

    @Test
    public void filterAuctionsByCurrencyTest6() {
        filteredList = auctionsFilterService.filterAuctions(listAuctions, "", Arrays.asList("KUDOS")/*, new PriceRange(10, 100)*/);
        assertEquals(0, filteredList.size());
    }

}
