package com.visma.seli.service.auction;

import com.visma.seli.service.enums.SortingEnum;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(MockitoJUnitRunner.class)
public class SortingServiceTest {

    private SortingService sortingService = new SortingService();

    public List<AuctionBody> listAuctions;

    @Before
    public void init(){
        initMocks(this);

        AuctionBody auction1 = getAuction("A", new BigDecimal(100));
        AuctionBody auction2 = getAuction("a", new BigDecimal(50));
        AuctionBody auction3 = getAuction("c", new BigDecimal(10));
        AuctionBody auction4 = getAuction("b", new BigDecimal(90));

        listAuctions = new ArrayList<AuctionBody>();
        listAuctions.add(auction1);
        listAuctions.add(auction2);
        listAuctions.add(auction3);
        listAuctions.add(auction4);
    }
//
    private AuctionBody getAuction(String title, BigDecimal startingPrice) {
        AuctionBody auction = new AuctionBody();

        auction.setTitle(title);
        auction.setStartingPrice(startingPrice);

        return auction;
    }

    @Test
    public void sortAuctionsByTitleTest() {
        sortingService.sortBy(SortingEnum.BY_TITLE.toString(), listAuctions);
        assertEquals("A", listAuctions.get(0).getTitle());
        assertEquals("a", listAuctions.get(1).getTitle());
        assertEquals("b", listAuctions.get(2).getTitle());
        assertEquals("c", listAuctions.get(3).getTitle());
    }

    @Test
    public void sortAuctionsByPriceTest() throws Exception {
        sortingService.sortBy(SortingEnum.BY_PRICE.toString(), listAuctions);
        assertEquals(new BigDecimal(10), listAuctions.get(0).getStartingPrice());
        assertEquals(new BigDecimal(50), listAuctions.get(1).getStartingPrice());
        assertEquals(new BigDecimal(90), listAuctions.get(2).getStartingPrice());
        assertEquals(new BigDecimal(100), listAuctions.get(3).getStartingPrice());
    }
}