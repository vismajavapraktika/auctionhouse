package com.visma.seli.service.auction.anonymous;

import com.visma.seli.server.data.access.repository.database.auction.AuctionRepository;
import com.visma.seli.server.data.access.repository.database.user.anonymousUser.AnonymousUserRepository;
import com.visma.seli.server.data.access.repository.database.user.logedInUser.UserRepository;
import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.Invitation;
import com.visma.seli.service.enums.AuctionCurrencyEnum;
import com.visma.seli.service.enums.AuctionStatusEnum;
import com.visma.seli.service.enums.AuctionTypeEnum;
import com.visma.seli.service.enums.UserTypes;
import com.visma.seli.service.exceptions.SeliException;
import com.visma.seli.service.user.AnonymousUser;
import com.visma.seli.service.user.UserBody;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cglib.core.Local;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import static org.junit.Assert.assertEquals;

/**
 * Created by justas.rutkauskas on 2/8/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class AnonymousInviteServiceTest {

    @Autowired
    private AnonymousInviteService anonymousInviteService;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AuctionRepository auctionRepository;
    @Autowired
    private AnonymousUserRepository anonymousUserRepository;

//    UserBody userBody = new UserBody(0, "name1", "surname1", "email1@one.lt", "12345678", UserTypes.ANONYMOUS_USER);
    UUID ownerUuid = UUID.randomUUID();
    AnonymousUser anonymousUser = getAnonymousUser();
    AuctionBody auctionBody = getAuction("title", anonymousUser.getId(), ownerUuid);

    private AnonymousUser getAnonymousUser() {
        AnonymousUser anonymousUser = new AnonymousUser();
        anonymousUser.setEmail("testAnonymousUser@eeee.lt");
        return anonymousUser;
    }

    @Before
    public void init(){

//        userBody.setPassword("123");
//        userRepository.insertNewUser(userBody);

        anonymousUserRepository.insertNewAnonymousUser(anonymousUser);

        auctionRepository.insertNewAuction(auctionBody, anonymousUser.getId(), AuctionTypeEnum.PRIVATE.toString());
    }

    public AuctionBody getAuction(String title, int ownerId, UUID uuid) {
        AuctionBody auctionBody = new AuctionBody();

        auctionBody.setAuctionIdUrlOwner(uuid);
        auctionBody.setAnonymous(true);
        auctionBody.setAllowBuyOut(false);
        auctionBody.setAllowToBidForQuantity(false);
        auctionBody.setStatus(AuctionStatusEnum.ACTIVE);
        auctionBody.setBuyOutPrice(BigDecimal.ZERO);
        auctionBody.setBidsQuantity(1);
        auctionBody.setCurrency(AuctionCurrencyEnum.EUR);
        auctionBody.setTitle(title);
        auctionBody.setDescription("description");
        auctionBody.setType(AuctionTypeEnum.ANONYMOUS);
        auctionBody.setStartDate(LocalDateTime.now());
        auctionBody.setEndDate(LocalDateTime.now().plusHours(48));
        auctionBody.setItemQuantity(1);
        auctionBody.setStartingPrice(BigDecimal.TEN);
        auctionBody.setOwnerId(ownerId);

        return auctionBody;
    }

    @Test
    public void anonymousAuctionInvitation() throws Exception {
    }

    @Test
    public void anonymousAuctionInvitationSaved() throws Exception {

        int invitationsCountBefore = JdbcTestUtils.countRowsInTable(jdbcTemplate, "invitations_anonymous");
        int anonymousUsersCountBefore = JdbcTestUtils.countRowsInTable(jdbcTemplate, "anonymous_user");

        Invitation invitation = new Invitation();
        invitation.setEmail("invitedAnonymousUser@eeeee.lt");

        auctionBody.setInvitations(Arrays.asList(invitation));
        anonymousInviteService.anonymousAuctionInvitationSaved(ownerUuid, auctionBody, getModel());

        int invitationsCountAfter = JdbcTestUtils.countRowsInTable(jdbcTemplate, "invitations_anonymous");
        int anonymousUsersCountAfter = JdbcTestUtils.countRowsInTable(jdbcTemplate, "anonymous_user");

        assertEquals(invitationsCountBefore+1, invitationsCountAfter);
        assertEquals(anonymousUsersCountBefore+1, anonymousUsersCountAfter);
    }

    @Test(expected=SeliException.class)
    public void anonymousAuctionInvitationSavedNoInvitationsAdded() throws Exception {

        anonymousInviteService.anonymousAuctionInvitationSaved(ownerUuid, auctionBody, getModel());
    }

    @Test(expected=SeliException.class)
    public void anonymousAuctionInvitationSavedSelfInvitation() throws Exception {

        Invitation invitation = new Invitation();
        invitation.setEmail(anonymousUser.getEmail());
        auctionBody.setInvitations(Arrays.asList(invitation));
        anonymousInviteService.anonymousAuctionInvitationSaved(ownerUuid, auctionBody, getModel());
    }

    @Test(expected=SeliException.class)
    public void anonymousAuctionInvitationSavedAlreadyInvited() throws Exception {

        Invitation invitation = new Invitation();
        invitation.setEmail("invitedAnonymousUser@eeeee.lt");
        auctionBody.setInvitations(Arrays.asList(invitation));

        anonymousInviteService.anonymousAuctionInvitationSaved(ownerUuid, auctionBody, getModel());
        anonymousInviteService.anonymousAuctionInvitationSaved(ownerUuid, auctionBody, getModel());
    }


    @Test(expected=SeliException.class)
    public void anonymousAuctionInvitationSaved12HoursPassedAfterStartDate() throws Exception {

        Invitation invitation = new Invitation();
        invitation.setEmail("invitedAnonymousUser@eeeee.lt");
        auctionBody.setInvitations(Arrays.asList(invitation));
        auctionBody.setStartDate(LocalDateTime.now().minusHours(12));
        auctionRepository.updateAuction(auctionBody);

        anonymousInviteService.anonymousAuctionInvitationSaved(ownerUuid, auctionBody, getModel());

    }
//    Either auction is not active OR you are not an owner of this auction OR not invited to this auction
    @Test(expected=SeliException.class)
    public void anonymousAuctionInvitationSavedAuctionIsNotActive() throws Exception {

        Invitation invitation = new Invitation();
        invitation.setEmail("invitedAnonymousUser@eeeee.lt");
        auctionBody.setInvitations(Arrays.asList(invitation));
        auctionBody.setStatus(AuctionStatusEnum.ENDED);
        auctionRepository.updateAuction(auctionBody);

        anonymousInviteService.anonymousAuctionInvitationSaved(ownerUuid, auctionBody, getModel());

        //invitations_anonymous, anonymous_user
    }

    private Model getModel() {
        return new Model() {
            @Override
            public Model addAttribute(String attributeName, Object attributeValue) {
                return null;
            }

            @Override
            public Model addAttribute(Object attributeValue) {
                return null;
            }

            @Override
            public Model addAllAttributes(Collection<?> attributeValues) {
                return null;
            }

            @Override
            public Model addAllAttributes(Map<String, ?> attributes) {
                return null;
            }

            @Override
            public Model mergeAttributes(Map<String, ?> attributes) {
                return null;
            }

            @Override
            public boolean containsAttribute(String attributeName) {
                return false;
            }

            @Override
            public Map<String, Object> asMap() {
                return null;
            }
        };
    }

    @Test
    public void sendNotificationsForAnonymousAuctionUsers() throws Exception {
    }

}