package com.visma.seli.service.auction;

import com.google.common.collect.Lists;
import com.visma.seli.server.data.access.repository.database.auction.AuctionRepository;
import com.visma.seli.server.data.access.repository.database.auction.bids.BidRepository;
import com.visma.seli.server.data.access.repository.database.user.logedInUser.UserRepository;
import com.visma.seli.service.AuctionSearchService;
import com.visma.seli.service.auction.bid.Bid;
import com.visma.seli.service.enums.AuctionTypeEnum;
import com.visma.seli.service.user.UserBody;
import com.visma.seli.service.user.UserService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static junit.framework.Assert.assertTrue;
import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

@RunWith(MockitoJUnitRunner.class)
public class AuctionServiceTest {

    @InjectMocks
    private AuctionService auctionService;
    @Mock
    AuctionRepository auctionRepository;
    @Mock
    BidRepository bidRepository;
    @Mock
    UserService userService;
    @Mock
    UserRepository userRepository;
    @Mock
    AuctionSearchService auctionTypes;
    @Mock
    AuctionSearchService auctionSearchService;

    @Before
    public void init(){
        initMocks(this);
        when(userRepository.getOwnerByAuctionId(1)).thenReturn(getDefaultUser());
        when(bidRepository.findAllAuctionBids(1)).thenReturn(getDefaultBidList());
        when(auctionRepository.findAuctionPictureBytes(1,1)).thenReturn(getDefaultPictureBytes());
        when(auctionRepository.getDefaultPicture()).thenReturn(getDefaultPictureBytes());
        when(auctionRepository.findAuction(1)).thenReturn(getAuctionBodyForLoggedInUser());
        when(auctionRepository.findAuction(2)).thenReturn(getAuctionBodyForLoggedInUser2());
        when(userService.getLoggedInUserId()).thenReturn(getDefaultUser().getId());
        when(auctionRepository.getItemQuantityLeft(1)).thenReturn(getItemQuantityLeftInt());
        when(auctionSearchService.findByType("public")).thenReturn(getListOfAuctionsOfType(getListOfAuctions(),AuctionTypeEnum.PUBLIC));
        when(auctionSearchService.findByType("private")).thenReturn(getListOfAuctionsOfType(getListOfAuctions(),AuctionTypeEnum.PRIVATE));
        when(auctionSearchService.findByType("all")).thenReturn(getListOfAuctionsOfType(getListOfAuctions(),AuctionTypeEnum.ALL));

    }

    @Test
    public void shouldReturnTrueIfCorrectUser(){
        UserBody result = auctionService.getAuctionOwner(1);
        UserBody expectedResult = getDefaultUser();

        assertEquals(expectedResult, result);
    }

    @Test
    public void shouldReturnTrueIfBidListIsCorrect(){
        List<Bid> resultBidList = auctionService.getAllAuctionBids(1);
        List<Bid> expectedBidListResult = getDefaultBidList();

        assertEquals(expectedBidListResult, resultBidList);
    }

    @Test
    public void shouldReturnTrueIfGetAmountOfBidsIsCorrect(){
        int resultAmountOfBids = auctionService.getAmountOfBids(1);
        int expectedAmountOfBids = getDefaultBidList().size();

        assertEquals(expectedAmountOfBids, resultAmountOfBids);
    }

    @Test
    public void shouldReturnTrueIfAuctionPictureBytesIsCorrect(){
        byte[] resultAuctionPictureBytes = auctionService.getAuctionPictureBytes(1,1);
        byte[] expectedAuctionPictureBytes = getDefaultPictureBytes();

        assertTrue(Arrays.equals(resultAuctionPictureBytes,expectedAuctionPictureBytes));
    }

    @Test
    public void shouldReturnTrueIfDefaultAuctionPictureBytesIsCorrect(){
        byte[] resultAuctionPictureBytes = auctionService.getAuctionPictureBytes(1,0);
        byte[] expectedAuctionPictureBytes = getDefaultPictureBytes();

        assertTrue(Arrays.equals(resultAuctionPictureBytes,expectedAuctionPictureBytes));
    }

    @Test
    public void shouldReturnFalseIfDefaultAuctionPictureBytesIsCorrect(){
        byte[] resultAuctionPictureBytes = auctionService.getAuctionPictureBytes(1,0);
        byte[] expectedAuctionPictureBytes = getPictureBytes();

        assertFalse(Arrays.equals(resultAuctionPictureBytes,expectedAuctionPictureBytes));
    }

    @Test
    public void shouldReturnTrueIfLoggedInUserIsAuctionOwner(){
        assertTrue(auctionService.isLoggedInUserAuctionOwner(1));
    }

    @Test
    public void shouldReturnFalseForLoggedInUserWhoIsNotAuctionOwner(){
        assertFalse(auctionService.isLoggedInUserAuctionOwner(2));
    }

    @Test
    public void shouldReturnTrueIfSelectedItemQuantityIsLessOrEqualThanLeftItemQuantity(){
        for (int i = 0; i <= getItemQuantityLeftInt(); i++) {
            assertTrue(auctionService.getItemQuantityYouCanBuy(i, 1) == i);
        }
    }

    @Test
    public void shouldReturnTrueIfSelectedItemQuantityMoreThanLeftItemQuantity(){
        for (int i = getItemQuantityLeftInt()+1; i <= getItemQuantityLeftInt() + 10; i++) {
            assertTrue(auctionService.getItemQuantityYouCanBuy(i, 1) == getItemQuantityLeftInt());
        }
    }

    @Test
    public void shouldReturnTrueIfAuctionTypeIsSelectedCorrectly(){
        List<AuctionBody> resultListPublic = auctionSearchService.findByType("public");
        List<AuctionBody> expectedListPublic = getListOfAuctionsOfType(getListOfAuctions(),AuctionTypeEnum.PUBLIC);
        assertEquals(expectedListPublic,resultListPublic);

        List<AuctionBody> resultListPrivate = auctionSearchService.findByType("private");
        List<AuctionBody> expectedListPrivate = getListOfAuctionsOfType(getListOfAuctions(),AuctionTypeEnum.PRIVATE);
        assertEquals(expectedListPrivate,resultListPrivate);

        List<AuctionBody> resultListAll = auctionSearchService.findByType("all");
        List<AuctionBody> expectedListAll = getListOfAuctionsOfType(getListOfAuctions(),AuctionTypeEnum.ALL);
        assertEquals(expectedListAll,resultListAll);
    }

    @Test
    public void shouldReturnTrueWhenAuctionEnded() throws Exception {
        // given
        List<Bid> expectedBids = Lists.newArrayList(fixedBid());
        given(bidRepository.findAllAuctionBids(anyInt())).willReturn(expectedBids);

        // when
        List<Bid> result = auctionService.getAllAuctionBids(5);

        // then
        assertThat(result, is(expectedBids));
        verify(bidRepository).findAllAuctionBids(eq(5));
    }

    private Bid fixedBid() {
        return new Bid();
    }

    private LocalDateTime formatStringToLocalDateTime(String dateString){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return LocalDateTime.parse(dateString, formatter);
    }

    private static UserBody getDefaultUser(){
        return new UserBody(1, "name", "surName", "email", "phone", "password");
    }

    private static UserBody getUser(){
        return new UserBody(2, "name2","surName2","email2","phone2","password2");
    }

    private static AuctionBody getDefaultAuction() {
        AuctionBody auctionBody = new AuctionBody();
        auctionBody.setOwnerId(1);
        return auctionBody;
    }

    private UserBody getAuctionOwner(int auctionId) {
        AuctionBody auctionBody = new AuctionBody();
        auctionBody.setEndDate(formatStringToLocalDateTime("2016-12-01 12:00"));
        return userRepository.getOwnerByAuctionId(auctionId);
    }

    private static List<Bid> getDefaultBidList(){
        List<Bid> listOfBids = new ArrayList<>();
        Bid newBid = new Bid(1,1,1,new BigDecimal("10.00"),1);
        Bid newBid2 = new Bid(2,1,2,new BigDecimal("11.00"),1);
        Bid newBid3 = new Bid(3,1,3,new BigDecimal("12.00"),1);
        listOfBids.add(newBid);
        listOfBids.add(newBid2);
        listOfBids.add(newBid3);
        return listOfBids;
    }

    private static byte[] getDefaultPictureBytes(){
        return new BigInteger("111110011",2).toByteArray();
    }
    private static byte[] getPictureBytes(){
        return new BigInteger("111110011000",2).toByteArray();
    }

    private static AuctionBody getAuctionBodyForLoggedInUser(){
        AuctionBody auctionBody = new AuctionBody();
        auctionBody.setOwnerId(1);
        auctionBody.setId(1);
        return auctionBody;
    }

    private static AuctionBody getAuctionBodyForLoggedInUser2() {
        AuctionBody auctionBody = new AuctionBody();
        auctionBody.setOwnerId(2);
        auctionBody.setId(2);
        return auctionBody;
    }

    private static Integer getItemQuantityLeftInt() {
        return 10;
    }

    private static AuctionBody getAuctionForAuctionList(int id, AuctionTypeEnum type, UserBody userBody){
        AuctionBody auctionBody = new AuctionBody();
        auctionBody.setId(id);
        auctionBody.setType(type);
        auctionBody.setOwnerId(userBody.getId());
        return auctionBody;
    }

    private static List<AuctionBody> getListOfAuctions(){
        List<AuctionBody> listOfAuctions = new ArrayList<>();
        for (int i = 0; i < 3; i++){
            listOfAuctions.add(getAuctionForAuctionList(i,AuctionTypeEnum.PRIVATE, getUser()));
        }
        for (int i = 3; i < 6; i++){
            listOfAuctions.add(getAuctionForAuctionList(i,AuctionTypeEnum.PUBLIC, getUser()));
        }
        for (int i = 6; i < 9; i++){
            listOfAuctions.add(getAuctionForAuctionList(i,AuctionTypeEnum.MY,getDefaultUser()));
        }
        for (int i = 9; i < 12; i++){
            listOfAuctions.add(getAuctionForAuctionList(i,AuctionTypeEnum.MYBIDS,getUser()));
        }
        for (int i = 12; i < 15; i++){
            listOfAuctions.add(getAuctionForAuctionList(i,AuctionTypeEnum.WON,getUser()));
        }
        return listOfAuctions;
    }

    private static List<AuctionBody> getListOfAuctionsOfType(List<AuctionBody> auctionBodyList,AuctionTypeEnum typeEnum){
        if (typeEnum.equals(AuctionTypeEnum.ALL)){
            List<AuctionBody> list = new ArrayList<>();
            for (AuctionBody anAuctionBodyList : auctionBodyList) {
                if (anAuctionBodyList.getType().equals(AuctionTypeEnum.PUBLIC)) {
                    list.add(anAuctionBodyList);
                }
                if (anAuctionBodyList.getType().equals(AuctionTypeEnum.PRIVATE)) {
                    list.add(anAuctionBodyList);
                }
            }
            return list;
        }
        return auctionBodyList.stream()
                .filter(anAuctionBodyList -> anAuctionBodyList.getType().equals(typeEnum))
                .collect(Collectors.toList());
    }

    private static List<AuctionBody> getAuctionListByUserID(List<AuctionBody> auctionBodyList, int userId){
        List<AuctionBody> listToReturn = new ArrayList<>();
        for (AuctionBody anAuctionBodyList : auctionBodyList) {
            if (anAuctionBodyList.getOwnerId() == userId) {
                listToReturn.add(anAuctionBodyList);
            }
        }
        return listToReturn;
    }

}