package com.visma.seli.controllers.customValidators.CustomAuctionValidators;

import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.enums.AuctionCurrencyEnum;
import com.visma.seli.service.captions.DefaultCaptionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

@RunWith(MockitoJUnitRunner.class)
public class AuctionBuyOutValidatorTest {

    @Mock
    private DefaultCaptionService defaultCaptionService;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private ConstraintValidatorContext constraintValidatorContext;
    @InjectMocks
    private AuctionBuyOutValidator auctionBuyOutValidator;

    @Test
    public void shouldValidateBuyoutPriceIsSetWhenKudosSelected() {
        // given
        AuctionBody auctionBody = new AuctionBody();
        auctionBody.setCurrency(AuctionCurrencyEnum.KUDOS);
        auctionBody.setAllowBuyOut(true);
        auctionBody.setBuyOutPrice(BigDecimal.TEN);
        given(defaultCaptionService.getCaption(anyString())).willReturn("error message");

        // when
        boolean result = auctionBuyOutValidator.isValid(auctionBody, constraintValidatorContext);

        // then
        assertThat(result, is(false));
        verify(constraintValidatorContext).buildConstraintViolationWithTemplate("error message");
    }

    @Test
    public void shouldValidateBuyoutPriceIsSetWhenQuantityIsMoreThanOne() {
        // given
        AuctionBody auctionBody = new AuctionBody();
        auctionBody.setCurrency(AuctionCurrencyEnum.EUR);
        auctionBody.setAllowBuyOut(true);
        auctionBody.setItemQuantity(2);
        auctionBody.setBuyOutPrice(BigDecimal.ONE);
        given(defaultCaptionService.getCaption(anyString())).willReturn("error message");

        // when
        boolean result = auctionBuyOutValidator.isValid(auctionBody, constraintValidatorContext);

        // then
        assertThat(result, is(false));
        verify(constraintValidatorContext).buildConstraintViolationWithTemplate("error message");
    }

    @Test
    public void shouldValidateBuyoutPriceIsLoverThenStartingPrice() {
        // given
        AuctionBody auctionBody = new AuctionBody();
        auctionBody.setCurrency(AuctionCurrencyEnum.EUR);
        auctionBody.setAllowBuyOut(true);
        auctionBody.setItemQuantity(2);
        auctionBody.setBuyOutPrice(BigDecimal.ONE);
        auctionBody.setStartingPrice(BigDecimal.TEN);
        given(defaultCaptionService.getCaption(anyString())).willReturn("error message");

        // when
        boolean result = auctionBuyOutValidator.isValid(auctionBody, constraintValidatorContext);

        // then
        assertThat(result, is(false));
        verify(constraintValidatorContext).buildConstraintViolationWithTemplate("error message");
    }

    @Test
    public void shouldValidateBuyoutPriceIsNullWhenSelectedToAllowBuyout() {
        // given
        AuctionBody auctionBody = new AuctionBody();
        auctionBody.setCurrency(AuctionCurrencyEnum.EUR);
        auctionBody.setAllowBuyOut(true);
        auctionBody.setItemQuantity(null);
        auctionBody.setBuyOutPrice(null);
        auctionBody.setStartingPrice(BigDecimal.TEN);
        given(defaultCaptionService.getCaption(anyString())).willReturn("error message");

        // when
        boolean result = auctionBuyOutValidator.isValid(auctionBody, constraintValidatorContext);

        // then
        assertThat(result, is(false));
        verify(constraintValidatorContext).buildConstraintViolationWithTemplate("error message");
    }

    @Test
    public void correctAuctionBodyShouldPassValidationWhenNotAllowed() throws Exception {
        // given
        AuctionBody auctionBody = correctAuctionWhenAllowedToBuyout();
        given(defaultCaptionService.getCaption(anyString())).willReturn("error message");

        // when
        boolean result = auctionBuyOutValidator.isValid(auctionBody, constraintValidatorContext);

        // then
        assertThat(result, is(true));
        verifyZeroInteractions(constraintValidatorContext);
    }

    private AuctionBody correctAuctionWhenAllowedToBuyout() {
        AuctionBody auctionBody = new AuctionBody();
        auctionBody.setCurrency(AuctionCurrencyEnum.EUR);
        auctionBody.setAllowBuyOut(false);
        auctionBody.setItemQuantity(1);
        auctionBody.setBuyOutPrice(null);
        auctionBody.setStartingPrice(BigDecimal.ONE);
        return auctionBody;
    }
}