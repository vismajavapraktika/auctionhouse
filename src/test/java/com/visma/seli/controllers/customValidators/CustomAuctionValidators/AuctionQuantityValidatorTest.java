package com.visma.seli.controllers.customValidators.CustomAuctionValidators;

import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.captions.DefaultCaptionService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.validation.ConstraintValidatorContext;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

@RunWith(MockitoJUnitRunner.class)
public class AuctionQuantityValidatorTest {
    @Mock
    private DefaultCaptionService defaultCaptionService;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private ConstraintValidatorContext constraintValidatorContext;
    @InjectMocks
    private AuctionQuantityValidator auctionQuantityValidator;

    @Test
    public void shouldValidateThatQuantityIsSetWhenAllowedToBidForQuantity() {
        // given
        AuctionBody auctionBody = new AuctionBody();
        auctionBody.setItemQuantity(null);
        auctionBody.setAllowToBidForQuantity(true);
        given(defaultCaptionService.getCaption(anyString())).willReturn("error message");

        // when
        boolean result = auctionQuantityValidator.isValid(auctionBody, constraintValidatorContext);

        // then
        assertThat(result, is(false));
        verify(constraintValidatorContext).buildConstraintViolationWithTemplate("error message");
        verify(constraintValidatorContext.buildConstraintViolationWithTemplate("error message")).addPropertyNode("itemQuantity");
        verify(constraintValidatorContext.buildConstraintViolationWithTemplate("error message").addPropertyNode("itemQuantity")).addConstraintViolation();
    }

    @Test
    public void shouldValidateThatQuantityIsSetWhenNotAllowedToBidForQuantity() {
        // given
        AuctionBody auctionBody = new AuctionBody();
        auctionBody.setItemQuantity(20);
        auctionBody.setAllowToBidForQuantity(false);
        given(defaultCaptionService.getCaption(anyString())).willReturn("error message");

        // when
        boolean result = auctionQuantityValidator.isValid(auctionBody, constraintValidatorContext);

        // then
        assertThat(result, is(false));
        verify(constraintValidatorContext).buildConstraintViolationWithTemplate("error message");
        verify(constraintValidatorContext.buildConstraintViolationWithTemplate("error message")).addPropertyNode("itemQuantity");
        verify(constraintValidatorContext.buildConstraintViolationWithTemplate("error message").addPropertyNode("itemQuantity")).addConstraintViolation();
    }

    @Test
    public void correctAuctionBodyShouldPassValidationWhenAllowed() throws Exception {
        // given
        AuctionBody auctionBody = correctAuctionWhenAllowedToBidForQuantity();
        given(defaultCaptionService.getCaption(anyString())).willReturn("error message");

        // when
        boolean result = auctionQuantityValidator.isValid(auctionBody, constraintValidatorContext);

        // then
        assertThat(result, is(true));
        verifyZeroInteractions(constraintValidatorContext);
    }

    @Test
    public void correctAuctionBodyShouldPassValidationWhenNotAllowed() throws Exception {
        // given
        AuctionBody auctionBody = correctAuctionWhenNotAllowedToBidForQuantity();
        given(defaultCaptionService.getCaption(anyString())).willReturn("error message");

        // when
        boolean result = auctionQuantityValidator.isValid(auctionBody, constraintValidatorContext);

        // then
        assertThat(result, is(true));
        verifyZeroInteractions(constraintValidatorContext);
    }

    private AuctionBody correctAuctionWhenAllowedToBidForQuantity() {
        AuctionBody auctionBody = new AuctionBody();
        auctionBody.setItemQuantity(2);
        auctionBody.setAllowToBidForQuantity(true);
        return auctionBody;
    }

    private AuctionBody correctAuctionWhenNotAllowedToBidForQuantity() {
        AuctionBody auctionBody = new AuctionBody();
        auctionBody.setItemQuantity(1);
        auctionBody.setAllowToBidForQuantity(false);
        return auctionBody;
    }
}