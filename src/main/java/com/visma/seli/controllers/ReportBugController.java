package com.visma.seli.controllers;

import com.visma.seli.server.data.access.repository.database.user.logedInUser.UserRepository;
import com.visma.seli.service.ReportBug;
import com.visma.seli.service.mailing.NotificationService;
import com.visma.seli.service.user.UserBody;
import com.visma.seli.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class ReportBugController {
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;
    private static final int NO_NUMBER = -1;

    @RequestMapping(value = "/report-bug", method = RequestMethod.GET)
    public String reportBug(Model model){
        ReportBug reportBug = new ReportBug();
        model.addAttribute("reportBug", reportBug);
        return "html/reportBug";
    }

    @RequestMapping(value = "/report-bug", method = RequestMethod.POST)
    public String reportBugPost(@Valid @ModelAttribute("reportBug") ReportBug reportBug, BindingResult bindingResult){
        if (bindingResult.hasErrors()){
            return "html/report-bug";
        }
        if (!userService.getLoggedInUserEmail().equals("anonymousUser")) {
            UserBody loggedInUser = userRepository.getUserById(userService.getLoggedInUserId());
            notificationService.sendNotification(NO_NUMBER, "REPORT BUG " + reportBug.getSubject(), reportBug.getText() + "\nFrom email: " + loggedInUser.getEmail() +
                    "\nName and Surname: " + loggedInUser.getName() + " " + loggedInUser.getSurname(), "lukas.karmanovas@visma.com");
            return "redirect:/home";
        }else{
            notificationService.sendNotification(NO_NUMBER, "REPORT BUG " + reportBug.getSubject(), reportBug.getText() + "\nAnonymous user", "lukas.karmanovas@visma.com");
            return "redirect:/home";
        }
    }
}
