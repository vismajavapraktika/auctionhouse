package com.visma.seli.controllers.customValidators.CustomBiddingValidators.BiddingValidatorUtils;

import com.visma.seli.server.data.access.repository.database.auction.bids.BidRepository;
import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.bid.Bid;
import com.visma.seli.service.captions.DefaultCaptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;

@Component
public class CompareBidToLastBid {

    @Autowired
    private DefaultCaptionService defaultCaptionService;
    @Autowired
    private BidRepository bidRepository;
    @Autowired
    private BidTimeExtensionChecker bidTimeExtensionChecker;

    public boolean compareBidToHighestBidResult(Bid bid, ConstraintValidatorContext context, AuctionBody auction) {

        BigDecimal highestBid = bidRepository.findHighestBid(bid.getAuctionId()).getAmount();
        if (bid.getAmount().compareTo(highestBid) == 0) {
            context.buildConstraintViolationWithTemplate(defaultCaptionService.getCaption("bid.error.bid.not.equal.to.highest.bid"))
                    .addPropertyNode("amount").addConstraintViolation();
            return false;
        } else if (bid.getAmount().compareTo(highestBid) == 1) {
            return bidTimeExtensionChecker.isBidPlacedDuringLastFiveMinutes(auction, bid);
        } else if (bid.getAmount().compareTo(highestBid) == -1) {
            context.buildConstraintViolationWithTemplate(defaultCaptionService.getCaption("bid.error.bid.smaller.highest.bid"))
                    .addPropertyNode("amount").addConstraintViolation();
            return false;
        }
        else {
            return true;
        }
    }
}
