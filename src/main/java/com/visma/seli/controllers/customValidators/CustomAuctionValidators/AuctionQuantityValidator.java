package com.visma.seli.controllers.customValidators.CustomAuctionValidators;

import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.captions.DefaultCaptionService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AuctionQuantityValidator implements ConstraintValidator<ValidAuctionQuantity, AuctionBody> {

    @Autowired
    private DefaultCaptionService defaultCaptionService;


    @Override
    public void initialize(ValidAuctionQuantity constraintAnnotation) {
    }


    public boolean isValid(final AuctionBody auctionBody, final ConstraintValidatorContext context) {
        return validateAuctionQuantity(auctionBody, context);
    }

    private boolean validateAuctionQuantity(AuctionBody auctionBody, ConstraintValidatorContext context) {
    	if(auctionBody.getItemQuantity() == null)
    		auctionBody.setItemQuantity(1);
        if (auctionBody.getItemQuantity() == null && auctionBody.isAllowToBidForQuantity()) {
            addValidationMessage(context, "auction.error.notnull");
            return false;
        } else if (auctionBody.getItemQuantity() == 1 && !auctionBody.isAllowToBidForQuantity()) {
            return true;
        } else if (auctionBody.getItemQuantity() != 1 && !auctionBody.isAllowToBidForQuantity()) {
            addValidationMessage(context, "You should not be doing that");
            return false;
        } else if (auctionBody.getItemQuantity() < 2 && auctionBody.isAllowToBidForQuantity()) {
            addValidationMessage(context, "auction.error.notNegative");
            return false;
        } else {
            return true;
        }
    }

    private void addValidationMessage(ConstraintValidatorContext context, String validationMessage) {
        context.buildConstraintViolationWithTemplate(
                defaultCaptionService.getCaption(validationMessage))
                .addPropertyNode("itemQuantity").addConstraintViolation();
    }
}
