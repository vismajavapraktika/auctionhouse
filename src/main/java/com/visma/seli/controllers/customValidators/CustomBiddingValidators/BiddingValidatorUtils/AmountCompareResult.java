package com.visma.seli.controllers.customValidators.CustomBiddingValidators.BiddingValidatorUtils;

import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.bid.Bid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidatorContext;

@Component
public class AmountCompareResult {

    @Autowired
    private CompareBidToAuctionPrice compareBidToAuctionPrice;
    @Autowired
    private CompareBidToLastBid compareBidToLastBid;

    public boolean getComparingResult(int i, Bid bid, ConstraintValidatorContext context, AuctionBody auction) {
        if (i == 1) {
            return compareBidToAuctionPrice.compareBidToAuctionPriceResult(bid, context, auction);
        } else if (i == 2) {
            return compareBidToLastBid.compareBidToHighestBidResult(bid, context, auction);
        } else {
            return false;
        }
    }

}
