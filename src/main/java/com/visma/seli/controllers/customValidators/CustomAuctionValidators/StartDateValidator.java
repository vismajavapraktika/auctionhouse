package com.visma.seli.controllers.customValidators.CustomAuctionValidators;


import com.visma.seli.service.captions.DefaultCaptionService;
import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;


public class StartDateValidator implements ConstraintValidator<ValidStartDate, Object> {
    @Autowired
    private MessageSource messageSource;
    private String startDate, id;
    @Autowired
    private DefaultCaptionService defaultCaptionService;
    private int INTERVAL_FOR_AUCTION_TO_CREATE = 15;
    private static final Logger logger = Logger.getLogger(Class.class.getName());


    private boolean validStartTime(Object objToValidate, ConstraintValidatorContext constraintValidatorContext) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        LocalDateTime startDateTime = (LocalDateTime) PropertyUtils.getSimpleProperty(objToValidate, startDate);
        int newId = (int) PropertyUtils.getSimpleProperty(objToValidate, id);
        if (newId > 0) {
            return true;
        }
        if (startDateTime != null) {
            if (startDateTime.isAfter(LocalDateTime.now().minusMinutes(INTERVAL_FOR_AUCTION_TO_CREATE)) && (startDate.equals(LocalDateTime.now().plusWeeks(1))
                    || (startDateTime.isAfter(LocalDateTime.now().minusMinutes(INTERVAL_FOR_AUCTION_TO_CREATE)) && startDateTime.isBefore(LocalDateTime.now().plusWeeks(1))))) {
                return true;
            } else {
                constraintValidatorContext.disableDefaultConstraintViolation();
                constraintValidatorContext
                        .buildConstraintViolationWithTemplate((messageSource.getMessage("auction.error.start.time.length",
                                new Object[]{LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")), LocalDateTime.now().plusWeeks(1).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"))}
                                , LocaleContextHolder.getLocale())))
                        .addPropertyNode(startDate)
                        .addConstraintViolation();
                return false;
            }
        } else {
            constraintValidatorContext.disableDefaultConstraintViolation();
            addValidationMessage(constraintValidatorContext, "auction.error.start.time.not.empty", startDate);
            return false;
        }

    }


    @Override
    public void initialize(ValidStartDate constraintAnnotation) {
        startDate = constraintAnnotation.first();
        id = constraintAnnotation.second();
    }

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        try {
            return validStartTime(o, constraintValidatorContext);
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            logger.log(Level.SEVERE,e.toString(),e);
            e.printStackTrace();
        }
        return false;
    }

    private void addValidationMessage(ConstraintValidatorContext context, String validationMessage, String node) {
        context.buildConstraintViolationWithTemplate(
                defaultCaptionService.getCaption(validationMessage))
                .addPropertyNode(node).addConstraintViolation();
    }

}
