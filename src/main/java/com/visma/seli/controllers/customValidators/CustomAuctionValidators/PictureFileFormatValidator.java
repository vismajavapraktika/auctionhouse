package com.visma.seli.controllers.customValidators.CustomAuctionValidators;

import com.visma.seli.controllers.auction.CreateAuctionController;
import com.visma.seli.service.captions.DefaultCaptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PictureFileFormatValidator implements ConstraintValidator<ValidPictureFileFormat, MultipartFile[]> {

    private static final String IMAGE_PATTERN = "([^\\n]+(\\.(?i)(jpg|png|bmp|jpeg))$)";
    @Autowired
    private DefaultCaptionService defaultCaptionService;
    private Pattern pattern;
    private Matcher matcher;
    @Autowired
    private CreateAuctionController createAuctionController;

    @Override
    public void initialize(ValidPictureFileFormat constraintAnnotation) {
        pattern = Pattern.compile(IMAGE_PATTERN);
    }

    @Override
    public boolean isValid(MultipartFile[] pictureFiles, ConstraintValidatorContext context) {
        return (validatePictureFileName(pictureFiles, context));
    }

    private boolean validatePictureFileName(MultipartFile[] pictureFiles, ConstraintValidatorContext context) {
        return Arrays.stream(pictureFiles).filter(p -> p != null).allMatch(f -> validPictureFile(context, f));
    }

    private boolean validPictureFile(ConstraintValidatorContext context, MultipartFile pictureFile) {
        matcher = pattern.matcher(pictureFile.getOriginalFilename());
        if ((pictureFile.getSize() == 0 || !matcher.matches()) && !pictureFile.getOriginalFilename().equals("")) {
            context.buildConstraintViolationWithTemplate(
                    defaultCaptionService.getCaption("auction.error.picture.invalid.pattern"))
                    .addConstraintViolation();
            return false;
        }
        return true;
    }

}