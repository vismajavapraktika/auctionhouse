package com.visma.seli.controllers.customValidators.CustomCommentsValidators;


import com.visma.seli.service.auction.comment.Comment;
import com.visma.seli.service.captions.DefaultCaptionService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CommentValidator implements ConstraintValidator<ValidComment, Comment> {

    @Autowired
    private DefaultCaptionService defaultCaptionService;

    @Override
    public void initialize(ValidComment constraintAnnotation) {
    }


    public boolean isValid(final Comment comment, final ConstraintValidatorContext context) {
        return validateAuctionComment(comment, context);
    }

    private boolean validateAuctionComment(Comment comment, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();

        if (comment.getText().isEmpty()) {
            context.buildConstraintViolationWithTemplate(defaultCaptionService.getCaption("auction.comment.text.empty"))
                    .addPropertyNode("text").addConstraintViolation();
            return false;
        }
        
		return true;
    }
}
