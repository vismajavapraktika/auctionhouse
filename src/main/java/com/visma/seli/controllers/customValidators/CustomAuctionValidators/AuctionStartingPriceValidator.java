package com.visma.seli.controllers.customValidators.CustomAuctionValidators;


import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.enums.AuctionCurrencyEnum;
import com.visma.seli.service.captions.DefaultCaptionService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AuctionStartingPriceValidator implements ConstraintValidator<ValidStartingPrice, AuctionBody> {

    @Autowired
    private DefaultCaptionService defaultCaptionService;


    @Override
    public void initialize(ValidStartingPrice constraintAnnotation) {
    }


    public boolean isValid(final AuctionBody auctionBody, final ConstraintValidatorContext context) {
        return validateAuctionStartingPriceByCurrencyType(auctionBody, context);
    }

    private boolean validateAuctionStartingPriceByCurrencyType(AuctionBody auctionBody, ConstraintValidatorContext context) {
        if (auctionBody.getCurrency() == AuctionCurrencyEnum.KUDOS) {
            if (auctionBody.getStartingPrice() == null){
                addValidationMessage(context, "auction.error.kudos.starting.price.Integer");
                return false;
            }
            if (auctionBody.getStartingPrice().stripTrailingZeros().scale() <= 0) {
                return true;
            }
            else {
                addValidationMessage(context, "auction.error.kudos.starting.price.Integer");
                return false;
            }
        } else {
            return true;
        }

    }

    private void addValidationMessage(ConstraintValidatorContext context, String validationMessage) {
        context.buildConstraintViolationWithTemplate(
                defaultCaptionService.getCaption(validationMessage))
                .addPropertyNode("startingPrice").addConstraintViolation();
    }
}

