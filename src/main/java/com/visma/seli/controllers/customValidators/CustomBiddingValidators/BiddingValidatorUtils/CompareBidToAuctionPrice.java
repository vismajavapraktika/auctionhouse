package com.visma.seli.controllers.customValidators.CustomBiddingValidators.BiddingValidatorUtils;

import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.bid.Bid;
import com.visma.seli.service.captions.DefaultCaptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidatorContext;

@Component
public class CompareBidToAuctionPrice {

    @Autowired
    private DefaultCaptionService defaultCaptionService;
    @Autowired
    private BidTimeExtensionChecker bidTimeExtensionChecker;

    public boolean compareBidToAuctionPriceResult(Bid bid, ConstraintValidatorContext context, AuctionBody auction) {
        int decimalCompareResult = bid.getAmount().compareTo(auction.getStartingPrice());
        if (decimalCompareResult == 0 || decimalCompareResult == 1) {
            return bidTimeExtensionChecker.isBidPlacedDuringLastFiveMinutes(auction, bid);
        } else {
            context.buildConstraintViolationWithTemplate(defaultCaptionService.getCaption("bid.error.lower.then.auction.price") + " (" + (auction.getStartingPrice()) + ")")
                    .addPropertyNode("amount").addConstraintViolation();
            return false;
        }
    }
}
