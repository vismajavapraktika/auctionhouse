package com.visma.seli.controllers.customValidators.CustomAuctionValidators;

import com.visma.seli.server.data.access.repository.database.user.logedInUser.UserRepository;
import com.visma.seli.service.captions.DefaultCaptionService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

public class EmailValidator implements ConstraintValidator<ValidEmail, String> {

    @Autowired
    private DefaultCaptionService defaultCaptionService;
    @Autowired
    private UserRepository userRepository;

    @Override
    public void initialize(ValidEmail constraintAnnotation) {
    }

    @Override
    public boolean isValid(String email, ConstraintValidatorContext context) {
        return (validateEmailExist(email, context));
    }

    private boolean validateEmailExist(String email, ConstraintValidatorContext context) {
        List<String> emails = userRepository.getEmails();
        for (String email2 : emails) {
            if (email.equals(email2)) {
                context.buildConstraintViolationWithTemplate(defaultCaptionService.getCaption("auction.error.email")).addConstraintViolation();
                return false;
            }
        }
        return true;
    }

}
