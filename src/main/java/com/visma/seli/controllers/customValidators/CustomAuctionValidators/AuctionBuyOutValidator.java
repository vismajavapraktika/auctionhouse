package com.visma.seli.controllers.customValidators.CustomAuctionValidators;

import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.enums.AuctionCurrencyEnum;
import com.visma.seli.service.captions.DefaultCaptionService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AuctionBuyOutValidator implements ConstraintValidator<ValidBuyOut, AuctionBody> {

    @Autowired
    private DefaultCaptionService defaultCaptionService;

    @Override
    public void initialize(ValidBuyOut constraintAnnotation) {
    }

    public boolean isValid(final AuctionBody auctionBody, final ConstraintValidatorContext context) {
        return validateAuctionBuyout(auctionBody, context);
    }

    private boolean validateAuctionBuyout(AuctionBody auctionBody, ConstraintValidatorContext context) {

        if (!auctionBody.isAllowBuyOut() && auctionBody.getBuyOutPrice() == null) {
            return true;
        }

        if (buyoutPriceIsSetWhenKudosSelected(auctionBody)) {
            addValidationMessage(context, "auction.error.buyout.not.allowed.when.kudos.selected");
            return false;
        }
        if (auctionBody.getCurrency().equals(AuctionCurrencyEnum.EUR)) {
            if (buyoutPriceIsNullWhenSelectedToAllowBuyout(auctionBody)) {
                addValidationMessage(context, "auction.error.buyout.not.entered.when.it.is.allowed.to.buy.out");
                return false;
            } else if (buyoutPriceIsSetWhenQuantityIsMoreThanOne(auctionBody)) {
                addValidationMessage(context, "auction.error.buyout.not.allowed.when.quantity.more.than.one.selected");
                return false;
            } else if (buyoutPriceIsLoverThenStartingPrice(auctionBody)) {
                addValidationMessage(context, "auction.error.buyout.can.not.be.less.then.starting.price");
                return false;
            }

        }

        return true;
    }

    private void addValidationMessage(ConstraintValidatorContext context, String validationMessage) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(
                defaultCaptionService.getCaption(validationMessage))
                .addPropertyNode("allowBuyOut").addConstraintViolation();
    }

    private boolean buyoutPriceIsSetWhenKudosSelected(AuctionBody auctionBody) {
        if (auctionBody.getCurrency().equals(AuctionCurrencyEnum.KUDOS))
            if (auctionBody.isAllowBuyOut() || auctionBody.getBuyOutPrice() != null)
                return true;

        return false;
    }

    private boolean buyoutPriceIsSetWhenQuantityIsMoreThanOne(AuctionBody auctionBody) {
        return auctionBody.getItemQuantity() != null && auctionBody.getItemQuantity() > 1
                && auctionBody.getBuyOutPrice() != null;
    }

    private boolean buyoutPriceIsLoverThenStartingPrice(AuctionBody auctionBody) {
        return auctionBody.getBuyOutPrice() != null && auctionBody.isAllowBuyOut() && auctionBody.getStartingPrice().compareTo(auctionBody.getBuyOutPrice()) == 1;
    }

    private boolean buyoutPriceIsNullWhenSelectedToAllowBuyout(AuctionBody auctionBody) {
        return auctionBody.isAllowBuyOut() && auctionBody.getBuyOutPrice() == null;
    }

}