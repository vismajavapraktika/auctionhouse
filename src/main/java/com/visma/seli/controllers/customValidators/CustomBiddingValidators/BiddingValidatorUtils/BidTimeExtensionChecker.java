package com.visma.seli.controllers.customValidators.CustomBiddingValidators.BiddingValidatorUtils;


import com.visma.seli.server.data.access.repository.database.auction.AuctionRepository;
import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.bid.Bid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Component
public class BidTimeExtensionChecker {

    @Autowired
    private AuctionRepository auctionRepository;

    @Transactional(propagation = Propagation.REQUIRED)
    public boolean isBidPlacedDuringLastFiveMinutes(AuctionBody auction, Bid bid) {
        if (LocalDateTime.now().plusMinutes(5).isAfter(auction.getEndDate())) {
            auctionRepository.updateAuctionEndTimeOnBid(bid.getAuctionId(), auction.getEndDate().plusMinutes(5));
        }
        return true;
    }
}
