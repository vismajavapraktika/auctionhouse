package com.visma.seli.controllers.customValidators.CustomUserValidators;

import com.visma.seli.service.captions.DefaultCaptionService;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.visma.seli.service.enums.UserTypes.GOOGLE_USER;

public class TwoFieldsMatchesValidator implements ConstraintValidator<TwoFieldsMatches, Object> {

    private String firstFieldName;
    private String secondFieldName;
    private String thirdFieldName;
    @Autowired
    private DefaultCaptionService defaultCaptionService;
    private static final Logger logger = Logger.getLogger(Class.class.getName());


    @Override
    public void initialize(TwoFieldsMatches constraintAnnotation) {
        firstFieldName = constraintAnnotation.first();
        secondFieldName = constraintAnnotation.second();
        thirdFieldName = constraintAnnotation.third();
    }

    @Override
    public boolean isValid(final Object obj, final ConstraintValidatorContext context) {
        try {
            final String firstObj = BeanUtils.getProperty(obj, firstFieldName);
            final String secondObj = BeanUtils.getProperty(obj, secondFieldName);
            final String thirdObject = (BeanUtils.getProperty(obj, thirdFieldName));
            if (!thirdObject.equals(GOOGLE_USER.toString())) {
                if (firstObj.isEmpty())
                    context.buildConstraintViolationWithTemplate(defaultCaptionService.getCaption("user.error.enter.password")).addPropertyNode(firstFieldName)
                            .addConstraintViolation();
                if (secondObj.isEmpty())
                    context.buildConstraintViolationWithTemplate(defaultCaptionService.getCaption("user.error.enter.password.confirm")).addPropertyNode(secondFieldName)
                            .addConstraintViolation();
                if (firstObj.isEmpty() || secondObj.isEmpty())
                    return false;
                if (!firstObj.equals(secondObj)) {
                    context.disableDefaultConstraintViolation();
                    context.buildConstraintViolationWithTemplate(defaultCaptionService.getCaption("user.error.password.confirm")).addPropertyNode(secondFieldName)
                            .addConstraintViolation();
                    context.buildConstraintViolationWithTemplate("").addPropertyNode(firstFieldName)
                            .addConstraintViolation();
                    return false;
                }
            }
            return true;
        } catch (Exception e) {
            logger.log(Level.SEVERE,e.toString(),e);
            throw new RuntimeException(e);
        }
    }
}
