package com.visma.seli.controllers.customValidators.CustomAuctionValidators;

import com.visma.seli.server.data.access.repository.database.auction.AuctionRepository;
import com.visma.seli.service.captions.DefaultCaptionService;
import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.InvocationTargetException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class EndDateValidator implements ConstraintValidator<ValidEndDate, Object> {

    @Autowired
    private DefaultCaptionService defaultCaptionService;
    @Autowired
    private AuctionRepository auctionRepository;
    private String startDate, endDate, id;
    private int HOURS_IN_A_DAY = 24, HOURS_IN_A_WEEK = 168;
    private static final Logger logger = Logger.getLogger(Class.class.getName());

    @Transactional(propagation = Propagation.REQUIRED)
    private boolean validEndTime(Object objToValidate, ConstraintValidatorContext constraintValidatorContext) throws IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        LocalDateTime startDateTime = (LocalDateTime) PropertyUtils.getSimpleProperty(objToValidate, startDate);
        LocalDateTime endDateTime = (LocalDateTime) PropertyUtils.getSimpleProperty(objToValidate, endDate);
        int auctionId = (int) PropertyUtils.getSimpleProperty(objToValidate, id);
        constraintValidatorContext.disableDefaultConstraintViolation();

        if (endDateTime != null) {
            if (auctionId > 0) {
                if (!isAuctionDurationLessThanHours(HOURS_IN_A_WEEK * 3, startDateTime, endDateTime)) {
                    constraintValidatorContext
                            .buildConstraintViolationWithTemplate(defaultCaptionService.getCaption("update.error.date.cant.exceed.three.weeks"))
                            .addPropertyNode(endDate)
                            .addConstraintViolation();
                    return false;
                }
                if (!isUpdateDateLater(auctionId, endDateTime)) {
                    constraintValidatorContext
                            .buildConstraintViolationWithTemplate(defaultCaptionService.getCaptionWithVariables("update.error.date.must.be.later",
                                    new Object[]{auctionRepository.findAuction(auctionId).getEndDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"))}))
                            .addPropertyNode(endDate)
                            .addConstraintViolation();
                    return false;
                }
            }

            if (startDateTime != null) {
                if (startDateTime.isBefore(endDateTime)) {
                    if (startDateTime.plusHours(2 * HOURS_IN_A_WEEK).isAfter(endDateTime) && startDateTime.plusHours(HOURS_IN_A_DAY).isBefore(endDateTime)) {
                        return true;
                    } else if (startDateTime.plusHours(HOURS_IN_A_DAY).isEqual(endDateTime)) {
                        return true;
                    } else if (startDateTime.plusHours(2 * HOURS_IN_A_WEEK).isEqual(endDateTime)) {
                        return true;
                    } else {
                        printErrorMessage(constraintValidatorContext, "auction.error.end.time.length", endDate);
                        return false;
                    }
                } else {
                    printErrorMessage(constraintValidatorContext, "auction.error.end.time.length", endDate);
                    return false;
                }
            }
            return true;
        } else {
            printErrorMessage(constraintValidatorContext, "auction.error.end.time.not.empty", endDate);
            return false;
        }
    }

    private void printErrorMessage(ConstraintValidatorContext constraintValidatorContext, String message, String node) {
        constraintValidatorContext
                .buildConstraintViolationWithTemplate(defaultCaptionService.getCaption(message))
                .addPropertyNode(node)
                .addConstraintViolation();
    }
    @Transactional(propagation = Propagation.REQUIRED)
    private boolean isUpdateDateLater(int id, LocalDateTime endTime) {
        return !endTime.isBefore(auctionRepository.findAuction(id).getEndDate());
    }

    private boolean isAuctionDurationLessThanHours(int durationHours, LocalDateTime startDate, LocalDateTime endDate) {
        return startDate.plusHours(durationHours).isAfter(endDate);
    }
    @Transactional(propagation = Propagation.REQUIRED)
    private boolean isEndTimeOnUpdateValid(int id, LocalDateTime startTime, LocalDateTime endTime) {
        if (!(startTime.isBefore(endTime) && startTime.plusHours(HOURS_IN_A_WEEK * 3).isAfter(endTime))) {
            return false;
        } else return auctionRepository.findAuction(id).getEndDate().isBefore(endTime);
    }


    @Override
    public void initialize(ValidEndDate constraintAnnotation) {
        startDate = constraintAnnotation.first();
        endDate = constraintAnnotation.second();
        id = constraintAnnotation.third();
    }

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        try {
            return validEndTime(o, constraintValidatorContext);
        } catch (IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            logger.log(Level.SEVERE,e.toString(),e);
            e.printStackTrace();
        }
        return false;
    }

}
