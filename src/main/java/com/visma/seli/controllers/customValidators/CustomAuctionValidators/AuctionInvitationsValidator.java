package com.visma.seli.controllers.customValidators.CustomAuctionValidators;

import com.visma.seli.server.data.access.repository.database.auction.AuctionRepository;
import com.visma.seli.server.data.access.repository.database.auction.invitations.InvitationRepository;
import com.visma.seli.server.data.access.repository.database.user.anonymousUser.AnonymousUserRepository;
import com.visma.seli.server.data.access.repository.database.user.logedInUser.UserRepository;
import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.AuctionService;
import com.visma.seli.service.auction.Invitation;
import com.visma.seli.service.auction.invitations.AuctionInviteService;
import com.visma.seli.service.captions.DefaultCaptionService;
import com.visma.seli.service.exceptions.SeliException;
import com.visma.seli.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.ArrayList;
import java.util.List;

import static java.time.LocalDateTime.now;

@Component
public class AuctionInvitationsValidator implements ConstraintValidator<ValidInvitations, AuctionBody> {

    @Autowired
    private DefaultCaptionService defaultCaptionService;
    @Autowired
    private AuctionService auctionService;
    @Autowired
    private AuctionRepository auctionRepository;
    @Autowired
    private InvitationRepository invitationRepository;
    @Autowired
	private UserService userService;
    @Autowired
	private UserRepository userRepository;
    @Autowired
	private AnonymousUserRepository anonymousUserRepository;
	@Autowired
	private AuctionInviteService auctionInviteService;

	@Override
    public void initialize(ValidInvitations constraintAnnotation) {
    }

    public boolean isValid(final AuctionBody auctionBody, final ConstraintValidatorContext context) {
        return validateAuctionInvitations(auctionBody, context);
    }

    private boolean validateAuctionInvitations(AuctionBody auctionBody, ConstraintValidatorContext context) {
        if (auctionBody.getId() > 0) {
			if (auctionBody.getInvitations() != null) {
				String invitationFailMessage = getInvitationFailMessage(auctionBody, auctionBody.getInvitations());
				if (invitationFailMessage != null) {
					addValidationMessage(context, invitationFailMessage);
					return false;
				}
			}
        }
        if (auctionBody.getInvitations() != null)
			for (Invitation invitation : auctionBody.getInvitations()){
				if (invitation.getEmail().equals(userService.getLoggedInUserEmail())){
					String invitationFailMessage = userService.getLoggedInUserEmail()+" ";
					invitationFailMessage += defaultCaptionService.getCaption("auction.owner.self.invitation.error") + " ";
					auctionBody.getInvitations().remove(invitation);
					addValidationMessage(context, invitationFailMessage);
					return false;
				}
			}
        return true;
    }

    private void addValidationMessage(ConstraintValidatorContext context, String validationMessage) {
        context.buildConstraintViolationWithTemplate(validationMessage)
                .addPropertyNode("invitations").addConstraintViolation();
    }

	public boolean validateInvitation(int id) throws Exception {
		return validateInvitation(id, new ArrayList<Invitation>());
	}

	public boolean validateInvitation(AuctionBody auctionBody) throws Exception {
		return validateInvitation(auctionBody, new ArrayList<Invitation>());
	}
    @Transactional(propagation = Propagation.REQUIRED)
	public boolean validateInvitation(int id, List<Invitation> newInvitations) throws Exception {
		AuctionBody auction = auctionRepository.findAuction(id);
		return validateInvitation(auction, newInvitations);
	}

	public boolean validateInvitation(AuctionBody auction, List<Invitation> newInvitations) throws Exception {

		String failMessage = getInvitationFailMessage(auction, newInvitations);

		if(failMessage != null)
		{
			throw new SeliException(failMessage);
		}

		return false;
	}

	private String getInvitationFailMessage(AuctionBody auction, List<Invitation> newInvitations) {

		String failMessage = null;

		if (newInvitations == null)
			return "Please add at least one invitation";

		auctionInviteService.removeEmptyInvitations(newInvitations);

		if (newInvitations != null) {
			for (int i = 0; i < newInvitations.size(); i++) {
				if (!auction.getIsAnonymous()) {
					if (newInvitations.get(i).getEmail().equals(userRepository.getUserById(auction.getOwnerId()).getEmail())) {
						failMessage = newInvitations.get(i).getEmail() + " " + defaultCaptionService.getCaption("auction.owner.self.invitation.error") + " ";
						return failMessage;
					}
				} else {
					if (newInvitations.get(i).getEmail().equals(anonymousUserRepository.getAnonymousUserById(auction.getOwnerId()).getEmail())) {
						failMessage = newInvitations.get(i).getEmail() + " " + defaultCaptionService.getCaption("auction.owner.self.invitation.error") + " ";
						return failMessage;
					}
				}
			}

			newInvitations = auctionInviteService.checkIfInvitationsExists(newInvitations, auction.getId());

			if (!auctionService.invitationIsValid(auction)) {
				failMessage = "Either auction is not active OR you are not an owner of this auction OR not invited to this auction";
			} else if (!now().isBefore(auction.getStartDate().plusHours(12))) {
				failMessage = "It is not possible to invite to auction if 12 hours passed after auction start time";
			} else
				failMessage = getAlreadyInvitedMessage(auction, newInvitations, failMessage);

		}
		return failMessage;
	}

	private String getAlreadyInvitedMessage(AuctionBody auction, List<Invitation> newInvitations, String failMessage){
    	boolean containsAlreadyInvited = false;

		for(Invitation newInvitation : newInvitations){
			String email = newInvitation.getEmail();
			if(invitationRepository.checkIfUserExistsInInvitation(email, auction.getId(), auction.getIsAnonymous())){
				failMessage = failMessage == null ? "" + email + ", " : failMessage + email + ", ";
				containsAlreadyInvited = true;
			}
		}
		if(containsAlreadyInvited) {
			failMessage.replaceFirst(", $", "");
			failMessage += " already invited, please remove it from list and invite again";
		}
		return failMessage;
	}

}
