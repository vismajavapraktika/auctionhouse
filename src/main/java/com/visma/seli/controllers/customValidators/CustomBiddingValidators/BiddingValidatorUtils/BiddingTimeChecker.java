package com.visma.seli.controllers.customValidators.CustomBiddingValidators.BiddingValidatorUtils;

import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.captions.DefaultCaptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidatorContext;
import java.time.LocalDateTime;

@Component
public class BiddingTimeChecker {

    @Autowired
    private DefaultCaptionService defaultCaptionService;

    public boolean timeBidValidation(ConstraintValidatorContext context, AuctionBody auction) {
        if (LocalDateTime.now().isBefore(auction.getStartDate())) {
            context.buildConstraintViolationWithTemplate(defaultCaptionService.getCaption("bid.error.is.before.now"))
                    .addPropertyNode("amount").addConstraintViolation();
            return false;
        } else if (LocalDateTime.now().isAfter(auction.getEndDate())) {
            context.buildConstraintViolationWithTemplate(defaultCaptionService.getCaption("bid.error.ended"))
                    .addPropertyNode("amount").addConstraintViolation();
            return false;
        } else {
            return true;
        }
    }
}
