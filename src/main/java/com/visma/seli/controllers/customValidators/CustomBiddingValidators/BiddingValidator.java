package com.visma.seli.controllers.customValidators.CustomBiddingValidators;

import com.visma.seli.controllers.customValidators.CustomBiddingValidators.BiddingValidatorUtils.AmountCompareResult;
import com.visma.seli.controllers.customValidators.CustomBiddingValidators.BiddingValidatorUtils.BiddingTimeChecker;
import com.visma.seli.server.data.access.repository.database.auction.AuctionRepository;
import com.visma.seli.server.data.access.repository.database.auction.bids.BidRepository;
import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.bid.Bid;
import com.visma.seli.service.captions.DefaultCaptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.math.BigDecimal;

public class BiddingValidator implements ConstraintValidator<ValidBidding, Bid> {

    @Autowired
    private AuctionRepository auctionRepository;
    @Autowired
    private DefaultCaptionService defaultCaptionService;
    @Autowired
    private BiddingTimeChecker biddingTimeChecker;
    @Autowired
    private AmountCompareResult amountCompareResult;
    @Autowired
    private BidRepository bidRepository;

    @Override
    public void initialize(ValidBidding constraintAnnotation) {
    }


    public boolean isValid(final Bid bid, final ConstraintValidatorContext context) {
        return validateAuctionBid(bid, context);
    }
    @Transactional(propagation = Propagation.REQUIRED)
    private boolean validateAuctionBid(Bid bid, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        AuctionBody auction = auctionRepository.findAuction(bid.getAuctionId());

        if (bid.getBiddingQuantity() == 0 && !auction.isAllowToBidForQuantity()) {
            bid.setBiddingQuantity(1);
        }

        if (bid.getAmount() == null || bid.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
            context.buildConstraintViolationWithTemplate(defaultCaptionService.getCaption("bid.error.notnull"))
                    .addPropertyNode("amount").addConstraintViolation();
            return false;
        } else if (bid.getAmount().compareTo(auction.getStartingPrice()) == -1) {
            context.buildConstraintViolationWithTemplate(defaultCaptionService.getCaption("bid.error.lower.then.auction.price")+ " (" + (auction.getStartingPrice()) + ")")
                    .addPropertyNode("amount").addConstraintViolation();
            return false;
        } else if (!biddingTimeChecker.timeBidValidation(context, auction)) {
            return false;
        } else if (bid.getBiddingQuantity() <= 0) {
            context.buildConstraintViolationWithTemplate(defaultCaptionService.getCaption("bid.error.quantity.notnull"))
                    .addPropertyNode("biddingQuantity").addConstraintViolation();
            return false;
        } else if (bid.getBiddingQuantity() > auction.getItemQuantity()) {
            context.buildConstraintViolationWithTemplate(defaultCaptionService.getCaption("bid.error.biddingQuantity.not.more.then.itemQuantity"))
                    .addPropertyNode("biddingQuantity").addConstraintViolation();
            return false;
        } else if (bidRepository.findAllAuctionBids(bid.getAuctionId()).isEmpty() || bidRepository.findAllAuctionBids(bid.getAuctionId()).equals(null)) {
            return amountCompareResult.getComparingResult(1, bid, context, auction);
        } else {
            return amountCompareResult.getComparingResult(2, bid, context, auction);
        }
    }

}
