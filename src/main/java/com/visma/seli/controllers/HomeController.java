package com.visma.seli.controllers;

import com.google.common.base.Stopwatch;
import com.visma.seli.server.data.access.repository.database.auction.AuctionRepository;
import com.visma.seli.server.data.access.repository.database.auction.bids.BidRepository;
import com.visma.seli.server.data.access.repository.database.user.logedInUser.UserRepository;
import com.visma.seli.service.AuctionSearchService;
import com.visma.seli.service.auction.*;
import com.visma.seli.service.auction.bid.Bid;
import com.visma.seli.service.enums.AuctionStatusEnum;
import com.visma.seli.service.enums.AuctionTypeEnum;
import com.visma.seli.service.enums.InvitationStatusEnum;
import com.visma.seli.service.exceptions.SeliException;
import com.visma.seli.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import static java.util.Arrays.asList;

@EnableAsync
@Controller
public class HomeController implements ErrorController {

    private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger(Class.class.getName());
    @Autowired
    public AuctionRepository auctionRepository;
    @Autowired
    public UserRepository userRepository;
    @Autowired
    public UserService userService;
    @Autowired
    public BidRepository bidRepository;
    @Autowired
    public SortingService sortingService;
    public List<AuctionBody> listAuctions;
    @Autowired
    private AuctionService auctionService;
    @Autowired
    private AuctionSearchService auctionSearchService;
    @Autowired
    private AuctionsFilterService auctionsFilterService;
    private String baseUrl;

    @Transactional(propagation = Propagation.REQUIRED)
    @RequestMapping("/")
    public String home(Principal user, Model model) {
        List<AuctionBody> endingAuctions = auctionRepository
                .findAllAuctionsWithTypesAndStatuses(asList(AuctionTypeEnum.PUBLIC), asList(AuctionStatusEnum.ACTIVE));
        if (userService.getLoggedInUserEmail().equals("anonymousUser")) {
//            return "index";
            return "googleIndex";
        }
        if (endingAuctions.isEmpty()) {
            return "redirect:/createAuction";
        }

        sortingService.sortEndDate(endingAuctions);
        model.addAttribute("allAuctions", endingAuctions);
        model.addAttribute("bidRepository", bidRepository);
        model.addAttribute("userRepository", userRepository);
        model.addAttribute("auctionService", auctionService);
        addItemsForBootstrapCarouselView(model);

//        return "index";
        return "googleIndex";
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @RequestMapping("/home")
    public String seliHome(Principal user, Model model) {
        List<AuctionBody> endingAuctions = auctionRepository
                .findAllAuctionsWithTypesAndStatuses(asList(AuctionTypeEnum.PUBLIC), asList(AuctionStatusEnum.ACTIVE));
        if (userService.getLoggedInUserEmail().equals("anonymousUser")) {
//            return "index";
            return "googleIndex";
        }
        sortingService.sortEndDate(endingAuctions);
        model.addAttribute("allAuctions", endingAuctions);
        model.addAttribute("bidRepository", bidRepository);
        model.addAttribute("userRepository", userRepository);
        model.addAttribute("auctionService", auctionService);
        addItemsForBootstrapCarouselView(model);

//        return "index";
        return "googleIndex";
    }

    private void addItemsForBootstrapCarouselView(Model model) {
        List<AuctionBody> trendingAuctions = bidRepository.findActiveAuctionsOrderedByBidsCount();
        if (!userService.getLoggedInUserEmail().equals("anonymousUser")) {
            List<AuctionBody> myBids = bidRepository.findMyBiddedAuctions(userService.getLoggedInUserId());
            model.addAttribute("myBids", myBids);
        }
        model.addAttribute("trendingAuctions", trendingAuctions);
        model.addAttribute("trendingAuctionsSize", trendingAuctions.size());
        int itemsCountInCarousel = 4;
        model.addAttribute("itemsCountInCarousel", itemsCountInCarousel);
        int carouselViewsCount = trendingAuctions.size() / itemsCountInCarousel;
        if ((trendingAuctions.size() % itemsCountInCarousel) > 0)
            carouselViewsCount += 1;
        model.addAttribute("carouselViewsCount", carouselViewsCount == 0 ? 1 : carouselViewsCount);
    }

    @RequestMapping(value = "/error")
    public String error() {
        return "html/errorPage";
    }

    @Override
    public String getErrorPath() {
        return "html/errorPage";
    }

    @RequestMapping(value = "/auctions/auctions-by-type/{type}/{sort}/filter/{pageNumber}", method = RequestMethod.GET)
    public String filterAuctions(@PathVariable String type, @PathVariable String sort,
                                 @ModelAttribute("filter") AuctionFilter filter,
                                 @RequestParam(value = "currency", required = false) List<String> currencies,
                                 @RequestParam("auctionOwner") String auctionOwnerNameAndSurname, @PathVariable String pageNumber,
                                 Model model) throws SeliException {

        findAuctionsByType(type, sort, pageNumber, model);

        List<AuctionBody> filteredList = auctionsFilterService.filterAuctions(listAuctions, auctionOwnerNameAndSurname, currencies);
        sortingService.sortBy(sort, filteredList);
        addPaginationModelAttributes(filteredList, Integer.valueOf(pageNumber), model);
        addFilterAttributes(filter, model, sort);
        model.addAttribute("sort", sort);
        model.addAttribute("type", type);
        baseUrl = "/auctions/auctions-by-type/" + type + "/" + "{sort}" + "/filter/";
        model.addAttribute("baseUrl", baseUrl);
        return "html/auction/auctionsList";
    }

    private void addFilterAttributes(AuctionFilter filter, Model model, String sort) {
        addAllowedPriceRangeAttributes(model, sort);
        model.addAttribute("filter", filter);
    }

    private void addAllowedPriceRangeAttributes(Model model, String sort) {
        AuctionFilter filter = new AuctionFilter();
        model.addAttribute("filter", filter);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @RequestMapping(value = "/auctions/auctions-by-type/{type}/{sort}/{pageNumber}", method = RequestMethod.GET)
    public String findAuctionsByType(@PathVariable String type, @PathVariable String sort, @PathVariable String pageNumber,
                                     Model model) throws SeliException {
        try {
            int pageNumber1 = Integer.parseInt(pageNumber);

            Stopwatch stopwatch = Stopwatch.createStarted();
            logger.log(Level.INFO, "Start of findAuctionsByType " + userService.getLoggedInUserEmail());
            List<AuctionBody> auctionsToAcceptOrDecline = findAuctionsToAcceptOrDecline();
            model.addAttribute("auctionsToAcceptOrDecline", auctionsToAcceptOrDecline);

            listAuctions = auctionSearchService.findByType(type);
            BigDecimal requiredPageNumber = BigDecimal.valueOf(listAuctions.size()).divide(BigDecimal.valueOf(5), 2);
            requiredPageNumber = requiredPageNumber.setScale(1, RoundingMode.HALF_UP);
            if (!(pageNumber1 == 1 || (pageNumber1 > 1 && pageNumber1 <= requiredPageNumber.intValue()))) {
                throw new SeliException("This page doesn't exists.");
            }

            addBidQuantity(listAuctions);

            sortingService.sortBy(sort, listAuctions);

            addAllowedPriceRangeAttributes(model,sort);
            addPaginationModelAttributes(listAuctions, pageNumber1, model);
            model.addAttribute("sort", sort);
            model.addAttribute("type", type);

            baseUrl = "/auctions/auctions-by-type/" + type + "/" + "{sort}" + "/";
            model.addAttribute("baseUrl", baseUrl);

            stopwatch.stop();
            logger.log(Level.INFO, "time spend in findAuctionsByType: " + stopwatch + " " + userService.getLoggedInUserEmail());
            return "html/auction/auctionsList";
        } catch (NumberFormatException e) {
            throw new SeliException("This page doesn't exists.");
        }
    }

    @Async
    private void addBidQuantity(List<AuctionBody> listAuctions) {
        logger.log(Level.INFO, "Start of addBidQuantity " + userService.getLoggedInUserEmail());
        for (AuctionBody listAuction : listAuctions) {
            listAuction.setBidsQuantity(auctionService.getAmountOfBids(listAuction.getId()));
        }
        logger.log(Level.INFO, "END of addBidQuantity " + userService.getLoggedInUserEmail());
    }

    private Model addPaginationModelAttributes(List<AuctionBody> listAuctions, int pageNumber, Model model) {

        addAuctionsLastBidAttribute(listAuctions, model);
        model.addAttribute("userRepository", userRepository);
        model.addAttribute("auctionService", auctionService);

        int pageSize = 5;

        PagedListHolder<AuctionBody> pagedListHolder = new PagedListHolder<>(listAuctions);
        pagedListHolder.setPageSize(pageSize);
        final int goToPage = pageNumber - 1;
        if (goToPage <= pagedListHolder.getPageCount() && goToPage >= 0) {
            pagedListHolder.setPage(goToPage);
        }

        int current = pagedListHolder.getPage() + 1;
        int begin = Math.max(1, current - 4);
        int end = Math.min(current + 4, pagedListHolder.getPageCount());
        int totalPageCount = pagedListHolder.getPageCount();

        model.addAttribute("beginIndex", begin);
        model.addAttribute("endIndex", end);
        model.addAttribute("currentIndex", current);
        model.addAttribute("totalPageCount", totalPageCount);
        model.addAttribute("allAuctions", pagedListHolder.getPageList());

        return model;
    }

    private void addAuctionsLastBidAttribute(List<AuctionBody> listAuctions, Model model) {
        Map<Integer, Bid> auctionIdToBid = new HashMap<Integer, Bid>();
        for (AuctionBody auction : listAuctions) {
            int id = auction.getId();
            auctionIdToBid.put(id, bidRepository.findHighestBid(id));
        }
        model.addAttribute("auctionTolastBid", auctionIdToBid);
    }

    private List<AuctionBody> findAuctionsToAcceptOrDecline() {

        return auctionRepository.findUserPrivateAuctions(userService.getLoggedInUserEmail(),
                InvitationStatusEnum.NO_RESPONSE, asList(AuctionStatusEnum.ACTIVE));
    }
}