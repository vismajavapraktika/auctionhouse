package com.visma.seli.controllers.view.models;

import com.visma.seli.server.data.access.repository.database.auction.AuctionRepository;
import com.visma.seli.server.data.access.repository.database.auction.bids.BidRepository;
import com.visma.seli.server.data.access.repository.database.auction.won.WonAuctionsRepository;
import com.visma.seli.server.data.access.repository.database.pictures.PictureRepository;
import com.visma.seli.server.data.access.repository.database.user.logedInUser.UserRepository;
import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.AuctionService;
import com.visma.seli.service.auction.bid.AuctionBidService;
import com.visma.seli.service.auction.bid.Bid;
import com.visma.seli.service.auction.bid.WinMessageData;
import com.visma.seli.service.user.UserBody;
import com.visma.seli.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import static com.visma.seli.service.enums.AuctionStatusEnum.ENDED;

@Service
public class AuctionSearchViewModelService {

    @Autowired
    private AuctionService auctionService;
    @Autowired
    private AuctionRepository auctionRepository;
    @Autowired
    private BidRepository bidRepository;

    @Autowired
    private WonAuctionsRepository wonAuctionsRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private PictureRepository pictureRepository;
    @Autowired
    private AuctionBidService auctionBidService;

    private void addLoggedInUserInformation(int id, Model model) {
        if (auctionService.isLoggedInUserAuctionOwner(id)) {
            model.addAttribute("loggedInUserIsAuctionOwner", true);
        } else
            model.addAttribute("loggedInUserIsAuctionOwner", false);
    }

    private void addHighestBidInformation(int auctionId, Model model) {
        Bid highestBid = bidRepository.findHighestBid(auctionId);
        if (highestBid != null) {
            model.addAttribute("highestBid", highestBid.getAmount());
            model.addAttribute("highestBidder", bidRepository.findHighestBidder(auctionId));
        } else {
            model.addAttribute("lastBidAmount", 0);
        }
        model.addAttribute("amountOfBids", auctionService.getAmountOfBids(auctionId));
    }
    @Transactional(propagation = Propagation.REQUIRED)
    public void getModelForViewAuctionController(int auctionId, Model model) {
        AuctionBody auctionBody = auctionRepository.findAuction(auctionId);

        model.addAttribute("auction", auctionBody);
        model.addAttribute("timeRemaining", auctionService.getSecondsRemaining((auctionBody)));
        model.addAttribute("status",auctionBody.getStatus());
        addHighestBidInformation(auctionId, model);
        addUsersHighestBid(auctionId,model);
        addLoggedInUserInformation(auctionId, model);
        model.addAttribute("numberOfAuctionPictures", pictureRepository.findAuctionPicturesCount(auctionId));
    }

    private void addUsersHighestBid(int auctionId, Model model) {
        model.addAttribute("highestUserBid", auctionBidService.highestBidForUser(auctionId));
    }

    public boolean isUserCurrentWinner(int id) {
        WinMessageData currentWinner = wonAuctionsRepository.getCurrentWinnerByAuctionId(id);
        if (currentWinner == null) {
            return false;
        }
        UserBody loggedUser = userRepository.getUserByEmail(userService.getLoggedInUserEmail());
        return currentWinner.getBid().getUserId() == loggedUser.getId();

    }
    @Transactional(propagation = Propagation.REQUIRED)
    public boolean isLoggedInUserEndedAuctionOwner(int id) {
        UserBody loggedUser = userRepository.getUserByEmail(userService.getLoggedInUserEmail());
        UserBody owner = userRepository.getOwnerByAuctionId(id);
        AuctionBody auction = auctionRepository.findAuction(id);
        return loggedUser.getId() == owner.getId() && auction.getStatus() == ENDED;

    }

}
