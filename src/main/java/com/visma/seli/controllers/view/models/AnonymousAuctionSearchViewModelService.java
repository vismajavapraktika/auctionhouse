package com.visma.seli.controllers.view.models;

import com.visma.seli.server.data.access.repository.database.auction.AuctionRepository;
import com.visma.seli.server.data.access.repository.database.auction.bids.BidRepository;
import com.visma.seli.server.data.access.repository.database.pictures.PictureRepository;
import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.AuctionService;
import com.visma.seli.service.auction.bid.Bid;
import com.visma.seli.service.enums.UserEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import java.util.UUID;

@Service
public class AnonymousAuctionSearchViewModelService {

    @Autowired
    private AuctionRepository auctionRepository;
    @Autowired
    private AuctionService auctionService;
    @Autowired
    private BidRepository bidRepository;
    @Autowired
    private PictureRepository pictureRepository;

    private void addLoggedInUserInformation(int id, Model model) {
        model.addAttribute("loggedInUserIsAuctionOwner", false);
    }

    private void addHighestBidInformation(int auctionId, Model model) {
        Bid highestBid = bidRepository.findHighestBid(auctionId);
        if (highestBid != null) {
            model.addAttribute("highestBid", highestBid.getAmount());
            model.addAttribute("highestBidder", bidRepository.findHighestBidder(auctionId));
        } else {
            model.addAttribute("lastBidAmount", 0);
        }
        model.addAttribute("amountOfBids", auctionService.getAmountOfBids(auctionId));
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void getModelForViewAnonymousAuctionController(UUID id, Model model) {
        AuctionBody auctionBody = auctionRepository.findAnonymousAuction(id, UserEnum.INVITED);

        model.addAttribute("auction", auctionBody);
        model.addAttribute("timeRemaining", auctionService.getSecondsRemaining((auctionBody)));
        model.addAttribute("status",auctionBody.getStatus());
        addHighestBidInformation(auctionBody.getId(), model);
        addLoggedInUserInformation(auctionBody.getId(), model);
        model.addAttribute("status",auctionBody.getStatus());
        model.addAttribute("numberOfAuctionPictures", pictureRepository.findAuctionPicturesCount(auctionBody.getId()));
    }
    @Transactional(propagation = Propagation.REQUIRED)
    public void getModelForViewAnonymousOwnerAuctionController(UUID id, Model model) {
        AuctionBody auctionBody = auctionRepository.findAnonymousAuction(id, UserEnum.OWNER);
        model.addAttribute("auction", auctionBody);
        model.addAttribute("timeRemaining", auctionService.getSecondsRemaining((auctionBody)));
        model.addAttribute("status",auctionBody.getStatus());
        model.addAttribute("numberOfAuctionPictures", pictureRepository.findAuctionPicturesCount(auctionBody.getId()));
        addHighestBidInformation(auctionBody.getId(), model);
        addLoggedInUserInformation(auctionBody.getId(), model);
    }
}