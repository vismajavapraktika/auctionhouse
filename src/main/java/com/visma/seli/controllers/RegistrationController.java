package com.visma.seli.controllers;

import com.visma.seli.server.data.access.repository.database.user.logedInUser.UserRepository;
import com.visma.seli.service.emailService.EmailService;
import com.visma.seli.service.enums.UserTypes;
import com.visma.seli.service.user.PassHasher;
import com.visma.seli.service.user.UserBody;
import com.visma.seli.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.xbill.DNS.TextParseException;

import javax.validation.Valid;

@Controller
public class RegistrationController {

    @Autowired
    PassHasher hashPass;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private EmailService emailService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registrationPage(Model model) {
        if (!userService.getLoggedInUserEmail().equals("anonymousUser")){
            return "redirect:/home";
        }
        return "html/registration";
    }

    @ModelAttribute("user")
    public UserBody getUser() {
        return new UserBody(UserTypes.FULL_ACCESS_USER);
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String saveNewUser(@Valid @ModelAttribute("user") UserBody user, BindingResult bindingResult) throws TextParseException {
        if (emailService.checkIfEmailIsGoogleMX(user.getEmail())){
            return "redirect:/login/google";
        }
        if (bindingResult.hasErrors()) {
            user.setPassword("");
            return "html/registration";
        }
        hashPass.hashThePass(user);
        userRepository.insertNewUser(user);
        return "html/success";
    }

}