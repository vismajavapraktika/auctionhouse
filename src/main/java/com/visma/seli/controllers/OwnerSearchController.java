package com.visma.seli.controllers;

import com.visma.seli.server.data.access.repository.database.user.logedInUser.UserRepository;
import com.visma.seli.service.AuctionSearchService;
import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.user.UserBody;
import com.visma.seli.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;


@Controller
public class OwnerSearchController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuctionSearchService auctionSearchService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/getTags", method = RequestMethod.GET) // TODO: 2017-02-02 breaks
    @ResponseBody
    public Set<String> execute(@RequestParam("query") String query, @RequestParam("type") String type) {

        Set<String> result = new HashSet<String>();

        if(query == null || query.isEmpty() || query.length() < 3) {
            return result;
        }

        String[] queryWords = query.split(" ");

        List<AuctionBody> auctions = auctionSearchService.findByType(type);

        for (UserBody userBody : userRepository.getUsersByIds(getOwners(auctions))) {
            String userName = userBody.getName();
            String surname = userBody.getSurname();

            if(userService.nameOrSurnameContainsQueryWords(queryWords, userName, surname))
                result.add(userName + " " + surname);
        }

        return result;
    }


    private List<Integer> getOwners(List<AuctionBody> auctions) {
        List<Integer> owners = new ArrayList<>();
        for(AuctionBody auction : auctions)
        {
            owners.add(auction.getOwnerId());
        }
        return owners;
    }
}
