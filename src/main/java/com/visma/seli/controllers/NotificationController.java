package com.visma.seli.controllers;

import com.visma.seli.server.data.access.repository.database.NotificationsRepository;
import com.visma.seli.server.data.access.repository.database.auction.AuctionRepository;
import com.visma.seli.service.Notifications;
import com.visma.seli.service.mailing.NotificationService;
import com.visma.seli.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class NotificationController {

    @Autowired
    private UserService userService;
    @Autowired
    private NotificationsRepository notificationsRepository;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private AuctionRepository auctionRepository;


    @ModelAttribute("notifications")
    public Notifications getNotifications() {
        return new Notifications();
    }

    @RequestMapping(value = "/notify-user-about-won-auctions", method = RequestMethod.GET)
    @ResponseBody
    public String notificationCounter() {
        String loggedInUserEmail = userService.getLoggedInUserEmail();
        List<Notifications> listOfNotifications = notificationsRepository.getNotificationsByEmail(loggedInUserEmail);
        if (listOfNotifications.size() > 0) {
            return String.valueOf(listOfNotifications.size());
        }
        return "";
    }
    @Transactional(propagation = Propagation.REQUIRED)
    @RequestMapping(value = "/notifications/getList", method = RequestMethod.GET)
    @ResponseBody
    public List<Notifications> notificationList() {
        String loggedInUserEmail = userService.getLoggedInUserEmail();
        List<Notifications> listOfNotifications = notificationsRepository.getNotificationsByEmail(loggedInUserEmail);
        if (listOfNotifications.size() > 0) {
            for (Notifications listOfNotification : listOfNotifications) {
                String auctionTitle = auctionRepository.findAuction(listOfNotification.getNotificationId()).getTitle();
                listOfNotification.getNotificationTitle(auctionTitle);
            }
            return listOfNotifications;
        }
        return null;
    }
    
    @RequestMapping(value = "/notifications/getList/changeNotificationStatus", method = RequestMethod.GET)
    // TODO: 2017-02-02 breaks if you just enter this one
    @ResponseBody
    public void changeNotificationStatus(@RequestParam("notificationId") int notificationId) {
    	notificationsRepository.deleteNotification(notificationId);
    }
}