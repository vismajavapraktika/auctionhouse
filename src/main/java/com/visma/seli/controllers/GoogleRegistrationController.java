package com.visma.seli.controllers;

import com.visma.seli.server.data.access.repository.database.user.logedInUser.UserRepository;
import com.visma.seli.server.login.google.GoogleAuth;
import com.visma.seli.server.login.google.GoogleLoginInfo;
import com.visma.seli.service.user.PassHasher;
import com.visma.seli.service.user.UserBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import static com.visma.seli.service.enums.UserTypes.GOOGLE_USER;

@Controller
public class GoogleRegistrationController {

    @Autowired
    PassHasher hashPass;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private GoogleAuth googleAuth;

    @RequestMapping(value = "/googleRegistration", method = RequestMethod.GET)
    public String googleRegistrationPage(Model model) {
        // DO NOT DELETE THIS PART!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        /*GoogleLoginInfo googleUser = googleAuth.getGoogleUser();

        if (googleUser != null) {
            String email = googleUser.getEmails().iterator().next().getValue();

            if (userRepository.getUserByEmail(email) == null) {
                UserBody userBody = new UserBody(0, googleUser.getName().getGivenName(),
                        googleUser.getName().getFamilyName(), email, "", GOOGLE_USER);
                model.addAttribute("user", userBody);
            }

            return "html/googleRegistration";
        }

        return "html/success";*/
        // DO NOT DELETE THIS PART!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        GoogleLoginInfo googleUser = googleAuth.getGoogleUser();

        if (googleUser != null) {
            String email = googleUser.getEmails().iterator().next().getValue();
            String [] checkIfVismaEmail = email.split("@");
            if (checkIfVismaEmail[1].equals("visma.com")) {
                if (userRepository.getUserByEmail(email) == null) {
                    UserBody userBody = new UserBody(0, googleUser.getName().getGivenName(),
                            googleUser.getName().getFamilyName(), email, "", GOOGLE_USER);
                    String submittedEmail = userBody.getEmail();
                    if (submittedEmail != null && submittedEmail.equals(email)) {
                        userBody.setPhone(null);
                        userRepository.insertNewUser(userBody);
                    }
                }

                return "html/success";
            }
            else {
                return "html/logout";
            }
        }
        return "html/success";
    }

    // DO NOT DELETE THIS PART!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    /*@RequestMapping(value = "/googleRegistration", method = RequestMethod.POST)
    public String saveNewGoogleUser(@Valid @ModelAttribute("user") UserBody user, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            user.setPassword("");
            return "html/googleRegistration";
        }
//        hashPass.hashThePass(user);
        String googleEmail = googleAuth.getGoogleUser().getEmails().iterator().next().getValue();
        String submittedEmail = user.getEmail();
        if (submittedEmail != null && submittedEmail.equals(googleEmail)) {
            userRepository.insertNewUser(user); // was google user
        }

        return "html/success";
    }*/
// DO NOT DELETE THIS PART!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
}