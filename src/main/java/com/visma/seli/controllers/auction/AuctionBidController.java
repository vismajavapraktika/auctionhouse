package com.visma.seli.controllers.auction;

import com.visma.seli.server.data.access.repository.database.auction.AuctionRepository;
import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.AuctionService;
import com.visma.seli.service.auction.bid.AuctionBidService;
import com.visma.seli.service.auction.bid.Bid;
import com.visma.seli.service.exceptions.SeliException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.math.BigDecimal;
import java.util.UUID;

@Controller
public class AuctionBidController {

    @Autowired
    private AuctionService auctionService;
    @Autowired
    private AuctionRepository auctionRepository;
    @Autowired
    private AuctionBidService auctionBidService;

    @Transactional(propagation = Propagation.REQUIRED)
    @RequestMapping(value = "/auction/placeBid/{id}", method = RequestMethod.POST)
    public String saveAuctionBid(@PathVariable int id, @Valid @ModelAttribute("auctionBid") Bid auctionBid,
                                 BindingResult bindingResult, HttpServletRequest request, final RedirectAttributes redirectAttributes) {
        return auctionBidService.saveAuctionBid(id, request, bindingResult,auctionBid,redirectAttributes);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @RequestMapping(value = "/auction/placeBidAnonymously/{userUuid}", method = RequestMethod.POST)
    public String saveAuctionBidAnonymously(@PathVariable UUID userUuid, @Valid @ModelAttribute("auctionBid") Bid auctionBid,
                                            BindingResult bindingResult, HttpServletRequest request, final RedirectAttributes redirectAttributes) {
        return auctionBidService.saveAnonymousAuction(userUuid, request, bindingResult, auctionBid, redirectAttributes);
    }


    @Transactional(propagation = Propagation.REQUIRED)
    @RequestMapping("/auction/buyout/{id}")
    public String buyOutAuction(@PathVariable int id) throws SeliException {
    	AuctionBody auction = auctionRepository.findAuction(id);
    	if(auctionService.isAuctionEnded(auction)
                || auctionService.isLoggedInUserAuctionOwner(id)
                || auction.getBuyOutPrice() == null
                || auction.getBuyOutPrice().compareTo(new BigDecimal(0)) <= 0)
    	    throw new SeliException("This auction has no buyout possibility.");

        auctionService.buyOutAuction(auction);
        
        return "redirect:/home";
    }
}