package com.visma.seli.controllers.auction;

import com.visma.seli.server.data.access.repository.database.pictures.PictureRepository;
import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.AuctionService;
import com.visma.seli.service.auction.Picture;
import com.visma.seli.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

@Controller
public class CreateAuctionController {

    private final String AUCTION_TYPE = "FullAuction";
    @Autowired
    private AuctionService auctionService;
    @Autowired
    private PictureRepository pictureRepository;
    @Autowired
    private UserService userService;

    private static final Logger logger = Logger.getLogger(Class.class.getName());

    @RequestMapping(value = "/createAuction", method = RequestMethod.GET)
    public String createNewAuction() {
        return "html/auction/createAuction";
    }


	@ModelAttribute("auction")
    public AuctionBody getAuction() {
        return new AuctionBody();
    }

    @ModelAttribute("picture")
    public Picture getPicture() {return new Picture();}

    @RequestMapping(value = "/createAuction", method = RequestMethod.POST)
    public String saveAuction(@Valid @ModelAttribute("auction") AuctionBody auctionBody, BindingResult bindingResult,
                              Model model, final RedirectAttributes redirectAttributes) throws Exception {

        auctionService.addNewPictures(auctionBody.getPictureFiles(), userService.getLoggedInUserEmail(), model);
        if (bindingResult.hasErrors()) {
            logger.log(Level.WARNING, "Auction create has error");
            return "html/auction/createAuction";
        }

        auctionService.saveCreatedAuction(auctionBody, model, redirectAttributes, AUCTION_TYPE);
        return "redirect:/auction/" + auctionBody.getId() + "/";
    }


    @RequestMapping(value = "/create-auction/remove-picture", method = RequestMethod.GET)
    public
    @ResponseBody
    void removePicture(@RequestParam("pictureUUID") UUID pictureUUID) {
        pictureRepository.deleteTemporaryPicture(pictureUUID);
    }

}