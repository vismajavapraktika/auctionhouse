package com.visma.seli.controllers.auction;

import com.visma.seli.service.auction.comment.AuctionCommentService;
import com.visma.seli.service.auction.comment.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.UUID;

@Controller
public class AuctionCommentController {

    @Autowired
    private AuctionCommentService commentService;

    @Transactional(propagation = Propagation.REQUIRED)
    @RequestMapping(value = "/auction/{id}/post-comment", method = RequestMethod.POST)
    public String saveAuctionComment(@PathVariable int id, @Valid @ModelAttribute("newComment") Comment auctionComment,
                                 BindingResult bindingResult, HttpServletRequest request, final RedirectAttributes redirectAttributes) {

        return commentService.saveAuctionComment(bindingResult,id,auctionComment);
    }
    
    @RequestMapping(value = "/anonymousAuction/{userUuid}/post-comment", method = RequestMethod.POST)
    public String saveAnonymousAuctionComment(@PathVariable UUID userUuid, @Valid @ModelAttribute("newComment") Comment auctionComment,
                                 BindingResult bindingResult, HttpServletRequest request, final RedirectAttributes redirectAttributes) {
        return commentService.saveAnonymousAuctionComment(bindingResult, userUuid,auctionComment);
    }
}