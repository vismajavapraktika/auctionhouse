package com.visma.seli.controllers.auction;

import com.visma.seli.server.data.access.repository.database.auction.AuctionRepository;
import com.visma.seli.server.data.access.repository.database.auction.invitations.InvitationRepository;
import com.visma.seli.server.data.access.repository.database.pictures.PictureRepository;
import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.AuctionService;
import com.visma.seli.service.auction.Picture;
import com.visma.seli.service.auction.anonymous.AnonymousAuctionService;
import com.visma.seli.service.enums.InvitationStatusEnum;
import com.visma.seli.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Controller
public class EditAuctionController {

    @Autowired
    private AuctionRepository auctionRepository;
    @Autowired
    private AuctionService auctionService;
    @Autowired
    private UserService userService;
    @Autowired
    private InvitationRepository invitationRepository;
    @Autowired
    private AnonymousAuctionService anonymousAuctionService;
    @Autowired
    private PictureRepository pictureRepository;

    @Transactional(propagation = Propagation.REQUIRED)
    @RequestMapping(value = "/auction/update/{id}", method = RequestMethod.GET)
    public String editAuctionPage(@PathVariable int id, Model model) {
        if (!auctionService.isLoggedInUserAuctionOwner(id) || auctionService.isAuctionEnded(auctionRepository.findAuction(id)))
            return "redirect:/auction/" + id + "/";
        addAuctionPictures(id, model);
        AuctionBody auctionBody = auctionRepository.findAuction(id);
        int numberOfBids = auctionService.getAmountOfBids(id);
        model.addAttribute("auction", auctionBody);
        model.addAttribute("numberOfBids", numberOfBids);
        return "html/auction/editAuction";
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @RequestMapping(value = "/auction/update/{id}", method = RequestMethod.POST)
    public String saveEditedAuction(@PathVariable int id, @Valid @ModelAttribute("auction") AuctionBody auctionBody,
                                    BindingResult result, Model model, final RedirectAttributes redirectAttributes) {

        if (auctionService.isLoggedInUserAuctionOwner(id) && !auctionService.isAuctionEnded(auctionRepository.findAuction(id))) {
            auctionService.addNewPictures(auctionBody.getPictureFiles(), userService.getLoggedInUserEmail(), model);
            addAuctionPictures(id, model);
            if (result.hasErrors()) {
                int numberOfBids = auctionService.getAmountOfBids(id);
                model.addAttribute("numberOfBids", numberOfBids);
                return "html/auction/editAuction";
            }
            auctionService.saveUpdatedAuction(auctionBody, id);
            redirectAttributes.addFlashAttribute("removePicturesFromLocalStorage", true);
        }
        return "redirect:/auction/" + id + "/";
    }

    private void addAuctionPictures(int id, Model model) {
        List<Picture> listOfPictures = pictureRepository.getPictureList(id);
        model.addAttribute("pictureList", listOfPictures);
    }

    @RequestMapping(value = "/delete-photos/{pictureId}")
    public String deletePhotos(@PathVariable int pictureId) {
        int auctionId = pictureRepository.getAuctionPictureByPictureId(pictureId).getPhotoAuctionId();
        if (auctionService.isLoggedInUserAuctionOwner(auctionId))
            pictureRepository.deletePicture(pictureId);
        return "redirect:/auction/update/" + auctionId;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @RequestMapping(value = "/auction/delete/{id}")
    public String deleteAuction(@PathVariable int id) {
        return auctionService.deleteAuction(id);
    }

    @RequestMapping(value = "/auction/deleteAnonymous/{userUuid}")
    public String deleteAnonymousAuction(@PathVariable UUID userUuid) {
        anonymousAuctionService.deleteAnonymousAuction(userUuid);
        return "redirect:/home";
    }

    @RequestMapping("/auction/{id}/accept-invitation")
    public String acceptInvitation(@PathVariable int id) {
        if (auctionService.isLoggedInUserInvitedToAuction(id))
            invitationRepository.updateInvitation(id, userService.getLoggedInUserEmail(), InvitationStatusEnum.ACCEPTED);
        return "redirect:/auction/" + id + "/";
    }

    @RequestMapping("/auction/{id}/decline-invitation")
    public String declineInvitation(@PathVariable int id) {
        if (auctionService.isLoggedInUserInvitedToAuction(id))
            invitationRepository.updateInvitation(id, userService.getLoggedInUserEmail(), InvitationStatusEnum.DECLINED);
        return "redirect:/auctions/page/All/1";
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @RequestMapping(value = "/auction/{id}/insert-temporary-picture", method = RequestMethod.POST)
    @ResponseBody
    public Picture anonymousPictureTemporarySave(@PathVariable int id, @RequestParam("anonymousPicture") MultipartFile picture) throws IOException {
        return pictureRepository.insertTemporaryPicture(picture.getOriginalFilename(), auctionService.resize(picture).getBytes(), auctionRepository.findAuction(id).getOwnerId());
    }

}