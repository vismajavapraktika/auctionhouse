package com.visma.seli.controllers.auction;

import com.visma.seli.controllers.customValidators.CustomAuctionValidators.AuctionInvitationsValidator;
import com.visma.seli.server.data.access.repository.database.NotificationsRepository;
import com.visma.seli.server.data.access.repository.database.auction.AuctionRepository;
import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.AuctionService;
import com.visma.seli.service.auction.invitations.AuctionInviteService;
import com.visma.seli.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import static java.time.LocalDateTime.now;



@Controller
public class AuctionInviteController {

	@Autowired
	private AuctionRepository auctionRepository;
	@Autowired
	private AuctionService auctionService;
	@Autowired
	private AuctionInvitationsValidator auctionInvitationsValidator;
	@Autowired
	private NotificationsRepository notificationsRepository;
	@Autowired
	private AuctionInviteService auctionInviteService;
	@Autowired
	private UserService userService;

	@ModelAttribute("auction")
    public AuctionBody getAuction() {
        AuctionBody auctionBody = new AuctionBody();
        auctionBody.setAnonymous(false);
		return auctionBody;
    }
    
	@RequestMapping(value = "/auction/invited/{id}", method = RequestMethod.POST)
	public String inviteToAuction(@PathVariable int id, @ModelAttribute("auction") AuctionBody auctionBody, Model model) throws Exception {
		if(auctionService.isLoggedInUserInvitedToAuction(id) && auctionBody.getType().toString().equals("PRIVATE") || auctionService.isLoggedInUserAuctionOwner(id)) {
			auctionInviteService.inviteToAuction(id, auctionBody);
		}
		return "redirect:/auction/{id}/";
	}

	@RequestMapping(value = "/go-back/auction/invited/{id}", method = RequestMethod.GET)
	public String goBackToAuction(@PathVariable int id, Model model){
    	model.addAttribute("id",id);
    	return "redirect:/auction/"+id+"/";
	}

	@RequestMapping(value = "/go-back/auction/{id}", method = RequestMethod.POST)
	public String goBackAfterExistingInvitation(@PathVariable int id, Model model){
		model.addAttribute("failMessage",null);
		model.addAttribute("id",id);
		return "html/auction/invite";
	}

	@RequestMapping(value = "/auction/{id}/reinvite/{invitationId}", method = RequestMethod.GET)
	public String reInviteToAuction(@PathVariable int id, @PathVariable int invitationId, Model model) {
		auctionInviteService.reinviteToAuction(id, invitationId);
		return "redirect:/auction/" + id + "/";
	}

	@RequestMapping(value = "/auction/invite/{id}", method = RequestMethod.GET)
	public String inviteToAuction(@PathVariable int id, Model model) throws Exception {
		auctionInvitationsValidator.validateInvitation(id);
		return "html/auction/invite";
	}
    @Transactional(propagation = Propagation.REQUIRED)
	@RequestMapping(value = "auction/invitation-button", method = RequestMethod.GET)
	public @ResponseBody boolean invitationButtonShow(@RequestParam("id") int id) {
		return auctionRepository.findAuction(id).getStartDate().plusHours(12).isAfter(now());
	}
}
