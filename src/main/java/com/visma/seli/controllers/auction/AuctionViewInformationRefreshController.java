package com.visma.seli.controllers.auction;

import com.visma.seli.server.data.access.repository.database.auction.bids.BidRepository;
import com.visma.seli.service.auction.AuctionService;
import com.visma.seli.service.auction.bid.AuctionBidService;
import com.visma.seli.service.auction.bid.Bid;
import com.visma.seli.service.user.UserBody;
import com.visma.seli.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@Transactional
public class AuctionViewInformationRefreshController {

    @Autowired
    private BidRepository bidRepository;
    @Autowired
    private AuctionService auctionService;
    @Autowired
    private AuctionBidService auctionBidService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/auction/refresh-bid-information")
    @ResponseBody
    public List<Object> refreshBidInformation(@RequestParam(value="id", required=true) int auctionId){
        List<Object> list = new ArrayList<>();
        if (!userService.getLoggedInUserEmail().equals("anonymousUser")) {
            list.add(highestBidRefresh(auctionId));
            list.add(bidsCounterRefresh(auctionId));
            list.add(highestBidderInformation(auctionId));
            list.add(highestLoggedInUserBidOnAuction(auctionId));
        }
        return list;
    }

    private String highestBidRefresh(int auctionId){
        Bid bid = bidRepository.findHighestBid(auctionId);
        if (bid == null){
            return "";
        }
        return String.valueOf(bid.getAmount());
    }

    private int bidsCounterRefresh( int auctionId){
        return auctionService.getAmountOfBids(auctionId);
    }


    private String highestBidderInformation(int auctionId){
        UserBody highestBidder = bidRepository.findHighestBidder(auctionId);
        if (highestBidder == null) {
            return "";
        }
        return (highestBidder.getName()+ " " + highestBidder.getSurname());
    }

    private String highestLoggedInUserBidOnAuction(int auctionId){
        return auctionBidService.highestBidForUser(auctionId);
    }
}
