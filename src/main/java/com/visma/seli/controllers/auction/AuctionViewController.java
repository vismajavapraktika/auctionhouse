package com.visma.seli.controllers.auction;

import com.visma.seli.controllers.view.models.AuctionSearchViewModelService;
import com.visma.seli.server.data.access.repository.database.auction.AuctionRepository;
import com.visma.seli.server.data.access.repository.database.auction.comment.CommentRepository;
import com.visma.seli.server.data.access.repository.database.user.logedInUser.UserRepository;
import com.visma.seli.server.data.access.repository.database.user.rating.UserRatingRepository;
import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.AuctionService;
import com.visma.seli.service.auction.WonAuctionService;
import com.visma.seli.service.auction.bid.AuctionBidService;
import com.visma.seli.service.auction.bid.Bid;
import com.visma.seli.service.auction.comment.Comment;
import com.visma.seli.service.exceptions.SeliException;
import com.visma.seli.service.user.UserBody;
import com.visma.seli.service.user.UserService;
import com.visma.seli.service.user.rating.UserRatingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;

import static com.visma.seli.service.enums.AuctionStatusEnum.ENDED;

// TODO: 2016-12-02 Rewrite controller insertInformationToModel -> to service

@Controller
public class AuctionViewController {

    @Autowired
    private AuctionBidService auctionBidService;
    @Autowired
    private AuctionService auctionService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private AuctionSearchViewModelService auctionSearchViewModelService;
    @Autowired
    private WonAuctionService wonAuctionService;
    @Autowired
    private UserRatingRepository userRatingRepository;
    @Autowired
    private UserRatingService userRatingService;
    @Autowired
    private AuctionRepository auctionRepository;
    @Autowired
    private CommentRepository commentRepository;

    @RequestMapping("/auction/{id}")
    public String viewAuction(@PathVariable int id, Model model) throws SeliException {
        return auctionView(id, model);
    }

    private String auctionView(int id, Model model) throws SeliException {
        AuctionBody auctionBody = auctionRepository.findAuction(id);
        if (auctionBody.getType().toString().equals("PRIVATE") || auctionBody.getStatus().toString().equals("DELETED")) {
            if (!auctionService.isLoggedInUserAuctionOwner(id)){
                if (!auctionService.isLoggedInUserInvitedToAuction(id)){
                    throw new SeliException("This page does not exist or you don't have permission to see it");
                }
            }
        }

        if (auctionBody.getLink()==null || auctionBody.getLink().equals(""))
            auctionBody.setLink(null);

        auctionSearchViewModelService.getModelForViewAuctionController(id, model);
        int bidderId = userRepository.getUserByEmail(userService.getLoggedInUserEmail()).getId();
        insertInformationToModel(model,bidderId,id);
        model.addAttribute("auctionStarted", LocalDateTime.now().isAfter(auctionBody.getStartDate()));

        return "html/auction/auctionView";
    }

    private void insertInformationToModel(Model model, int bidderId, int id) {
        if (!model.containsAttribute("auctionBid")) {
            Bid bid = new Bid(id, bidderId, null);
            model.addAttribute("auctionBid", bid);
        }
        Boolean isUserCurrentWinner = auctionSearchViewModelService.isUserCurrentWinner(id);
        Boolean isUserEndedAuctionOwner = auctionSearchViewModelService.isLoggedInUserEndedAuctionOwner(id);

        model.addAttribute("isLoggedInUserInvitedToAuction", auctionService.isLoggedInUserInvitedToAuction(id));
        model.addAttribute("auctionBidContext", auctionBidService.createBidContext(id));
        model.addAttribute("isUserCurrentWinner", isUserCurrentWinner);
        model.addAttribute("isUserEndedAuctionOwner", isUserEndedAuctionOwner);
        UserBody auctionOwner = auctionService.getAuctionOwner(id);
        model.addAttribute("auctionOwner", auctionOwner);
        model.addAttribute("givenRating", userRatingRepository.findRating(userService.getLoggedInUserId(), auctionOwner.getId(),id));
        model.addAttribute("avarageRating", userRatingRepository.findUserAvarageRating(auctionOwner.getId()));
        model.addAttribute("newComment", new Comment());
        model.addAttribute("comments", commentRepository.findAllAuctionComments(id));
    }

    @ResponseBody
    @RequestMapping(value = "/auction/picture/{id}/{pictureId}", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    // TODO: 2017-02-02 If auction is private you can see picture - change this 
    // TODO: 2017-02-02 You can get pictures without being logged in
    public ResponseEntity<byte[]> getQRImage(HttpServletRequest request, @PathVariable int id, @PathVariable int pictureId) throws NoSuchAlgorithmException {
        AuctionBody auctionBody = auctionRepository.findAuction(id);
        if (!auctionService.checkIfUserCanSeePictures(auctionBody)){
            return null;
        }

        return auctionService.getImageResponseEntity(id, pictureId);
    }

    @RequestMapping("/auction/{id}/accept-win") 
    public String acceptWin(@PathVariable int id) {
        if (auctionRepository.findAuction(id).getStatus().equals(ENDED)) // TODO: 2017-02-03 Check if this if is correct 
            wonAuctionService.acceptWin(id, userRepository.getUserByEmail(userService.getLoggedInUserEmail()).getId());
        return "redirect:/home";
    }

    @RequestMapping("/auction/{id}/decline-win") 
    public String declineWin(@PathVariable int id) {
        if (auctionRepository.findAuction(id).getStatus().equals(ENDED)) // TODO: 2017-02-03 Check if this if is correct
            wonAuctionService.declineWin(id, userRepository.getUserByEmail(userService.getLoggedInUserEmail()).getId());
        return "redirect:/home";
    }

    @RequestMapping(value = "/auction/{id}/rate-owner", method = RequestMethod.GET)
    @ResponseBody
    public void rateOwner(@PathVariable int id, @RequestParam("rating") String rating) {
        Float ratingValue = Float.valueOf(rating);
        if (!auctionService.isLoggedInUserAuctionOwner(id) && (ratingValue >= 0 && ratingValue <= 5))
            userRatingService.insertOrUpdate(ratingValue, userService.getLoggedInUserId(), auctionService.getAuctionOwner(id).getId(),id);
    }

    @RequestMapping(value = "/auction/{id}/delete-user-rating", method = RequestMethod.GET)
    public @ResponseBody boolean deleteUserRating(@PathVariable int id) {
        if (!auctionService.isLoggedInUserAuctionOwner(id)) {
            userRatingRepository.delete(userService.getLoggedInUserId(), auctionService.getAuctionOwner(id).getId(), id);
            return true;
        }
        return false;
    }
}
