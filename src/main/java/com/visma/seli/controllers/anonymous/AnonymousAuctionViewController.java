package com.visma.seli.controllers.anonymous;

import com.visma.seli.controllers.view.models.AnonymousAuctionSearchViewModelService;
import com.visma.seli.server.data.access.repository.database.auction.AuctionRepository;
import com.visma.seli.server.data.access.repository.database.auction.comment.CommentRepository;
import com.visma.seli.server.data.access.repository.database.user.logedInUser.UserRepository;
import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.AuctionService;
import com.visma.seli.service.auction.bid.Bid;
import com.visma.seli.service.auction.comment.AuctionCommentService;
import com.visma.seli.service.auction.comment.Comment;
import com.visma.seli.service.enums.UserEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.UUID;

@Controller
public class AnonymousAuctionViewController {
    @Autowired
    AuctionRepository auctionRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AnonymousAuctionSearchViewModelService anonymousAuctionSearchViewModelService;
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private AuctionCommentService auctionCommentService;
    @Autowired
    private AuctionService auctionService;

    @RequestMapping("/anonymousAuction/{userUuid}")
    public String viewAnonymousAuction(@PathVariable UUID userUuid, Model model) {
        anonymousAuctionSearchViewModelService.getModelForViewAnonymousAuctionController(userUuid, model);
        AuctionBody anonymousAuction = auctionRepository.findAnonymousAuction(userUuid, UserEnum.INVITED);
        int bidderId = userRepository.getInvitedUserIdByUUID(userUuid);
        if (!model.containsAttribute("auctionBid")) {
            Bid bid = new Bid(anonymousAuction.getId(), bidderId, null);  // PATAISYTI USER ID
            model.addAttribute("auctionBid", bid);
            model.addAttribute("uuid", userUuid);
        }
        model.addAttribute("uuid", userUuid);
        model.addAttribute("newComment", new Comment());
        model.addAttribute("comments", commentRepository.findAllAuctionComments(anonymousAuction.getId()));
        model.addAttribute("auctionStarted", LocalDateTime.now().isAfter(anonymousAuction.getStartDate()));
        
        return "html/auction/anonymousAuctionView";
    }

    @RequestMapping("/anonymousOwnerAuction/{ownerUuid}")
    public String viewAnonymousOwnerAuction(@PathVariable UUID ownerUuid, Model model) {
        anonymousAuctionSearchViewModelService.getModelForViewAnonymousOwnerAuctionController(ownerUuid, model);
        AuctionBody anonymousAuction = auctionRepository.findAnonymousAuction(ownerUuid, UserEnum.OWNER);
        if (!model.containsAttribute("auctionBid")) {
            Bid bid = new Bid(anonymousAuction.getId(), 1, null);  // PATAISYTI USER ID
            model.addAttribute("auctionBid", bid);
        }
        model.addAttribute("uuid", ownerUuid);
        model.addAttribute("newComment", new Comment());
        model.addAttribute("comments", commentRepository.findAllAuctionComments(anonymousAuction.getId()));
        return "html/auction/anonymousAuctionOwnerView";
    }

    @RequestMapping("/auction/anonymous/picture/{uuid}/{pictureId}")
    public ResponseEntity<byte[]> getAnonymousQRImage(@PathVariable UUID uuid, @PathVariable int pictureId) throws NoSuchAlgorithmException {
        if (auctionRepository.findAnonymousAuctionIdByUUID(uuid) > 0){
            return getPicture(auctionRepository.findAnonymousAuctionIdByUUID(uuid), pictureId);
        }
        else if (auctionRepository.findAnonymousAuction(uuid,UserEnum.OWNER) != null){
            return getPicture(auctionRepository.findAnonymousAuction(uuid,UserEnum.OWNER).getId(), pictureId);
        }
        else
            return null;
    }

    private ResponseEntity<byte[]> getPicture(int auctionId, int pictureId) throws NoSuchAlgorithmException {
        return auctionService.getImageResponseEntity(auctionId, pictureId);
    }


}
