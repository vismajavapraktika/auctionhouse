package com.visma.seli.controllers.anonymous;

import com.visma.seli.server.data.access.repository.database.pictures.PictureRepository;
import com.visma.seli.server.data.access.repository.database.user.anonymousUser.AnonymousUserRepository;
import com.visma.seli.service.auction.AnonymousUserData;
import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.AuctionService;
import com.visma.seli.service.auction.Picture;
import com.visma.seli.service.auction.anonymous.AnonymousAuctionService;
import com.visma.seli.service.enums.AuctionStatusEnum;
import com.visma.seli.service.user.AnonymousUser;
import com.visma.seli.service.user.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Controller
public class AnonymousAuctionCreateController {

    private final String AUCTION_TYPE = "anonymous";
    @Autowired
    private AnonymousUserRepository anonymousUserRepository;
    @Autowired
    private AnonymousUserData anonymousUserData;
    @Autowired
    private AnonymousAuctionService anonymousAuctionService;
    @Autowired
    private PictureRepository pictureRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private AuctionService auctionService;

    public static List<String> ALLOWED_PICTURE_TYPES = getAllowedPictureTypes();
	
    private static List<String> getAllowedPictureTypes() {
    	List<String> ret = Arrays.asList("jpg", "png", "jpeg");
    	return ret;
    }

    
    @RequestMapping(value = "/newAnonymousAuction", method = RequestMethod.POST)
    public String getAnonymousAuctionPage(@Valid @ModelAttribute("anonymousUser") AnonymousUser anonymousUser,
                                          BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            anonymousUserData.setAnonymousUserId(anonymousUser.getId());
            return "html/auction/anonymous-user-info";
        }
        AnonymousUser temporaryAnonymousUser = anonymousUserRepository.checkIfUserExistsInJDBC(anonymousUser.getEmail());
        if (temporaryAnonymousUser == null)
            anonymousUserRepository.insertNewAnonymousUser(anonymousUser);
        else
            anonymousUser = temporaryAnonymousUser;
        model.addAttribute("anonymousUserID", anonymousUser.getId());
        anonymousUserData.setAnonymousUserId(anonymousUser.getId());
        return "html/auction/create-anonymous-auction";
    }

    @ModelAttribute("auction")
    public AuctionBody getAuction() {
        AuctionBody auctionBody = new AuctionBody();
        auctionBody.setAnonymous(true);
        auctionBody.setStatus(AuctionStatusEnum.ACTIVE);
		return auctionBody;
    }

    @RequestMapping(value = "/anonymousAuctionUser")
    public String getAuctionUserPage() {
        return "html/auction/anonymous-user-info";
    }

    @ModelAttribute("anonymousUser")
    public AnonymousUser getAnonymousUser() {
        return new AnonymousUser();
    }

    @RequestMapping(value = "/createAnonymousAuction", method = RequestMethod.POST)
    public String saveAnonymousAuction(@Valid @ModelAttribute("auction") AuctionBody auctionBody,
                                       BindingResult bindingResult, Model model, final RedirectAttributes redirectAttributes) {
        int anonymousUserID = anonymousUserData.getAnonymousUserId();
        bindingResult = anonymousAuctionService.checkAnonymousAuctionBindingResult(auctionBody, bindingResult,anonymousUserID);
        if (bindingResult.hasErrors()) {
            return "html/auction/create-anonymous-auction";
        }
        anonymousAuctionService.setAnonymousAuctionDefaultValues(auctionBody);
        anonymousAuctionService.insertAnonymousAuctionValuesToJDBC(auctionBody, anonymousUserID, AUCTION_TYPE);
        anonymousAuctionService.sendNotificationsForAuctionUsers(auctionBody, anonymousUserID);
        
        redirectAttributes.addFlashAttribute("removePicturesFromLocalStorage", true);
        
        return "redirect:/anonymousOwnerAuction/" + auctionBody.getAuctionIdUrlOwner();
    }


    @RequestMapping(value = "/create-auction/picture-check", method = RequestMethod.POST)
    @ResponseBody
    public Picture pictureTemporarySave(@RequestParam("picture") MultipartFile picture) throws IOException {
    	String[] typeAndSubtype = picture.getContentType().split("/");
    	if(typeAndSubtype[0].equals("image") && ALLOWED_PICTURE_TYPES.contains(typeAndSubtype[1]))
    	{
	        Integer anonymousUserId = userService.getLoggedInUserEmail().equals("anonymousUser") ? anonymousUserData.getAnonymousUserId() : userService.getLoggedInUserId();
	        return pictureRepository.insertTemporaryPicture(picture.getOriginalFilename(), auctionService.resize(picture).getBytes(), anonymousUserId);
    	}
    	
    	return null;
    }

}
