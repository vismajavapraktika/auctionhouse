package com.visma.seli.controllers.anonymous;

import com.visma.seli.server.data.access.repository.database.auction.AuctionRepository;
import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.anonymous.AnonymousAuctionService;
import com.visma.seli.service.auction.anonymous.AnonymousInviteService;
import com.visma.seli.service.auction.invitations.AuctionInviteService;
import com.visma.seli.service.enums.UserEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.UUID;

@Controller
public class AnonymousAuctionInviteController {

    @Autowired
    private AnonymousAuctionService anonymousAuctionService;
    @Autowired
    private AuctionRepository auctionRepository;
    @Autowired
    private AnonymousInviteService anonymousAuctionInviteService;

    @ModelAttribute("auction")
    public AuctionBody getAuction() {
        AuctionBody auctionBody = new AuctionBody();
        auctionBody.setAnonymous(true);
		return auctionBody;
    }
    
    @RequestMapping(value = "/anonymousAuction/anonymousInvite/{uuid}", method = RequestMethod.GET)
    public String inviteToAuction(@PathVariable UUID uuid, Model model) throws Exception {
        return anonymousAuctionInviteService.anonymousAuctionInvitation(uuid, UserEnum.INVITED, model);
    }

    @RequestMapping(value = "/anonymousAuctionOwner/anonymousInvite/{uuid}", method = RequestMethod.GET)
    public String inviteToAuctionByOwner(@PathVariable UUID uuid, Model model) throws Exception {
        return anonymousAuctionInviteService.anonymousAuctionInvitation(uuid,UserEnum.OWNER, model);
    }

    @RequestMapping(value = "/anonymousAuction/anonymousInvited/{uuid}", method = RequestMethod.POST)
    public String saveAnonymousInvitations(@PathVariable UUID uuid, @ModelAttribute("auction") AuctionBody auctionBody, Model model) throws Exception {
        return anonymousAuctionInviteService.anonymousAuctionInvitationSaved(uuid, auctionBody, model);
    }

    @RequestMapping(value = "/go-back/anonymousAuction/anonymousInvited/{uuid}", method = RequestMethod.GET)
    public String goBackToAnonymousAuction(@PathVariable UUID uuid, Model model){
        model.addAttribute("id",uuid);
        if (auctionRepository.checkIfAnonymousAuctionOwner(uuid))
            return "redirect:/anonymousOwnerAuction/"+uuid;
        return "redirect:/anonymousAuction/"+uuid;
    }

    @RequestMapping(value = "/go-back/anonymous/{uuid}", method = RequestMethod.POST)
    public String goBackAfterExistingAnonymousInvitation(@PathVariable UUID uuid, Model model){
        model.addAttribute("failMessage",null);
        model.addAttribute("id",uuid);
        return "html/auction/invite";
    }
}
