package com.visma.seli.server.data.access.mappers.jdbc.user;

import com.visma.seli.service.enums.UserTypes;
import com.visma.seli.service.user.UserBody;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;


@Component
public class GoogleUserBodyMapper implements RowMapper<UserBody> {

    @Override
    public UserBody mapRow(ResultSet resultSet, int i) throws SQLException {
        int id = resultSet.getInt("id");
        String name = resultSet.getString("name");
        String surname = resultSet.getString("surname");
        String email = resultSet.getString("email");
        String phone = resultSet.getString("phone");
        return new UserBody(id,name,surname,email,phone, UserTypes.GOOGLE_USER);
    }
}
