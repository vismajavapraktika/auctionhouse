package com.visma.seli.server.data.access.mappers.jdbc.user;

import com.visma.seli.service.enums.UserTypes;
import com.visma.seli.service.user.UserBody;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class UserBodyMapper implements RowMapper<UserBody> {

    @Override
    public UserBody mapRow(ResultSet resultSet, int i) throws SQLException {
        int id = resultSet.getInt("id");
        String name = resultSet.getString("name");
        String surname = resultSet.getString("surname");
        String email = resultSet.getString("email");
        String phone = resultSet.getString("phone");
        String password = resultSet.getString("password");
        String userType = resultSet.getString("user_type");
        UserTypes userTypeEnum = UserTypes.valueOf(userType.toUpperCase());
        return new UserBody(id, name, surname, email, phone, password, userTypeEnum);
    }
}
