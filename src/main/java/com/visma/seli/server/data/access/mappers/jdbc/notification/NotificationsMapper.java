package com.visma.seli.server.data.access.mappers.jdbc.notification;

import com.visma.seli.service.Notifications;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class NotificationsMapper implements RowMapper<Notifications>{

    @Override
    public Notifications mapRow(ResultSet resultSet, int i) throws SQLException {
        Notifications notifications = new Notifications();
        notifications.setId(resultSet.getInt("id"));
        notifications.setType(resultSet.getString("type"));
        notifications.setEmail(resultSet.getString("email"));
        notifications.setNewNotification(resultSet.getBoolean("new_notification"));
        notifications.setNotificationId(resultSet.getInt("auction_id"));
        notifications.setInsertDate(resultSet.getTimestamp("insert_date").toLocalDateTime());
        return notifications;
    }
}
