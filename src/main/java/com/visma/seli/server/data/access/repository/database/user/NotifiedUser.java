package com.visma.seli.server.data.access.repository.database.user;

public class NotifiedUser {
    private int notifiedUserId, notifiedAuctionId;

    public int getNotifiedUserId() {
        return notifiedUserId;
    }

    public void setNotifiedUserId(int notifiedUserId) {
        this.notifiedUserId = notifiedUserId;
    }

    public int getNotifiedAuctionId() {
        return notifiedAuctionId;
    }

    public void setNotifiedAuctionId(int notifiedAuctionId) {
        this.notifiedAuctionId = notifiedAuctionId;
    }
}
