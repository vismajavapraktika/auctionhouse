package com.visma.seli.server.data.access.mappers.jdbc.auction;

import com.visma.seli.service.auction.Picture;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class TemporaryPictureMapper implements RowMapper<Picture> {
    @Override
    public Picture mapRow(ResultSet resultSet, int i) throws SQLException {
        Picture picture = new Picture();
        picture.setPictureId(resultSet.getInt("id"));
        picture.setPictureBytes(resultSet.getBytes("bytes"));
        picture.setInsertDate(resultSet.getTimestamp("insert_date").toLocalDateTime());
        return picture;
    }
}
