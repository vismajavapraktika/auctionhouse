package com.visma.seli.server.data.access.repository.database.user.logedInUser;

import com.visma.seli.server.data.access.mappers.jdbc.user.GoogleUserBodyMapper;
import com.visma.seli.server.data.access.mappers.jdbc.user.UserBodyMapper;
import com.visma.seli.service.captions.CaptionService;
import com.visma.seli.service.enums.UserTypes;
import com.visma.seli.service.user.UserBody;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.*;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private static final String SELECT_USER_WHEN_LOGIN = "SELECT * FROM test_user WHERE email = ?";
    private static final String SELECT_ALL_USERS = "SELECT * FROM test_user";
//    private static final String SELECT_ALL_GOOGLE_USERS = "SELECT * FROM google_users";
    private static final String SELECT_EMAIL_FOR_VALIDATION = "SELECT email FROM test_user";
//    private static final String SELECT_EMAIL_FROM_GOOGEL_USER_FOR_VALIDATION = "SELECT email FROM google_users";
    private static final String SELECT_USER_BY_ID_QUERY = "SELECT * FROM test_user WHERE id = ?";
    private static final String SELECT_USERS_BY_IDS_QUERY = "SELECT * FROM test_user WHERE id IN (:ids)";
//    private static final String SELECT_USER_FROM_GOOGLE_BY_ID_QUERY = "SELECT * FROM google_users WHERE id = ?";
    private static final String SELECT_USER_ID_BY_UUID_QUERY = "SELECT user_id FROM invitations_anonymous WHERE uuid = ?";
    private static final String SELECT_USER_EMAIL_BY_UUID_QUERY = "SELECT invited_email FROM invitations_anonymous WHERE uuid = ?";
    private static final String SELECT_ANONYMOUS_USER_BY_ID_QUERY = "SELECT * FROM anonymous_users WHERE id = ?";
    private static final String UPDATE_ANONYMOUS_USER_SENT_EMAIL_ABOUT_ENDED_AUCTION_STATUS = "UPDATE anonymous_users SET informed_about_ended_auction = 'true' where id = ?";
    private static final String GET_OWNER_BY_ID = "SELECT * FROM test_user WHERE id = (SELECT owner_id FROM auctions WHERE id = ?)";
//    private static final String GET_OWNER_FROM_GOOGLE_BY_ID = "SELECT * FROM google_users WHERE id = (SELECT owner_id FROM auctions WhERE id = ?)";
//    private static final String SELECT_GOOGLE_USER_WHEN_LOGIN = "SELECT * FROM google_users WHERE email = ?";

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbctemplate;
    @Autowired
    private DataSource dataSource;
    @Autowired
    private UserBodyMapper userBodyMapper;
    @Autowired
    private CaptionService captionService;
    @Autowired
    private GoogleUserBodyMapper googleUserBodyMapper;

    @Override
    public UserBody insertNewUser(UserBody userBody) {
        int allUsersId = insertNewAllUsers(userBody.getUserType());
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("test_user");
        Map<String, Object> parameters = new HashMap();
        parameters.put("id", allUsersId);
        parameters.put("name", userBody.getName());
        parameters.put("surname", userBody.getSurname());
        parameters.put("email", userBody.getEmail());
        parameters.put("phone", userBody.getPhone());
        parameters.put("password", userBody.getPassword());
        parameters.put("enabled", "true");
        parameters.put("user_role", "USER");
        parameters.put("user_type",userBody.getUserType());
        simpleJdbcInsert.execute(parameters);
        userBody.setId(allUsersId);
        return userBody;
    }

    @Override
    public UserBody insertNewGoogleUser(UserBody userBody) {
        int allUsersId = insertNewAllUsers(userBody.getUserType());
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("google_users");
        Map<String, Object> parameters = new HashedMap();
        parameters.put("id", allUsersId);
        parameters.put("name",userBody.getName());
        parameters.put("surname", userBody.getSurname());
        parameters.put("email", userBody.getEmail());
        parameters.put("phone", userBody.getPhone());
        parameters.put("enabled", "true");
        parameters.put("user_role", "USER");
        simpleJdbcInsert.execute(parameters);
        userBody.setId(allUsersId);
        return userBody;
    }

    @Override
    public UserBody getUserByEmail(String login) {
        /*if (getUserFromUsersTableByEmail(login) != null){
            return getUserFromUsersTableByEmail(login);
        }else if (getUserFromGoogleUsersTableByEmail(login)!=null){
            return getUserFromGoogleUsersTableByEmail(login);
        }
        else return null;*/
        return getUserFromUsersTableByEmail(login);
    }

    private int insertNewAllUsers(UserTypes usersType) {
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("all_users")
                .usingGeneratedKeyColumns("id");
        Map<String, Object> parameters = new HashedMap();
        parameters.put("type", usersType);
        return (Integer) simpleJdbcInsert.executeAndReturnKey(parameters);
    }

    private UserBody getUserFromUsersTableByEmail(String login) {
        List<UserBody> userBodyList;
        userBodyList = jdbcTemplate.query(SELECT_USER_WHEN_LOGIN, new Object[]{login}, userBodyMapper);
        if (userBodyList.isEmpty()) {
            return null;
        } else {
            return userBodyList.get(0);
        }
    }

    /*private UserBody getUserFromGoogleUsersTableByEmail(String login) {
        List<UserBody> userBodyList;
        userBodyList = jdbcTemplate.query(SELECT_GOOGLE_USER_WHEN_LOGIN, new Object[]{login},googleUserBodyMapper);
        if (userBodyList.isEmpty()){
            return null;
        } else {
            return userBodyList.get(0);
        }
    }
*/
    @Override
    public List<String> getEmails() {
        List<String> listOfEmails = jdbcTemplate.queryForList(SELECT_EMAIL_FOR_VALIDATION, String.class);
//        List<String> listOfGoogleEmails = jdbcTemplate.queryForList(SELECT_EMAIL_FROM_GOOGEL_USER_FOR_VALIDATION, String.class);
//        listOfEmails.addAll(listOfGoogleEmails);
        return listOfEmails;
    }

    @Override
    public List<UserBody> getAllUsers() {
        List<UserBody> allUsersList;
        allUsersList = jdbcTemplate.query(SELECT_ALL_USERS, userBodyMapper);
//        List<UserBody> allGoogleUserList = jdbcTemplate.query(SELECT_ALL_GOOGLE_USERS, googleUserBodyMapper);
//        allUsersList.addAll(allGoogleUserList);
        return allUsersList;
    }

    @Override
    public UserBody getUserById(int id) {
        UserBody userFromUsersTableById = getUserFromUsersTableById(id);
        if (userFromUsersTableById != null)
            return userFromUsersTableById;
        /*else if (getUserFromGoogleUsersTableById(id) != null)
            return getUserFromGoogleUsersTableById(id);*/
        return null;
    }

    @Override
    public List<UserBody> getUsersByIds(List<Integer> ids) {

        MapSqlParameterSource parameters = new MapSqlParameterSource();
        parameters.addValue("ids", ids);

        return namedParameterJdbctemplate.query(SELECT_USERS_BY_IDS_QUERY, parameters, userBodyMapper);
    }

    private UserBody getUserFromUsersTableById(int id){
        List<UserBody> userBodyList;
        userBodyList = jdbcTemplate.query(SELECT_USER_BY_ID_QUERY, new Object[]{id}, userBodyMapper);
        if (userBodyList.isEmpty()) {
            return null;
        } else {
            return userBodyList.get(0);
        }
    }

    /*private UserBody getUserFromGoogleUsersTableById(int id){
        List<UserBody> userBodyList;
        userBodyList = jdbcTemplate.query(SELECT_USER_FROM_GOOGLE_BY_ID_QUERY, new Object[]{id}, googleUserBodyMapper);
        if (userBodyList.isEmpty()) {
            return null;
        } else {
            return userBodyList.get(0);
        }
    }*/



    @Override
    public int getInvitedUserIdByUUID(UUID id) {
        return jdbcTemplate.queryForObject(SELECT_USER_ID_BY_UUID_QUERY, new Object[]{id.toString()}, Integer.class);
    }
    
    @Override
    public String getInvitedUserEmailByUUID(UUID id) {
        return jdbcTemplate.queryForObject(SELECT_USER_EMAIL_BY_UUID_QUERY, new Object[]{id.toString()}, String.class);
    }

    @Override
    public void changeAnonymousUsersSentEmailAboutEndedAuctionStatus(int id) {
        jdbcTemplate.update(UPDATE_ANONYMOUS_USER_SENT_EMAIL_ABOUT_ENDED_AUCTION_STATUS, id);
    }

    @Override
    public UserBody getOwnerByAuctionId(int auctionId) {
        if (getOwnerFromUsersTableByAuctionId(auctionId) != null)
            return getOwnerFromUsersTableByAuctionId(auctionId);
        /*else if (getOwnerFromGoogleUsersTableByAuctionId(auctionId) != null)
            return getOwnerFromGoogleUsersTableByAuctionId(auctionId);*/
        return null;
    }

    private UserBody getOwnerFromUsersTableByAuctionId(int auctionId){
        List<UserBody> ownerList = jdbcTemplate.query(GET_OWNER_BY_ID, new Object[]{auctionId}, userBodyMapper);
        if (ownerList.isEmpty()) {
            return null;
        } else {
            return ownerList.get(0);
        }
    }

    /*private UserBody getOwnerFromGoogleUsersTableByAuctionId(int auctionId){
        List<UserBody> ownerList = jdbcTemplate.query(GET_OWNER_FROM_GOOGLE_BY_ID, new Object[]{auctionId}, googleUserBodyMapper);
        if (ownerList.isEmpty()) {
            return null;
        } else {
            return ownerList.get(0);
        }
    }*/
}
