package com.visma.seli.server.data.access.repository.database.auction.won;

import com.visma.seli.service.enums.WinnerMessageResponseStatusEnum;
import com.visma.seli.service.auction.bid.Bid;
import com.visma.seli.service.auction.bid.WinMessageData;

import java.util.List;

public interface WonAuctionsRepository {

    List<Bid> findByUser(int userId);

    void insertNewBidWinner(Bid bid);

    void updateAllExpiredWinStatus();

    List<WinMessageData> selectAllWinnersToNotify();

    void setNewMessageExpirationTimeStatus(WinMessageData winMessageData);

    WinMessageData getCurrentWinnerByAuctionId(int id);

    Integer updateStatusAfterWinnerAnswer(WinnerMessageResponseStatusEnum statusTo, int userID, int auctionId);

    int getWinnersPosition(int auction_id, int user_id);

    WinMessageData getWinMessageData(int auctionId, int userId);

    void updateAuctionStatuses(WinnerMessageResponseStatusEnum from, WinnerMessageResponseStatusEnum to, int id);

    int getWinnersLeft(int id);

    List<WinMessageData> setNextWinnersStatusAndExpirationDate(int auctionId);
}
