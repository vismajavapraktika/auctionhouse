package com.visma.seli.server.data.access.mappers.jdbc.user;

import com.visma.seli.server.data.access.repository.database.user.NotifiedUser;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class NotifiedUserMapper implements RowMapper<NotifiedUser>{
    @Override
    public NotifiedUser mapRow(ResultSet resultSet, int i) throws SQLException {
        NotifiedUser notifiedUser = new NotifiedUser();
        notifiedUser.setNotifiedAuctionId(resultSet.getInt("auction_id"));
        notifiedUser.setNotifiedUserId(resultSet.getInt("user_id"));
        return notifiedUser;
    }
}
