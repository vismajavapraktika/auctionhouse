package com.visma.seli.server.data.access.repository.database.auction;

import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.enums.AuctionStatusEnum;
import com.visma.seli.service.enums.AuctionTypeEnum;
import com.visma.seli.service.enums.InvitationStatusEnum;
import com.visma.seli.service.enums.UserEnum;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

public interface AuctionRepository {

    AuctionBody insertNewAuction(AuctionBody auctionBody, int id, String AUCTION_TYPE);
    AuctionBody findAuction(int id);
    List<AuctionBody> findAll();
    int findMinPrice(String sort);
    int findMaxPrice(String sort);
    List<AuctionBody> findAuctionsByType(String type);
    List<AuctionBody> findMyAuctions(int id);
    byte[] findAuctionPictureBytes(int auctionId, int pictureId);
    byte[] getDefaultPicture();
    void updateAuction(AuctionBody auctionBody);
    void updateAuctionEndTimeOnBid(int id, LocalDateTime endTime);
    void changeAuctionStatus(AuctionStatusEnum status, int id);
    List<AuctionBody> findUserPrivateAuctions(String email, InvitationStatusEnum status, List<AuctionStatusEnum> auctionStatusEnums);
    List<AuctionBody> findAllAuctionsWithTypesAndStatuses(List<AuctionTypeEnum> types, List<AuctionStatusEnum> statuses);
    int findAuctionIdByUUID(UUID id);
    UUID findAuctionUUIDByEmail(String email, int auctionId);
    String findAnonymousOwnerEmailById(int id); // What id???? If User then change place of this function to user repository, if auction id then leave this here
    int getItemQuantityLeft(int auction_id);
    void setSubtractSoldItemQuantity(int quantity, int auction_id);
    String findIfAnonymousOrFullAccessAuction(int auctionId); // delete maybe?
    boolean checkIfAnonymousAuctionOwner(UUID uuid);
    AuctionBody findAnonymousAuction(UUID id, UserEnum userEnum);
    int findAnonymousAuctionIdByUUID(UUID id);
}
