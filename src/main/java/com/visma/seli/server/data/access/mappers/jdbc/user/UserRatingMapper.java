package com.visma.seli.server.data.access.mappers.jdbc.user;

import com.visma.seli.service.user.rating.UserRating;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class UserRatingMapper implements RowMapper<UserRating> {

    @Override
    public UserRating mapRow(ResultSet rs, int arg1) throws SQLException {
        UserRating userRating = new UserRating();
        userRating.setId(rs.getInt("id"));
        userRating.setRateeId(rs.getInt("ratee_id"));
        userRating.setRaterId(rs.getInt("rater_id"));
        userRating.setRating(rs.getFloat("rating"));
        return userRating;
    }

}