package com.visma.seli.server.data.access.repository.database.pictures;

import com.visma.seli.server.data.access.mappers.jdbc.auction.PictureMapper;
import com.visma.seli.server.data.access.mappers.jdbc.auction.TemporaryPictureMapper;
import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.Picture;
import com.visma.seli.service.auction.UrlIdCoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PictureRepositoryImpl implements PictureRepository {
    private static final String FIND_PICTURES_COUNT = "SELECT count(*) FROM auction_pictures WHERE auction_id = ?";
    private static final String FIND_TEMPORARY_AUCTION_PICTURES_BY_ID = "SELECT bytes FROM temporary_photos WHERE photo_uuid = ?";
    private static final String DELETE_TEMPORARY_PICTURE_BY_UUID = ("DELETE FROM temporary_photos WHERE photo_uuid = ?");
    private static final String GET_AUCTION_PHOTOS_BY_AUCTION_ID = ("SELECT * FROM auction_pictures WHERE auction_id = ?");
    private static final String DELETE_PICTURE = "DELETE FROM auction_pictures WHERE id = ?";
    private static final String GET_AUCTION_PICTURE_BY_PICTURE_ID = ("SELECT * FROM auction_pictures WHERE id = ?");
    private static final String GET_ALL_TEMPORARY_PICTURES = ("SELECT * FROM temporary_photos");
    private static final String DELETE_TEMPORARY_PICTURE = ("DELETE FROM temporary_photos WHERE id = ?");
    private static final String GET_ALL_PICTURES = ("SELECT * FROM auction_pictures");
    private static final String UPDATE_PICTURE_INFORMATION = ("UPDATE auction_pictures SET bytes = ? WHERE id = ?");
    private static final Logger logger = Logger.getLogger(Class.class.getName());


    @Autowired
    private UrlIdCoder urlIdCoder;
    @Autowired
    private javax.sql.DataSource dataSource;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private PictureMapper pictureMapper;
    @Autowired
    private TemporaryPictureMapper temporaryPictureMapper;

    @Override
    public int findAuctionPicturesCount(int auctionId) {
        return jdbcTemplate.queryForObject(FIND_PICTURES_COUNT, new Object[]{auctionId}, Integer.class);
    }

    @Override
    public Picture insertTemporaryPicture(String pictureName, byte[] pictureBytes, int userId) {
        try {
            LocalDateTime localDateTime = LocalDateTime.now();
            SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("temporary_photos")
                    .usingGeneratedKeyColumns("id");
            Map<String, Object> parameters = new HashMap<String, Object>();
            UUID returnUUID = urlIdCoder.createUUID();
            Picture picture = new Picture();
            picture.setPictureName(pictureName);
            picture.setPictureUUID(returnUUID);
            parameters.put("photo_uuid", returnUUID);
            parameters.put("bytes", pictureBytes);
            parameters.put("picture_name", pictureName);
            parameters.put("user_id", userId);
            parameters.put("insert_date", Timestamp.valueOf(localDateTime));
            simpleJdbcInsert.execute(parameters);
            return picture;
        } catch (DataAccessException e) {
            logger.log(Level.SEVERE, e.toString(), e);
            e.printStackTrace();
            throw new RuntimeException();
        }
    }

    @Override
    public byte[] getTemporaryPicture(UUID temporaryPictureIdList) {
        List<byte[]> picture = this.jdbcTemplate.queryForList(FIND_TEMPORARY_AUCTION_PICTURES_BY_ID, new Object[]{temporaryPictureIdList}, byte[].class);
        if (picture.isEmpty())
            return null;
        return picture.get(0);
    }

    @Override
    public void deleteTemporaryPicture(UUID pictureUUID) {
        jdbcTemplate.update(DELETE_TEMPORARY_PICTURE_BY_UUID, pictureUUID);
    }

    @Override
    public List<Picture> getPictureList(int auctionId) {
        return jdbcTemplate.query(GET_AUCTION_PHOTOS_BY_AUCTION_ID,
                new Object[]{auctionId}, pictureMapper);
    }

    @Override
    public void deletePicture(int pictureId) {
        jdbcTemplate.update(DELETE_PICTURE, pictureId);
    }

    @Override
    public Picture getAuctionPictureByPictureId(int pictureId) {
        List<Picture> pictureList = jdbcTemplate.query(GET_AUCTION_PICTURE_BY_PICTURE_ID,
                new Object[]{pictureId}, pictureMapper);
        return pictureList.get(0);
    }

    @Override
    public List<Picture> getTemporaryPicturesList() {
        return jdbcTemplate.query(GET_ALL_TEMPORARY_PICTURES, temporaryPictureMapper);
    }

    @Override
    public void deleteTemporaryPicture(int pictureId) {
        jdbcTemplate.update(DELETE_TEMPORARY_PICTURE, pictureId);
    }

    @Override
    public void deleteAuctionTemporaryPictures(AuctionBody auctionBody) {
        for (UUID sendUUID : auctionBody.getListOfPhotosUUUID()) {
            jdbcTemplate.update(DELETE_TEMPORARY_PICTURE_BY_UUID, sendUUID);
        }
    }

    @Override
    public List<Picture> getAllPictures() {
        return jdbcTemplate.query(GET_ALL_PICTURES, pictureMapper);
    }

    @Override
    public void updatePictures(Picture resizePictures) {
        jdbcTemplate.update(UPDATE_PICTURE_INFORMATION, resizePictures.getPictureBytes(), resizePictures.getPictureId() );
    }
}
