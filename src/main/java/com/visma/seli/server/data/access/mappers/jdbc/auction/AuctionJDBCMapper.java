package com.visma.seli.server.data.access.mappers.jdbc.auction;

import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.enums.AuctionCurrencyEnum;
import com.visma.seli.service.enums.AuctionStatusEnum;
import com.visma.seli.service.enums.AuctionTypeEnum;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

@Component
public class AuctionJDBCMapper implements RowMapper<AuctionBody> {

    @Override
    public AuctionBody mapRow(ResultSet rs, int arg1) throws SQLException {
        AuctionBody auction = new AuctionBody();
        auction.setId(rs.getInt("id"));
        auction.setTitle(rs.getString("title"));
        auction.setDescription(rs.getString("description"));
        auction.setType(AuctionTypeEnum.valueOf(rs.getString("type")));
        auction.setStartDate(rs.getTimestamp("start_time").toLocalDateTime());
        auction.setEndDate(rs.getTimestamp("end_time").toLocalDateTime());
        auction.setBuyOutPrice(rs.getBigDecimal("buy_out_price"));
        auction.setItemQuantity(rs.getInt("quantity"));
        auction.setStartingPrice(rs.getBigDecimal("starting_price"));
        auction.setCurrency(AuctionCurrencyEnum.valueOf(rs.getString("currency").toUpperCase()));
        auction.setStatus(AuctionStatusEnum.valueOf(rs.getString("status").toUpperCase()));
        auction.setOwnerId(rs.getInt("owner_id"));
        auction.setAllowToBidForQuantity(rs.getBoolean("allow_bid_for_quantity"));
        auction.setBuyOutPrice(rs.getBigDecimal("buy_out_price"));
        auction.setAuctionIdUrlOwner(UUID.fromString(rs.getString("uuid")));
        auction.setLink(rs.getString("link"));
        auction.setAnonymous(rs.getBoolean("is_anonymous"));
        return auction;
    }

}