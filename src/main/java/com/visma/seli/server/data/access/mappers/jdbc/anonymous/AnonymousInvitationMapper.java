package com.visma.seli.server.data.access.mappers.jdbc.anonymous;

import com.visma.seli.service.auction.Invitation;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

@Component
public class AnonymousInvitationMapper implements RowMapper<Invitation> {

    @Override
    public Invitation mapRow(ResultSet rs, int arg1) throws SQLException {
        Invitation invitation = new Invitation();
        invitation.setId(rs.getInt("id"));
        invitation.setAuctionId(rs.getInt("auction_id"));
        invitation.setEmail(rs.getString("invited_email"));
        invitation.setInvitedUuid(UUID.fromString(rs.getString("uuid")));
        invitation.setUserId(rs.getInt("user_id"));
        invitation.setNotifiedAboutAuctionEnd(rs.getBoolean("notified_about_auction_end"));
        return invitation;
    }
}
