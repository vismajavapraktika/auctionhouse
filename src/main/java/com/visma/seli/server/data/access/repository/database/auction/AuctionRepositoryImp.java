package com.visma.seli.server.data.access.repository.database.auction;

import com.google.common.io.ByteStreams;
import com.visma.seli.server.data.access.mappers.jdbc.anonymous.AnonymousInvitationMapper;
import com.visma.seli.server.data.access.mappers.jdbc.auction.AuctionJDBCMapper;
import com.visma.seli.server.data.access.mappers.jdbc.auction.InvitationMapper;
import com.visma.seli.server.data.access.repository.database.auction.invitations.InvitationRepository;
import com.visma.seli.server.data.access.repository.database.pictures.PictureRepository;
import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.Invitation;
import com.visma.seli.service.enums.AuctionStatusEnum;
import com.visma.seli.service.enums.AuctionTypeEnum;
import com.visma.seli.service.enums.InvitationStatusEnum;
import com.visma.seli.service.enums.UserEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;
import java.util.logging.Level;

@Component
@Transactional
public class AuctionRepositoryImp implements AuctionRepository {

    private static final String INSERT_PICTURE = ("INSERT INTO auction_pictures (auction_id, photo_name, bytes) Values(?,?,?)");
    private static final String FIND_AUCTION_PICTURES_NAME_BY_ID = "SELECT picture_name FROM temporary_photos WHERE photo_uuid = ?";
    private static final String FIND_AUCTION_QUERY = "SELECT * FROM auctions WHERE id = ?";
    private static final String FIND_AUCTION_QUERY_BY_UUID = "SELECT * FROM auctions WHERE id in (SELECT auction_id FROM invitations_anonymous  where uuid = ?)";
    private static final String FIND_AUCTION_QUERY_BY_OWNER_UUID = "SELECT * FROM auctions WHERE uuid = ?";
    private static final String FIND_AUCTION_ID_BY_UUID = "SELECT * FROM invitations_anonymous WHERE uuid = ?";
    private static final String FIND_ANONYMOUS_USER_UUID_BY_EMAIL = "SELECT uuid FROM invitations_anonymous WHERE invited_email = ? and auction_id = ?";
    private static final String FIND_MY_AUCTIONS_QUERY = "SELECT * FROM auctions WHERE owner_id = ?";
    private static final String FIND_ANONYMOUS_AUCTION_OWNER_EMAIL_BY_ID = "SELECT user_email FROM anonymous_user WHERE id = ?";
    private static final String FIND_AUCTION_MIN_PRICE_WITHOUT_DELETED_AUCTIONS = "SELECT starting_price FROM auctions WHERE status <> 'DELETE' ORDER BY starting_price ASC LIMIT 1 OFFSET 0";
    private static final String FIND_AUCTION_MIN_PRICE = "SELECT starting_price FROM auctions WHERE status ORDER BY starting_price ASC LIMIT 1 OFFSET 0";
    private static final String FIND_AUCTION_MAX_PRICE_WITHOUT_DELETED_AUCTIONS  = "SELECT starting_price FROM auctions WHERE status <> 'DELETE' ORDER BY starting_price DESC LIMIT 1 OFFSET 0";
    private static final String FIND_AUCTION_MAX_PRICE = "SELECT starting_price FROM auctions ORDER BY starting_price DESC LIMIT 1 OFFSET 0";
    private static final String FIND_ALL_AUCTIONS_QUERY = "SELECT * FROM auctions";
    private static final String FIND_TYPE_AUCTIONS_QUERY = "SELECT * FROM auctions WHERE type = ?";
    private static final String UPDATE_AUCTION = "UPDATE auctions set status = ?, start_time = ?, end_time = ?, quantity = ?, starting_price = ?, description = ?, link = ? where id = ?";
    private static final String SELECT_PICTURE = "SELECT bytes FROM Auction_Pictures WHERE auction_id = ?";
    private static final String UPDATE_AUCTION_PICTURE = "UPDATE auction_pictures set photo_name = ? , bytes = ? where auction_id = ?";
    private static final String CHANGE_STATUS = "UPDATE auctions set status = ? where id = ?";
    private static final String DELETE_AUCTION = "DELETE FROM auctions WHERE id = ?";
    private static final String FIND_USER_PRIVATE_AUCTIONS_QUERY = "SELECT * FROM invitations WHERE email = ? and status = ?";
    private static final String FIND_AUCTIONS_BY_TYPE_STATUS = "SELECT * FROM auctions WHERE type in (:listOfTypes) and status in (:listOfStatuses)";
    private static final String UPDATE_AUCTION_END_TIME_ON_BID = "UPDATE auctions SET end_time = ? WHERE id = ?";
    private static final String GET_ITEM_QUANTITY_LEFT = "SELECT quantity_left FROM auctions WHERE id = ?";
    private static final String SUBTRACT_SOLD_ITEM_QUANTITY = "UPDATE auctions SET quantity_left = quantity_left - ? WHERE id = ?";
    private static final String FIND_IF_ANONYMOUS_OR_FULL_ACCESS_AUCTION = "select all_users.type from all_users inner join auctions on all_users.id = auctions.owner_id and auctions.id = ?";
    private static final String FIND_AUCTION_BY_UUID = "SELECT * FROM auctions WHERE uuid = ?";
    private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger(Class.class.getName());

    @Autowired
    private DataSource dataSource;
    @Autowired
    private NamedParameterJdbcTemplate namedParameterJdbctemplate;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private AuctionJDBCMapper auctionJDBCMapper;
    @Autowired
    private InvitationMapper invitationMapper;
    @Autowired
    private AnonymousInvitationMapper anonymousInvitationMapper;
    @Autowired
    private InvitationRepository invitationRepository;
    @Autowired
    private PictureRepository pictureRepository;

    public static byte[] DEFAULT_PICTURE = readDefaultPicture();
    
    public static byte[] readDefaultPicture() {
    	try {
    		ClassPathResource resource = new ClassPathResource("static/css/img/no-auction-picture.png");
    		InputStream resourceInputStream = resource.getInputStream();
    		
    		byte[] imageByteArray = ByteStreams.toByteArray(resourceInputStream);
    		resourceInputStream.close();
    		return imageByteArray;
    	} catch (IOException e) {
    		logger.log(Level.SEVERE, e.toString(), e);
    	}
    	return null;
    }

    @Override
    public AuctionBody insertNewAuction(AuctionBody auctionBody, int ownerId, String AUCTION_TYPE) {
        try {
            logger.log(Level.INFO, "Inserting auction " + auctionBody.getTitle());
            SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("auctions")
                    .usingGeneratedKeyColumns("id");
            Map<String, Object> parameters = new HashMap<String, Object>();
            parameters.put("title", auctionBody.getTitle());
            parameters.put("type", auctionBody.getType());
            parameters.put("start_time", Timestamp.valueOf(auctionBody.getStartDate()));
            parameters.put("end_time", Timestamp.valueOf(auctionBody.getEndDate()));
            parameters.put("quantity", auctionBody.getItemQuantity());
            parameters.put("starting_price", auctionBody.getStartingPrice());
            parameters.put("currency", auctionBody.getCurrency());
            parameters.put("status", auctionBody.getStatus());
            parameters.put("description", auctionBody.getDescription());
            parameters.put("allow_bid_for_quantity", auctionBody.isAllowToBidForQuantity());
            parameters.put("buy_out_price", auctionBody.getBuyOutPrice());
            parameters.put("owner_id", ownerId);
            parameters.put("buy_out_price", auctionBody.getBuyOutPrice());
            parameters.put("quantity_left", auctionBody.getItemQuantity());
            parameters.put("uuid", auctionBody.getAuctionIdUrlOwner());
            parameters.put("allow_buy_out", auctionBody.isAllowBuyOut());
            parameters.put("link", auctionBody.getLink());
            parameters.put("is_anonymous", auctionBody.getIsAnonymous());
            auctionBody.setId((Integer) simpleJdbcInsert.executeAndReturnKey(parameters));
            logger.log(Level.INFO, () ->
                    String.format("Auction %s was inserted.", auctionBody.getTitle()));

            insertNewAuctionPictures(auctionBody, auctionBody.getId());
            if (AUCTION_TYPE.equals("FullAuction")) {
                invitationRepository.insertPeopleToInvite(auctionBody);
            }
            logger.log(Level.INFO,"Insert new auction ended");
            return auctionBody;
        } catch (DataAccessException e) {
            e.getMessage();
            logger.log(Level.SEVERE, "Insert new auction error ended " + e.toString(), e);
            throw new RuntimeException();
        }
    }

    public List<AuctionBody> findAllAuctionsWithTypesAndStatuses(List<AuctionTypeEnum> types,
                                                                 List<AuctionStatusEnum> statuses) {

        List<String> typeList = new ArrayList<>();
        List<String> statusList = new ArrayList<>();

        for (AuctionTypeEnum type : types) {
            typeList.add(type.name());
        }
        for (AuctionStatusEnum status : statuses) {
            statusList.add(status.name());
        }
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("listOfStatuses", statusList);
        parameters.put("listOfTypes", typeList);

        List<AuctionBody> temp = namedParameterJdbctemplate.query(FIND_AUCTIONS_BY_TYPE_STATUS, parameters, auctionJDBCMapper);
        return temp;
    }

    public void insertNewAuctionPictures(AuctionBody auctionBody, int auctionId) {
		if (auctionBody.getListOfPhotosUUUID() != null)
            for (UUID photoUuid : auctionBody.getListOfPhotosUUUID()) {
                byte[] temporaryPicture = pictureRepository.getTemporaryPicture(photoUuid);
                String temporaryPictureName = getTemporaryPictureName(photoUuid);
                jdbcTemplate.update(INSERT_PICTURE, auctionId, temporaryPictureName,
                        temporaryPicture);
            }
    }

    private String getTemporaryPictureName(UUID uuid) {
        logger.log(Level.INFO,"Query for get temporary picture name");
        List<String> picture = this.jdbcTemplate.queryForList(FIND_AUCTION_PICTURES_NAME_BY_ID, new Object[]{uuid}, String.class);
        if (picture.isEmpty()) {
            logger.log(Level.WARNING,"Query for get temporary picture name ended with null.");
            return null;
        }
        logger.log(Level.INFO,"Query for get temporary picture name ended");
        return picture.get(0);
    }

    @Override
    public AuctionBody findAuction(int id) {
        AuctionBody auctionBody = jdbcTemplate.queryForObject(FIND_AUCTION_QUERY, new Object[]{id},
                auctionJDBCMapper);
        auctionBody.setInvitations(findInvitations(auctionBody));
        return auctionBody;
    }

    @Override
    public AuctionBody findAnonymousAuction(UUID id, UserEnum userEnum) {
        logger.log(Level.INFO,"Query for find anonymous auction");
        AuctionBody auctionBody = null;

        if(userEnum.equals(UserEnum.OWNER))
            auctionBody = jdbcTemplate.queryForObject(FIND_AUCTION_QUERY_BY_OWNER_UUID,
                new Object[]{id.toString()}, auctionJDBCMapper);
        else
            auctionBody = jdbcTemplate.queryForObject(FIND_AUCTION_QUERY_BY_UUID,
                    new Object[]{id.toString()}, auctionJDBCMapper);

        auctionBody.setInvitations(findInvitations(auctionBody));
        logger.log(Level.INFO,"Query for find anonymous auction ended");
        return auctionBody;
    }

    private List<Invitation> findInvitations(AuctionBody auctionBody) {
        List<Invitation> invitations;
        if  (!auctionBody.getIsAnonymous()) {
            invitations = invitationRepository.findInvitations(auctionBody.getId());
        } else{
            invitations = invitationRepository.findAnonymousInvitations(auctionBody.getId());
        }
        return invitations;
    }

    @Override
    public int findAuctionIdByUUID(UUID id) {
        logger.log(Level.INFO,"Query for find auction id by uuid");
        List<Invitation> invitation = jdbcTemplate.query(FIND_AUCTION_ID_BY_UUID, new Object[]{id.toString()}, anonymousInvitationMapper);
        if (invitation.size() <= 0){
            logger.log(Level.WARNING,"Query for find auction id by uuid ended returns -1.");
            return -1;
        }
        logger.log(Level.INFO,"Query for find auction id by uuid ended");
        return invitation.get(0).getAuctionId();
    }

    @Override
    public int findAnonymousAuctionIdByUUID(UUID id) {
        logger.log(Level.INFO,"Query for find anonymous auction id by uuid");
        List<Invitation> invitation = jdbcTemplate.query(FIND_AUCTION_ID_BY_UUID, new Object[]{id.toString()}, anonymousInvitationMapper);
        if (invitation.size() <= 0){
            logger.log(Level.WARNING,"Query for find anonymous auction id by uuid ended returns -1.");
            return -1;
        }
        logger.log(Level.INFO,"Query for find anonymous auction id by uuid ended");
        return invitation.get(0).getAuctionId();
    }

    @Override
    public UUID findAuctionUUIDByEmail(String email, int auctionId) {
        logger.log(Level.INFO,"Query for find auction uuid by email");
        UUID auctionUUIDByEmail = UUID.fromString(jdbcTemplate.queryForObject(FIND_ANONYMOUS_USER_UUID_BY_EMAIL, new Object[]{email, auctionId}, String.class));
        logger.log(Level.INFO,"Query for find auction uuid by email ended");
        return auctionUUIDByEmail;
    }


    @Override
    public String findIfAnonymousOrFullAccessAuction(int auctionId) {
        logger.log(Level.INFO,"Query for find if anonymous or full access auction");
        String email = jdbcTemplate.queryForObject(FIND_IF_ANONYMOUS_OR_FULL_ACCESS_AUCTION, new Object[]{auctionId},
                String.class);
        logger.log(Level.INFO,"Query for find if anonymous or full access auction ended");
        return email;
    }

    @Override
    public boolean checkIfAnonymousAuctionOwner(UUID uuid) {
        logger.log(Level.INFO,"Query for check if anonymous auction owner");
        List<Map<String, Object>> list = jdbcTemplate.queryForList(FIND_AUCTION_BY_UUID, new Object[]{uuid.toString()});
        logger.log(Level.INFO,"Query for check if anonymous auction owner ended returns for user " + uuid + " " + (list.size() != 0));
        return list.size() != 0;
    }

    @Override
    public String findAnonymousOwnerEmailById(int id) {
        logger.log(Level.INFO,"Query for find anonymous auction owner email by id");
        String email = jdbcTemplate.queryForObject(FIND_ANONYMOUS_AUCTION_OWNER_EMAIL_BY_ID, new Object[]{id},
                String.class);
        logger.log(Level.INFO,"Query for find anonymous auction owner email by id ended");
        return email;
    }

    @Override
    public List<AuctionBody> findAll() {
        logger.log(Level.INFO,"Query for find all auctions");
        List<AuctionBody> allAuctions = this.jdbcTemplate.query(FIND_ALL_AUCTIONS_QUERY, auctionJDBCMapper);
        logger.log(Level.INFO,"Query for find all auctions ended");
        return allAuctions;
    }

    private int priceReturnValue(String selectSentence) {
        List<Map<String, Object>> resultOfMinimumPrice = jdbcTemplate.queryForList(selectSentence);
        if (resultOfMinimumPrice.size() == 0) {
            return 0;
        }
        String tmp = resultOfMinimumPrice.get(0).toString();
        double result = Double.parseDouble(tmp.split("=")[1].split("}")[0]);  //ToDo Check logic
        return (int) result;
    }


    @Override
    public int findMinPrice(String sort) {
        int minPrice = 1;
        logger.log(Level.INFO,"Query for find min price");
        if  (sort.equals("MY"))
            minPrice = priceReturnValue(FIND_AUCTION_MIN_PRICE);
        else
            minPrice = priceReturnValue(FIND_AUCTION_MIN_PRICE_WITHOUT_DELETED_AUCTIONS);
        logger.log(Level.INFO,"Query for find min price ended");
        return minPrice;
    }

    @Override
    public int findMaxPrice(String sort) {
        int maxPrice = 1;
        logger.log(Level.INFO,"Query for find max price");
        if  (sort.equals("MY"))
            maxPrice = priceReturnValue(FIND_AUCTION_MAX_PRICE);
        else
            maxPrice = priceReturnValue(FIND_AUCTION_MAX_PRICE_WITHOUT_DELETED_AUCTIONS);
        logger.log(Level.INFO,"Query for find max price ended");
        return maxPrice;
    }

    @Override
    public List<AuctionBody> findAuctionsByType(String type) {
        logger.log(Level.INFO,"Query for find auctions by type");
        List<AuctionBody> auctionsByType = this.jdbcTemplate.query(FIND_TYPE_AUCTIONS_QUERY, new Object[]{type}, auctionJDBCMapper);
        logger.log(Level.INFO,"Query for find auctions by type ended");
        return auctionsByType;
    }

    @Override
    public List<AuctionBody> findMyAuctions(int userId) {
        logger.log(Level.INFO,"Query for find my auctions");
        List<AuctionBody> myAuctions = this.jdbcTemplate.query(FIND_MY_AUCTIONS_QUERY, new Object[]{userId}, auctionJDBCMapper);
        logger.log(Level.INFO,"Query for find my auctions ended");
        return myAuctions;
    }

    @Override
    public byte[] findAuctionPictureBytes(int auctionId, int pictureId) {
        logger.log(Level.INFO,"Query for find auction picture bytes");
        List<byte[]> queryForList = jdbcTemplate.queryForList(SELECT_PICTURE, new Object[]{auctionId}, byte[].class);
        if (queryForList.isEmpty()) {
            logger.log(Level.INFO,"Query for find auction picture bytes ended return no image.");
            return null;
        }
        logger.log(Level.INFO,"Query for find auction picture bytes ended");
        return queryForList.get(pictureId);
    }


    public byte[] getDefaultPicture() {
    	return DEFAULT_PICTURE;
    }
    
    @Override
    public void updateAuction(AuctionBody auctionBody) {
        logger.log(Level.INFO,"Query for update auction");
        jdbcTemplate.update(UPDATE_AUCTION, auctionBody.getStatus().toString(), Timestamp.valueOf(auctionBody.getStartDate()),Timestamp.valueOf(auctionBody.getEndDate()), auctionBody.getItemQuantity(),
                auctionBody.getStartingPrice(), auctionBody.getDescription(), auctionBody.getLink(), auctionBody.getId());
        insertNewAuctionPictures(auctionBody, auctionBody.getId());
        logger.log(Level.INFO,"Query for update auction ended");
    }

    @Override
    public void updateAuctionEndTimeOnBid(int auctionId, LocalDateTime endTime) {
        logger.log(Level.INFO,"Query for update auction end time on bid");
        jdbcTemplate.update(UPDATE_AUCTION_END_TIME_ON_BID, Timestamp.valueOf(endTime), auctionId);
        logger.log(Level.INFO,"Query for update auction end time on bid ended");
    }

    @Override
    public void changeAuctionStatus(AuctionStatusEnum status, int auctionId) {
        logger.log(Level.INFO,"Query for update auction status");
        jdbcTemplate.update(CHANGE_STATUS, status.name(), auctionId);
        logger.log(Level.INFO,"Query for update auction status ended");
    }
    @Override
    public List<AuctionBody> findUserPrivateAuctions(String email, InvitationStatusEnum status,
                                                     List<AuctionStatusEnum> auctionStatusEnums) {
        logger.log(Level.INFO,"Query for find user private auctions");
        List<AuctionBody> ret = new ArrayList<AuctionBody>();

        List<Invitation> invitations = this.jdbcTemplate.query(FIND_USER_PRIVATE_AUCTIONS_QUERY,
                new Object[]{email, status.toString()}, invitationMapper);

        for (Invitation invitation : invitations) {
            AuctionBody auctionBody = findAuction(invitation.getAuctionId());
            if (auctionStatusEnums.contains(auctionBody.getStatus())
                    && auctionBody.getType().equals(AuctionTypeEnum.PRIVATE))
                ret.add(auctionBody);
        }
        logger.log(Level.INFO,"Query for find user private auctions ended");
        return ret;
    }

    @Override
    public int getItemQuantityLeft(int auction_id) {
        logger.log(Level.INFO,"Query for get item quantity left");
        int itemQuantityLeft = jdbcTemplate.queryForObject(GET_ITEM_QUANTITY_LEFT, new Object[]{auction_id}, Integer.class);
        logger.log(Level.INFO,"Query for get item quantity left ended");
        return itemQuantityLeft;
    }

    @Override
    public void setSubtractSoldItemQuantity(int quantity, int auction_id) {
        logger.log(Level.INFO,"Query for update sold item quantity");
        jdbcTemplate.update(SUBTRACT_SOLD_ITEM_QUANTITY, quantity, auction_id);
        logger.log(Level.INFO,"Query for update sold item quantity ended");
    }
}
