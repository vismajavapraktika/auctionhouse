package com.visma.seli.server.data.access.mappers.jdbc.auction;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.visma.seli.service.auction.comment.Comment;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class CommentMapper implements RowMapper<Comment> {

    @Override
    public Comment mapRow(ResultSet rs, int arg1) throws SQLException {
        Comment c = new Comment();
        c.setId(rs.getInt("id"));
        c.setUserId(rs.getInt("user_id"));
        c.setAuctionId(rs.getInt("auction_id"));
        c.setName(rs.getString("name"));
        c.setCreateDate(rs.getTimestamp("created").toLocalDateTime());
        c.setText((rs.getString("text")));
        return c;
    }

}