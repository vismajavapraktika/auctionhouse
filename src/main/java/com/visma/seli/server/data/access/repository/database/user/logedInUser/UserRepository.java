package com.visma.seli.server.data.access.repository.database.user.logedInUser;

import com.visma.seli.service.user.UserBody;

import java.util.List;
import java.util.UUID;

public interface UserRepository {
    UserBody insertNewUser(UserBody userBody);

    UserBody insertNewGoogleUser(UserBody userBody);

    UserBody getUserByEmail(String login);

    UserBody getUserById(int id);

    List<UserBody> getUsersByIds(List<Integer> ids);

    int getInvitedUserIdByUUID(UUID id);

    List<String> getEmails();

    List<UserBody> getAllUsers();

    void changeAnonymousUsersSentEmailAboutEndedAuctionStatus(int id);

    UserBody getOwnerByAuctionId(int auctionId);

	String getInvitedUserEmailByUUID(UUID id);
}
