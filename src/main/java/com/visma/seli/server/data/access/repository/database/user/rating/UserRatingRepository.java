package com.visma.seli.server.data.access.repository.database.user.rating;


public interface UserRatingRepository {
    void insertRating(float rating, int rater, int rate, int auctionId);

    void updateRating(float rating, int rater, int rate, int auctionId);

    void delete(int rater, int rate, int auctionId);

    Float findUserAvarageRating(int userId);

    Float findRating(int rater, int rate, int id);

    boolean checkIfNotRated(int rater, int ratee, int auctionId);

    void deleteRatingByAuctionId(int id);
}