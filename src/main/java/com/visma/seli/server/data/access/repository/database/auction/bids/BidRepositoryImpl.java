package com.visma.seli.server.data.access.repository.database.auction.bids;

import com.visma.seli.server.data.access.mappers.jdbc.auction.AuctionJDBCMapper;
import com.visma.seli.server.data.access.mappers.jdbc.auction.BidMapper;
import com.visma.seli.server.data.access.mappers.jdbc.user.UserBodyMapper;
import com.visma.seli.server.data.access.repository.database.user.anonymousUser.AnonymousUserRepository;
import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.bid.Bid;
import com.visma.seli.service.user.UserBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class BidRepositoryImpl implements BidRepository {
    private static final Logger logger = Logger.getLogger(Class.class.getName());

    private static final String FIND_MY_BIDDED_AUCTIONS_QUERY = "SELECT * FROM auctions WHERE id IN (SELECT auction_id FROM bid WHERE user_id = ?)";
    private static final String FIND_ALL_AUCTION_BIDS_QUERY = "SELECT * FROM bid WHERE auction_id = ?";
    private static final String FIND_LAST_BIDDER_QUERY = "SELECT * FROM users WHERE id = (SELECT user_id FROM bid WHERE auction_id = ? ORDER BY id DESC LIMIT 1) LIMIT 1";
    private static final String FIND_HIGHEST_BIDDER_QUERY = "SELECT * FROM users WHERE id = (SELECT user_id FROM bid WHERE auction_id = ? ORDER BY amount DESC LIMIT 1) LIMIT 1";
    private static final String FIND_LAST_BID_QUERY = "SELECT * FROM bid WHERE auction_id = ? ORDER BY id DESC LIMIT 1";
    private static final String FIND_AUCTION_HIGHEST_BIDS_FOR_DIFF_USERS_QUERY = "SELECT * FROM bid JOIN (SELECT  user_id, max(amount) as amount from bid where auction_id = ? group by user_id) as max_bids on bid.user_id = max_bids.user_id and bid.amount = max_bids.amount where auction_id = ?;";
    private static final String FIND_ACTIVE_AUCTIONS_ORDERED_BY_BIDS_COUNT = "select * from auctions inner join (select auction_id, count(auction_id) from bid GROUP BY auction_id order by (count(auction_id)) desc) as auctions_popularity ON auctions_popularity.auction_id = auctions.id AND auctions.status = 'ACTIVE' AND auctions.type = 'PUBLIC'";
    private static final String FIND_AUCTION_HIGHEST_BID = "SELECT * FROM bid WHERE auction_id = ? ORDER BY amount DESC LIMIT 1";
    private static final String FIND_ALL_ENDED_AUCTIONS_BIDS = "SELECT id, auction_id, user_id, amount, quantity FROM bids_winners WHERE auction_id = ? ORDER BY amount DESC";
    @Autowired
    AnonymousUserRepository anonymousUser;
    @Autowired
    private DataSource dataSource;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private AuctionJDBCMapper auctionJDBCMapper;
    @Autowired
    private BidMapper bidMapper;

    @Override
    public List<Bid> findAllAuctionBids(int auctionId) {
        return this.jdbcTemplate.query(FIND_ALL_AUCTION_BIDS_QUERY, new Object[]{auctionId}, new BidMapper());
    }

    @Override
    public List<AuctionBody> findActiveAuctionsOrderedByBidsCount() {
        return this.jdbcTemplate.query(FIND_ACTIVE_AUCTIONS_ORDERED_BY_BIDS_COUNT, auctionJDBCMapper);
    }

    @Override
    public UserBody findLastBidder(int auctionId) {
        logger.log(Level.INFO, "Query find last bidder");
        List<UserBody> lastBidder;
        lastBidder = jdbcTemplate.query(FIND_LAST_BIDDER_QUERY, new Object[]{auctionId}, new UserBodyMapper());
        if (lastBidder.isEmpty()) {
            logger.log(Level.WARNING, "Query find last bidder ended with null. Auction id " + auctionId);
            return null;
        } else {
            logger.log(Level.INFO, "Query find last bidder ended");
            return lastBidder.get(0);
        }
    }
//    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public UserBody findHighestBidder(int auctionId) {
        List<UserBody> highestBidder;
        highestBidder = jdbcTemplate.query(FIND_HIGHEST_BIDDER_QUERY, new Object[]{auctionId}, new UserBodyMapper());
        if (highestBidder.isEmpty()) {
            return null;
        } else {
            return highestBidder.get(0);
        }
    }

    @Override
    public Bid findLastBid(int auctionId) {
        logger.log(Level.INFO, "Query for find last bid");
        List<Bid> lastBid;
        lastBid = jdbcTemplate.query(FIND_LAST_BID_QUERY, new Object[]{auctionId}, new BidMapper());
        if (lastBid.isEmpty()) {
            return null;
        } else {
            logger.log(Level.INFO,"Query for find last bid ended");
            return lastBid.get(0);
        }
    }

    @Override
    public Bid findHighestBid(int auctionId) {
        List<Bid> highestBid = jdbcTemplate.query(FIND_AUCTION_HIGHEST_BID, new Object[]{auctionId}, new BidMapper());
        if (highestBid.isEmpty()) {
            return null;
        } else {
            return highestBid.get(0);
        }
    }

    @Override
    public List<AuctionBody> findMyBiddedAuctions(int userId) {
        return this.jdbcTemplate.query(FIND_MY_BIDDED_AUCTIONS_QUERY, new Object[]{userId}, new AuctionJDBCMapper());
    }

    @Override
    public Bid insertNewBid(Bid bid) {
        logger.log(Level.INFO, "Bid insert to table bid");
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("bid")
                .usingGeneratedKeyColumns("id");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("auction_id", bid.getAuctionId());
        parameters.put("amount", bid.getAmount());
        parameters.put("user_id", bid.getUserId());
        parameters.put("quantity", bid.getBiddingQuantity());
        bid.setId((Integer) simpleJdbcInsert.executeAndReturnKey(parameters));
        logger.log(Level.INFO,"Bid insert to table bid ended");
        return bid;
    }

    @Override
    public List<Bid> findUsersHighestBids(int auctionId) {
        return jdbcTemplate.query(FIND_AUCTION_HIGHEST_BIDS_FOR_DIFF_USERS_QUERY,
                new Object[]{auctionId, auctionId}, new BidMapper());
    }

    @Override
    public List<Bid> findAllEndedAuctionBids(int auctionId) {
        return jdbcTemplate.query(FIND_ALL_ENDED_AUCTIONS_BIDS, new Object[]{auctionId}, bidMapper);
    }
}
