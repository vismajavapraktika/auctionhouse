package com.visma.seli.server.data.access.mappers.jdbc.auction;

import com.visma.seli.service.auction.Picture;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class PictureMapper implements RowMapper<Picture> {
    @Override
    public Picture mapRow(ResultSet resultSet, int i) throws SQLException {
        Picture picture = new Picture();
        picture.setPictureId(resultSet.getInt("id"));
        picture.setPhotoAuctionId(resultSet.getInt("auction_id"));
        picture.setPictureName(resultSet.getString("photo_name"));
        picture.setPictureBytes(resultSet.getBytes("bytes"));
        return picture;
    }
}
