package com.visma.seli.server.data.access.repository.database.user.anonymousUser;

import com.visma.seli.service.auction.Invitation;
import com.visma.seli.service.user.AnonymousUser;

import java.util.List;

public interface AnonymousUserRepository {
    AnonymousUser insertNewAnonymousUser(AnonymousUser anonymousUser);

    AnonymousUser getAnonymousUserByEmail(String login);

    AnonymousUser getAnonymousUserById(int id);

    List<String> getAnonymousEmails();

    List<AnonymousUser> getAnonymousUsers(int id);

    AnonymousUser checkIfUserExistsInJDBC(String email);

    void notificationHasBeenSendToUser(int userId, int auctionId);

    boolean getIfNotifiedAboutAuctionEnd(int userId, int auctionId);

    boolean getIfUserExistsInNotifiedAnonymousAuctionUserTable(int id, int id1);

    void insertToAnonymousAuctionOwnerTable(int id, int id1);

    Invitation getAnonymousInvitation(int anonymousUserId, int auctionId);
}
