package com.visma.seli.server.data.access.repository.database.user.rating;

import com.visma.seli.server.data.access.mappers.jdbc.user.UserRatingMapper;
import com.visma.seli.service.user.rating.UserRating;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class UserRatingRepositoryImpl implements UserRatingRepository {

    private static final String FIND_USER_AVARAGE_RATING_QUERY = "SELECT AVG(rating) FROM user_rating WHERE ratee_id = ?";
    private static final String FIND_RATING_QUERY = "SELECT rating FROM user_rating WHERE rater_id = ? AND ratee_id = ? AND auction_id = ?";
    private static final String UPDATE_RATING_QUERY = "UPDATE user_rating set rating = ? where rater_id = ? AND ratee_id = ? AND auction_id = ?";
    private static final String DELETE_AUCTION = "DELETE FROM user_rating WHERE rater_id = ? AND ratee_id = ? AND auction_id = ?";
    private static final String CHCEK_IF_USER_WAS_RATED = "SELECT * FROM user_rating WHERE rater_id = ? AND ratee_id = ? AND auction_id = ?";
    private static final String DELETE_RATING_BY_AUCTION_ID = "DELETE FROM user_rating WHERE auction_id = ?";

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private DataSource dataSource;
    @Autowired
    private UserRatingMapper userRatingMapper;

    @Override
    public Float findUserAvarageRating(int userId) {
        List<Float> rating = jdbcTemplate.queryForList(FIND_USER_AVARAGE_RATING_QUERY, new Object[]{userId}, Float.class);
        return (rating == null || rating.iterator().next() == null) ? null : rating.iterator().next();
    }

    @Override
    public Float findRating(int rater, int rate, int id) {
        List<Float> rating = jdbcTemplate.queryForList(FIND_RATING_QUERY, new Object[]{rater, rate, id}, Float.class);
        return (rating == null || rating.isEmpty()) ? null : rating.iterator().next();
    }

    @Override
    public boolean checkIfNotRated(int rater, int ratee, int auctionId) {
        List<UserRating> userRatingList = jdbcTemplate.query(CHCEK_IF_USER_WAS_RATED, new Object[]{rater, ratee,auctionId}, userRatingMapper);
        return userRatingList.size() ==0;
    }

    @Override
    public void deleteRatingByAuctionId(int id) {
        jdbcTemplate.update(DELETE_RATING_BY_AUCTION_ID, id);
    }

    @Override
    public void insertRating(float rating, int rater, int rate, int auctionId) {
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("user_rating")
                .usingGeneratedKeyColumns("id");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("rating", rating);
        parameters.put("rater_id", rater);
        parameters.put("ratee_id", rate);
        parameters.put("auction_id",auctionId);
        simpleJdbcInsert.execute(parameters);

    }

    @Override
    public void updateRating(float rating, int rater, int ratee, int auctionId) {
        jdbcTemplate.update(UPDATE_RATING_QUERY, rating, rater, ratee, auctionId);
    }

    @Override
    public void delete(int rater, int rate, int auctionId) {
        jdbcTemplate.update(DELETE_AUCTION, rater, rate,auctionId);
    }
}