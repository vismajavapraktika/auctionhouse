package com.visma.seli.server.data.access.repository.database.pictures;

import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.Picture;

import java.util.List;
import java.util.UUID;

public interface PictureRepository {
    int findAuctionPicturesCount(int auctionId);

    Picture insertTemporaryPicture(String pictureName, byte[] pictureBytes, int userId);

    byte[] getTemporaryPicture(UUID temporaryPictureIdList);

    void deleteTemporaryPicture(UUID pictureUUID);

    List<Picture> getPictureList(int auctionId);

    void deletePicture(int pictureId);

    Picture getAuctionPictureByPictureId(int pictureId);

    List<Picture> getTemporaryPicturesList();

    void deleteTemporaryPicture(int pictureId);

    void deleteAuctionTemporaryPictures(AuctionBody auctionBody);

    List<Picture> getAllPictures();

    void updatePictures(Picture resizePictures);
}
