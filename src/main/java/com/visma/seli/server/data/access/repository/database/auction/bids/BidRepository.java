package com.visma.seli.server.data.access.repository.database.auction.bids;

import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.bid.Bid;
import com.visma.seli.service.user.UserBody;

import java.util.List;

public interface BidRepository {
    Bid insertNewBid(Bid bid);

    Bid findLastBid(int auctionId);

    List<Bid> findAllAuctionBids(int id);

    List<AuctionBody> findMyBiddedAuctions(int id);

    UserBody findLastBidder(int auctionId);

    UserBody findHighestBidder(int auctionId);

    List<Bid> findUsersHighestBids(int auctionId);

    List<AuctionBody> findActiveAuctionsOrderedByBidsCount();

    Bid findHighestBid(int auctionId);

    List<Bid> findAllEndedAuctionBids(int auctionId);
}
