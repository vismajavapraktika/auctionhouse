package com.visma.seli.server.data.access.mappers.jdbc.auction;

import com.visma.seli.service.auction.bid.Bid;
import com.visma.seli.service.auction.bid.WinMessageData;
import com.visma.seli.service.enums.WinnerMessageResponseStatusEnum;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

@Component
public class BidWinnerMapper implements RowMapper<WinMessageData> {
    @Override
    public WinMessageData mapRow(ResultSet rs, int i) throws SQLException {
        Bid bid = new Bid(rs.getInt("id"), rs.getInt("auction_id"), rs.getInt("user_id"), rs.getBigDecimal("amount"), rs.getInt("quantity"));
        LocalDateTime expirationTime = null;

        if (rs.getTimestamp("expiration_time") != null) {
            expirationTime = rs.getTimestamp("expiration_time").toLocalDateTime();
        }
        return new WinMessageData(bid, WinnerMessageResponseStatusEnum.valueOf(rs.getString("message_status")),
                expirationTime);
    }
}
