package com.visma.seli.server.data.access.repository.database.auction.won;

import com.visma.seli.server.data.access.mappers.jdbc.auction.BidMapper;
import com.visma.seli.server.data.access.mappers.jdbc.auction.BidWinnerMapper;
import com.visma.seli.server.data.access.mappers.jdbc.auction.WinMessageDataMapper;
import com.visma.seli.server.data.access.repository.database.NotificationsRepository;
import com.visma.seli.server.data.access.repository.database.user.anonymousUser.AnonymousUserRepository;
import com.visma.seli.server.data.access.repository.database.user.logedInUser.UserRepository;
import com.visma.seli.service.auction.bid.Bid;
import com.visma.seli.service.auction.bid.WinMessageData;
import com.visma.seli.service.enums.WinnerMessageResponseStatusEnum;
import com.visma.seli.service.user.AnonymousUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static com.visma.seli.service.enums.NotificationEnum.AUCTION_WINNER;
import static com.visma.seli.service.enums.WinnerMessageResponseStatusEnum.*;


@Component
public class WonAuctionsRepositoryImpl implements WonAuctionsRepository {
    private static final Logger logger = Logger.getLogger(Class.class.getName());

    private static final String FIND_WON_BIDS_BY_USER_QUERY = "SELECT * FROM bids_winners WHERE user_id = ?";
    private static final String FIND_NEXT_HIGHEST_WINNER_BID = "SELECT * FROM bids_winners where auction_id = ? AND message_status = ? and bids_winners.amount = " +
            "(SELECT max(amount)FROM bids_winners WHERE auction_id = ? AND message_status = ?)";
    private static final String SET_NOTIFICATION_EXPIRATION_DATE = "UPDATE bids_winners SET expiration_time = ?, message_status = ? where id = ?";

    private static final String UPDATE_NEXT_HIGHEST_WINNER_BID_INFORMATION = "UPDATE bids_winners SET message_status = ?, expiration_time = ? WHERE id = ?";

    private static final String UPDATE_STATUS_FOR_EXPIRED_WIN = "UPDATE bids_winners SET message_status = ? " +
            "where message_status = ? and expiration_time < now();";

    private static final String FIND_ALL_WINNERS_TO_NOTIFY = "SELECT * FROM " +
            "(SELECT auction_id, MAX(amount) as max_amount FROM bids_winners " +
            "WHERE message_status = ? and auction_id NOT IN " +
            "(SELECT auction_id from bids_winners WHERE message_status = ?) group by auction_id) as max " +
            "JOIN bids_winners as bw on bw.auction_id = max.auction_id and max.max_amount = bw.amount";

    private static final String FIND_CURRENT_AUCTION_WINNER = "SELECT * FROM bids_winners where auction_id = ? and message_status = ?";
    private static final String SET_MESSAGE_STATUS = "UPDATE bids_winners SET message_status = ? WHERE auction_id = ? and user_id = ?";
    private static final String GET_WINS_POSITION = "SELECT COUNT(amount)+1 FROM bids_winners WHERE auction_id = ? and amount > " +
            "(SELECT amount FROM bids_winners where auction_id = ? and user_id = ?)";
    private static final String GET_WIN_MESSAGE_DATA = "SELECT * FROM bids_winners WHERE auction_id = ? and user_id = ?";
    private static final String UPDATE_AUCTION_STATUSES = "UPDATE bids_winners SET message_status = ? where message_status = ? and auction_id = ?";
    private static final String GET_WINNERS_LEFT = "SELECT COUNT(*) FROM bids_winners where auction_id = ? and message_status = ?";

    @Autowired
    private DataSource dataSource;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private WinMessageDataMapper winMessageDataMapper;
    @Autowired
    private AnonymousUserRepository anonymousUserRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private NotificationsRepository notificationsRepository;
    @Autowired
    private BidWinnerMapper bidWinnerMapper;


    @Override
    public void insertNewBidWinner(Bid bid) {
        logger.log(Level.INFO,"Insert new bid winner. User " + bid.getUserId() + " auction " + bid.getAuctionId() + " bid amount " + bid.getAmount());
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("bids_winners")
                .usingGeneratedKeyColumns("id");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("auction_id", bid.getAuctionId());
        parameters.put("amount", bid.getAmount());
        parameters.put("user_id", bid.getUserId());
        parameters.put("quantity", bid.getBiddingQuantity());
        if (isUserAnonymous(bid.getUserId())) {
            parameters.put("message_status", ANONYMOUS.name());
        } else {
            parameters.put("message_status", NOT_SENT.name());
            notificationsRepository.insertToNotificationsTable(AUCTION_WINNER, userRepository.getUserById(bid.getUserId()).getEmail(), bid.getAuctionId());
        }
        simpleJdbcInsert.execute(parameters);
        logger.log(Level.INFO,"Insert new bid winner ended");
    }

    private boolean isUserAnonymous(int userId) {
        AnonymousUser anonymousUser = anonymousUserRepository.getAnonymousUserById(userId);
        return anonymousUser != null;
    }

    @Override
    public List<Bid> findByUser(int userId) {
        logger.log(Level.INFO,"Query for find bids by user");
        List<Bid> findByUser = this.jdbcTemplate.query(FIND_WON_BIDS_BY_USER_QUERY, new Object[]{userId}, new BidMapper());
        logger.log(Level.INFO,"Query for find bids by user ended");
        return findByUser;
    }

    @Override
    public void updateAllExpiredWinStatus() {
        logger.log(Level.INFO,"Query for update all expired win status");
        jdbcTemplate.update(UPDATE_STATUS_FOR_EXPIRED_WIN, EXPIRED.name(), WAITING_ANSWER.name());
        logger.log(Level.INFO,"Query for update all expired win status ended");
    }

    @Override
    public List<WinMessageData> selectAllWinnersToNotify() {
        logger.log(Level.INFO,"Query for find all winners to notify");
        List<WinMessageData> allWinnersToNotify = jdbcTemplate.query(FIND_ALL_WINNERS_TO_NOTIFY,
                new Object[]{NOT_SENT.name(), WAITING_ANSWER.name()}, winMessageDataMapper);
        logger.log(Level.INFO,"Query for find all winners to notify ended");
        return allWinnersToNotify;
    }

    @Override
    public void setNewMessageExpirationTimeStatus(WinMessageData winMessageData) {
        logger.log(Level.INFO,"Query for update new message expiration time status");
        jdbcTemplate.update(SET_NOTIFICATION_EXPIRATION_DATE, Timestamp.valueOf(winMessageData.getExpirationTime()),
                WAITING_ANSWER.name(), winMessageData.getBid().getId());
        logger.log(Level.INFO,"Query for update new message expiration time status ended");
    }

    @Override
    public WinMessageData getCurrentWinnerByAuctionId(int id) {
        logger.log(Level.INFO,"Query for find current auction winner");
        List<WinMessageData> winList = jdbcTemplate.query(FIND_CURRENT_AUCTION_WINNER, new Object[]{id, WAITING_ANSWER.name()}, winMessageDataMapper);
        if (winList.isEmpty()) {
            logger.log(Level.WARNING,"Query for find current auction winner ended return null.");
            return null;
        } else {
            logger.log(Level.INFO,"Query for find current auction winner ended");
            return winList.get(0);
        }
    }

    @Override
    public Integer updateStatusAfterWinnerAnswer(WinnerMessageResponseStatusEnum statusTo, int auctionId, int userId) {
        logger.log(Level.INFO,"Query for update status after winner answer");
        Integer statusAfterWinnerAnswer = jdbcTemplate.update(SET_MESSAGE_STATUS, statusTo.name(), auctionId, userId);
        logger.log(Level.INFO,"Query for update status after winner answer ended");
        return statusAfterWinnerAnswer;
    }

    @Override
    public int getWinnersPosition(int auction_id, int user_id) {
        logger.log(Level.INFO,"Query for getting winners position");
        int winnersPosition = jdbcTemplate.queryForObject(GET_WINS_POSITION, new Object[]{auction_id, auction_id, user_id}, Integer.class);
        logger.log(Level.INFO,"Query for getting winners position ended");
        return winnersPosition;
    }

    @Override
    public WinMessageData getWinMessageData(int auctionId, int userId) {
        logger.log(Level.INFO,"Query for getting win message data");
        List<WinMessageData> winList = jdbcTemplate.query(GET_WIN_MESSAGE_DATA, new Object[]{auctionId, userId}, winMessageDataMapper);
        if (winList.isEmpty()) {
            logger.log(Level.WARNING,"Query for getting win message data ended returns null.");
            return null;
        } else {
            logger.log(Level.INFO,"Query for getting win message data ended");
            return winList.get(0);
        }
    }

    @Override
    public void updateAuctionStatuses(WinnerMessageResponseStatusEnum from, WinnerMessageResponseStatusEnum to, int id) {
        logger.log(Level.INFO,"Query for update auction status");
        jdbcTemplate.update(UPDATE_AUCTION_STATUSES, to.name(), from.name(), id);
        logger.log(Level.INFO,"Query for update suction status ended");
    }


    @Override
    public int getWinnersLeft(int id) {
        logger.log(Level.INFO,"Query for getting winners left");
        int winnersLeft = jdbcTemplate.queryForObject(GET_WINNERS_LEFT, new Object[]{id, NOT_SENT.name()}, Integer.class);
        logger.log(Level.INFO,"Query for getting winners left ended");
        return winnersLeft;
    }

    @Override
    public List<WinMessageData> setNextWinnersStatusAndExpirationDate(int auctionId) {
        List<WinMessageData> winList = jdbcTemplate.query(FIND_NEXT_HIGHEST_WINNER_BID, new Object[]{auctionId, NOT_SENT.name(), auctionId, NOT_SENT.name()}, bidWinnerMapper);
        if (winList.isEmpty()){
            return null;
        }else {
            jdbcTemplate.update(UPDATE_NEXT_HIGHEST_WINNER_BID_INFORMATION, WAITING_ANSWER.name(), Timestamp.valueOf(LocalDateTime.now().plusDays(1)), winList.get(0).getBid().getId());
            return winList;
        }
    }
}
