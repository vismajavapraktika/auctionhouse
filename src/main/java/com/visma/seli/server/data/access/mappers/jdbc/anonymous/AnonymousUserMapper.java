package com.visma.seli.server.data.access.mappers.jdbc.anonymous;

import com.visma.seli.service.user.AnonymousUser;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class AnonymousUserMapper implements RowMapper<AnonymousUser> {

    @Override
    public AnonymousUser mapRow(ResultSet resultSet, int i) throws SQLException {
        int id = resultSet.getInt("id");
        String email = resultSet.getString("user_email");
        String phone = resultSet.getString("phone");
        return new AnonymousUser(id, email, phone);
    }
}
