package com.visma.seli.server.data.access.repository.database.auction.comment;

import com.visma.seli.service.auction.comment.Comment;
import java.util.List;

public interface CommentRepository {
    Comment insertNewComment(Comment comment);
	List<Comment> findAllAuctionComments(int auctionId);
}
