package com.visma.seli.server.data.access.mappers.jdbc.auction;

import com.visma.seli.service.enums.WinnerMessageResponseStatusEnum;
import com.visma.seli.service.auction.bid.Bid;
import com.visma.seli.service.auction.bid.WinMessageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;


@Component
public class WinMessageDataMapper implements RowMapper<WinMessageData> {

    @Autowired
    private BidMapper bidMapper;

    @Override
    public WinMessageData mapRow(ResultSet rs, int arg1) throws SQLException {
        Bid bid = bidMapper.mapRow(rs, arg1);
        LocalDateTime expirationTime = null;
        if (rs.getTimestamp("expiration_time") != null) {
            expirationTime = rs.getTimestamp("expiration_time").toLocalDateTime();
        }
        return new WinMessageData(bid, WinnerMessageResponseStatusEnum.valueOf(rs.getString("message_status")),
                expirationTime);
    }


}
