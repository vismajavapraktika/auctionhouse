package com.visma.seli.server.data.access.repository.database.auction.comment;

import com.visma.seli.server.data.access.mappers.jdbc.auction.CommentMapper;
import com.visma.seli.server.data.access.repository.database.user.anonymousUser.AnonymousUserRepository;
import com.visma.seli.service.auction.comment.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class CommentRepositoryImpl implements CommentRepository {
    private static final Logger logger = Logger.getLogger(Class.class.getName());

    private static final String FIND_ALL_AUCTION_COMMENTS_QUERY = "SELECT * FROM comments WHERE auction_id = ?";
    @Autowired
    AnonymousUserRepository anonymousUser;
    @Autowired
    private DataSource dataSource;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private CommentMapper commentMapper;

    @Override
    public List<Comment> findAllAuctionComments(int auctionId) {
        logger.log(Level.INFO,"Query for find all auction comments");
        List<Comment> componentList = jdbcTemplate.query(FIND_ALL_AUCTION_COMMENTS_QUERY, new Object[]{auctionId}, commentMapper);
        Collections.reverse(componentList);
        logger.log(Level.INFO,"Query for find all auction comments ended");
        return componentList;
    }

    @Override
    public Comment insertNewComment(Comment comment) {
        logger.log(Level.INFO,"Insert new comment to table comments");
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("comments")
                .usingGeneratedKeyColumns("id");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("auction_id", comment.getAuctionId());
        parameters.put("user_id", comment.getUserId());
        parameters.put("name", comment.getName());
        parameters.put("created", Timestamp.valueOf(comment.getCreateDate()));
        parameters.put("text", comment.getText());
        comment.setId((Integer) simpleJdbcInsert.executeAndReturnKey(parameters));
        logger.log(Level.INFO,"Insert new comment to table comments inserted");
        return comment;
    }
}
