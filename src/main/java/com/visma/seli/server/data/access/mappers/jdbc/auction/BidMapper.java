package com.visma.seli.server.data.access.mappers.jdbc.auction;

import com.visma.seli.service.auction.bid.Bid;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class BidMapper implements RowMapper<Bid> {

    @Override
    public Bid mapRow(ResultSet rs, int arg1) throws SQLException {
        return new Bid(rs.getInt("id"), rs.getInt("auction_id"), rs.getInt("user_id"), rs.getBigDecimal("amount"), rs.getInt("quantity"));
    }

}