package com.visma.seli.server.data.access.repository.database;

import com.visma.seli.server.data.access.mappers.jdbc.notification.NotificationsMapper;
import com.visma.seli.service.Notifications;
import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.enums.NotificationEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class NotificationsRepository {
    private static final String SELECT_NOTIFICATIONS_LIST_BY_EMAIL = "SELECT * FROM notifications WHERE email = ? AND new_notification = TRUE";
    private static final String UPDATE_NOTIFICATIONS_STATUS_BY_ID = "UPDATE notifications SET new_notification = ? WHERE id = ?";
    private static final String UPDATE_NOTIFICATIONS_STATUS_BY_AUCTION_ID = "UPDATE notifications SET new_notification = FALSE WHERE type = 'INVITATION' AND auction_id = ?";
    private static final String SELECT_IF_OVERBID_NOTIFICATION_EXISTS = "SELECT * FROM notifications WHERE auction_id = ? AND email = ? AND type = 'OVERBID'";
    private static final String DELETE_NOTIFICATION = "DELETE FROM notifications WHERE id = ?";
    private static final String FIND_NOTIFICATION_QUERY = "SELECT * FROM notifications WHERE id = ?";
    private static final String SELECT_ALL_NOTIFICATIONS = "SELECT * FROM notifications";
    
    @Autowired
    private DataSource dataSource;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private NotificationsMapper notificationsMapper;

    public void insertToNotificationsTable(NotificationEnum type, String email, int auctionId) {
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("notifications")
                .usingGeneratedKeyColumns("id");
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("type", type.toString());
        parameters.put("email", email);
        parameters.put("new_notification", true);
        parameters.put("auction_id", auctionId);
        parameters.put("insert_date", Timestamp.valueOf(LocalDateTime.now()));
        simpleJdbcInsert.execute(parameters);
    }

    public List<Notifications> getNotificationsByEmail(String email) {
        return jdbcTemplate.query(SELECT_NOTIFICATIONS_LIST_BY_EMAIL,
                new Object[]{email},notificationsMapper);
    }

    public void changeNotificationsStatus(int id, boolean notificationStatus) {
        jdbcTemplate.update(UPDATE_NOTIFICATIONS_STATUS_BY_ID, notificationStatus, id);
    }

    public void notificationChangeAfterAuctionStatusChange(AuctionBody auction) {
        jdbcTemplate.update(UPDATE_NOTIFICATIONS_STATUS_BY_AUCTION_ID,auction.getId());
    }

    public boolean checkIfUserHasNotificationAboutOverbid(int auctionId, String email) {
        List<Notifications> notificationsList = jdbcTemplate.query(SELECT_IF_OVERBID_NOTIFICATION_EXISTS, new Object[]{auctionId, email}, notificationsMapper);
        return notificationsList.isEmpty();
    }

    public void deleteNotification(int notificationId) {
        jdbcTemplate.update(DELETE_NOTIFICATION, notificationId);
    }

    public Notifications findNotification(int id) {
    	Notifications notification = jdbcTemplate.queryForObject(FIND_NOTIFICATION_QUERY, new Object[]{id},
    			notificationsMapper);
        return notification;
    }

    public List<Notifications> getAllNotifications() {
        return jdbcTemplate.query(SELECT_ALL_NOTIFICATIONS, notificationsMapper);
    }

}
