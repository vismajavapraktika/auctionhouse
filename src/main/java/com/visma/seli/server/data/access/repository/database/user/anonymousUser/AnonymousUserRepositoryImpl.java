package com.visma.seli.server.data.access.repository.database.user.anonymousUser;

import com.visma.seli.server.data.access.mappers.jdbc.anonymous.AnonymousInvitationMapper;
import com.visma.seli.server.data.access.mappers.jdbc.anonymous.AnonymousUserMapper;
import com.visma.seli.server.data.access.mappers.jdbc.auction.InvitationMapper;
import com.visma.seli.server.data.access.mappers.jdbc.user.NotifiedUserMapper;
import com.visma.seli.server.data.access.repository.database.user.NotifiedUser;
import com.visma.seli.service.auction.Invitation;
import com.visma.seli.service.user.AnonymousUser;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;
import java.util.Map;

@Repository
public class AnonymousUserRepositoryImpl implements AnonymousUserRepository {

    private static final String SELECT_ANONYMOUS_USER_WHEN_LOGIN = "SELECT * FROM anonymous_user WHERE user_email = ?";
    private static final String SELECT_EMAIL_FROM_VALIDATION = "SELECT email FROM anonymous_user";
    private static final String SELECT_USER_BY_ID_QUERY = "SELECT * FROM anonymous_user WHERE id = ?";
    private static final String UPDATE_USER_INFORMATION_SENT_STATUS = "UPDATE invitations_anonymous SET notified_about_auction_end = 'TRUE' where user_id = ? and auction_id = ?";
    private static final String SELECT_NOTIFIED_ABOUT_AUCTION_END = "SELECT * FROM invitations_anonymous WHERE user_id = ? and auction_id = ?";
    private static final String SELECT_ANONYMOUS_AUCTION_OWNER_USER = "SELECT * FROM notified_anonymous_auction_owners WHERE user_id = ? and auction_id = ?";
    private static final String SELECT_ANONYYMOUS_INVITATION_BY_USER_ID_AND_AUCTION_ID = "SELECT * FROM invitations_anonymous WHERE user_id = ? and auction_id = ?";
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private DataSource dataSource;
    @Autowired
    private AnonymousUserMapper anonymousUserMapper;
    @Autowired
    private InvitationMapper invitationMapper;
    @Autowired
    private NotifiedUserMapper notifiedUserMapper;
    @Autowired
    private AnonymousInvitationMapper anonymousInvitationMapper;

    @Override
    public AnonymousUser insertNewAnonymousUser(AnonymousUser anonymousUser) {
        Integer allUsersId = insertNewAllUsers();
        Map<String, Object> parameters = new HashedMap();
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("anonymous_user");
        parameters.put("id", allUsersId);
        parameters.put("user_email", anonymousUser.getEmail());
        parameters.put("phone", anonymousUser.getPhone());
        anonymousUser.setId(allUsersId);
        simpleJdbcInsert.execute(parameters);
        return anonymousUser;
    }

    private Integer insertNewAllUsers() {
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("all_users")
                .usingGeneratedKeyColumns("id");
        Map<String, Object> parameters = new HashedMap();
        parameters.put("type", "anonymous");
        return (Integer) simpleJdbcInsert.executeAndReturnKey(parameters);
    }

    @Override
    public AnonymousUser getAnonymousUserByEmail(String login) {
        List<AnonymousUser> anonymousUsersList;
        anonymousUsersList = jdbcTemplate.query(SELECT_ANONYMOUS_USER_WHEN_LOGIN, new Object[]{login}, anonymousUserMapper);
        if (anonymousUsersList.isEmpty()) {
            return null;
        } else {
            return anonymousUsersList.get(0);
        }
    }

    @Override
    public AnonymousUser getAnonymousUserById(int id) {
        List<AnonymousUser> anonymousUsersList;
        anonymousUsersList = jdbcTemplate.query(SELECT_USER_BY_ID_QUERY, new Object[]{id}, anonymousUserMapper);
        if (anonymousUsersList.isEmpty())
            return null;
        else
            return anonymousUsersList.get(0);
    }

    @Override
    public List<String> getAnonymousEmails() {
        return jdbcTemplate.queryForList(SELECT_EMAIL_FROM_VALIDATION, String.class);
    }

    @Override
    public List<AnonymousUser> getAnonymousUsers(int id) {
        List<AnonymousUser> anonymousUsersList;
        anonymousUsersList = jdbcTemplate.query(SELECT_USER_BY_ID_QUERY, new Object[]{id}, anonymousUserMapper);
        if (anonymousUsersList.isEmpty())
            return null;
        else
            return anonymousUsersList;
    }

    @Override
    public AnonymousUser checkIfUserExistsInJDBC(String email) {
        List<AnonymousUser> temporaryUser = jdbcTemplate.query(SELECT_ANONYMOUS_USER_WHEN_LOGIN, new Object[]{email},anonymousUserMapper);
        if (temporaryUser.isEmpty())
            return null;
        else
            return temporaryUser.get(0);
    }

    @Override
    public void notificationHasBeenSendToUser(int userId, int auctionId) {
        jdbcTemplate.update(UPDATE_USER_INFORMATION_SENT_STATUS,userId, auctionId);
    }

    @Override
    public boolean getIfNotifiedAboutAuctionEnd(int userId, int auctionId) {
        List<Invitation> invitation = jdbcTemplate.query(SELECT_NOTIFIED_ABOUT_AUCTION_END, new Object[]{userId,auctionId},anonymousInvitationMapper);
        if (invitation.get(0) == null){
            return true;
        }
        else
            return invitation.get(0).isNotifiedAboutAuctionEnd();
    }

    @Override
    public boolean getIfUserExistsInNotifiedAnonymousAuctionUserTable(int userId, int auctionId) {
        List<NotifiedUser> notifiedUser = jdbcTemplate.query(SELECT_ANONYMOUS_AUCTION_OWNER_USER, new Object[]{userId, auctionId}, notifiedUserMapper);
        boolean result = (notifiedUser.size() != 0);
        return result;
    }

    @Override
    public void insertToAnonymousAuctionOwnerTable(int userId, int auctionId) {
        Map<String, Object> parameters = new HashedMap();
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("notified_anonymous_auction_owners");
        parameters.put("user_id",userId);
        parameters.put("auction_id",auctionId);
        simpleJdbcInsert.execute(parameters);
    }

    @Override
    public Invitation getAnonymousInvitation(int anonymousUserId, int auctionId) {
        List<Invitation> anonymousInvitationList = jdbcTemplate.query(SELECT_ANONYYMOUS_INVITATION_BY_USER_ID_AND_AUCTION_ID,
                new Object[]{anonymousUserId,auctionId}, anonymousInvitationMapper);
        if (anonymousInvitationList.size() <= 0){
            return null;
        }
        return anonymousInvitationList.get(0);
    }

}
