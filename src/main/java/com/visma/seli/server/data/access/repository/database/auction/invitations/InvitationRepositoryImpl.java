package com.visma.seli.server.data.access.repository.database.auction.invitations;

import com.visma.seli.server.data.access.mappers.jdbc.anonymous.AnonymousInvitationMapper;
import com.visma.seli.server.data.access.mappers.jdbc.anonymous.AnonymousUserMapper;
import com.visma.seli.server.data.access.mappers.jdbc.auction.InvitationMapper;
import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.Invitation;
import com.visma.seli.service.auction.UrlIdCoder;
import com.visma.seli.service.auction.anonymous.AnonymousAuctionService;
import com.visma.seli.service.enums.InvitationStatusEnum;
import com.visma.seli.service.user.AnonymousUser;
import org.apache.commons.collections.map.HashedMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

@Transactional
@Component
public class InvitationRepositoryImpl implements InvitationRepository {
    private static final Logger logger = Logger.getLogger(Class.class.getName());

    private static final String UPDATE_INVITATION_STATUS = "UPDATE invitations SET status = ? WHERE auction_id = ? AND email = ?";
    private static final String FIND_AUCTION_INVITATIONS_QUERY = "SELECT * FROM invitations WHERE auction_id = ?";
    private static final String FIND_AUCTION_ANONYMOUS_INVITATIONS_QUERY = "SELECT * FROM invitations_anonymous WHERE auction_id = ?";
    private static final String FIND_ANONYMOUS_USER_BY_EMAIL = "SELECT * FROM anonymous_user WHERE user_email = ?";
    private static final String FIND_ANONYMOUS_USER_BY_EMAIL_IN_INVITATIONS_TABLE = "SELECT * FROM invitations_anonymous WHERE invited_email = ? and auction_id = ?";
    private static final String FIND_USER_BY_EMAIL_IN_INVITATION_TABLE = "SELECT * FROM invitations WHERE email = ? AND auction_id = ?";
    private static final String FIND_ALL_INVITATIONS = "SELECT * FROM invitations";
    private static final String DELETE_INVITATION_BY_ID = "DELETE FROM invitations WHERE id = ?";

    @Autowired
    private DataSource dataSource;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private InvitationMapper invitationMapper;
    @Autowired
    private AnonymousInvitationMapper anonymousInvitationMapper;
    @Autowired
    private AnonymousUserMapper anonymousUserMapper;
    @Autowired
    private AnonymousAuctionService anonymousAuctionService;
    @Autowired
    private UrlIdCoder urlIdCoder;

    @Override
    public void updateInvitation(int auctionId, String userEmail, InvitationStatusEnum invitStatus) {
        logger.log(Level.INFO,"Query for update invitation");
        jdbcTemplate.update(UPDATE_INVITATION_STATUS, invitStatus.toString(), auctionId, userEmail);
        logger.log(Level.INFO,"Query for update invitation updated");
    }

    @Override
    public List<Invitation> findInvitations(int auctionId) {
        List<Invitation> invitations = jdbcTemplate.query(FIND_AUCTION_INVITATIONS_QUERY,
                new Object[]{auctionId}, invitationMapper);
        return invitations;
    }

    @Override
    public List<Invitation> findAnonymousInvitations(int auctionId) {
        logger.log(Level.INFO,"Query for find anonymous invitations");
        List<Invitation> invitationList = jdbcTemplate.query(FIND_AUCTION_ANONYMOUS_INVITATIONS_QUERY,
                new Object[]{auctionId}, anonymousInvitationMapper);
        logger.log(Level.INFO,"Query for find anonymous invitations ended");
        return invitationList;
    }

    @Override
    public void insertInvitedAnonymousToAnonymousUserTable(List<Invitation> invitations){
        logger.log(Level.INFO,"Insert invited anonymous to anonymous user table");
        if (invitations != null) {
            for (Invitation invitation : invitations) {
                if (!checkIfAnonymousUserExists(invitation.getEmail())) {
                    SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("anonymous_user");
                    Map<String, Object> parameters = new HashMap<>();
                    parameters.put("id", invitation.getId());
                    parameters.put("user_email", invitation.getEmail());
                    parameters.put("phone", "");
                    simpleJdbcInsert.execute(parameters);
                    logger.log(Level.INFO,"Insert invited anonymous to anonymous user table. User " + invitation.getEmail());
                }
                else{
                    logger.log(Level.INFO,"Insert invited anonymous to anonymous user table. User "+ invitation.getEmail());
                }
            }
        }
        else {
            logger.log(Level.INFO,"Insert invited anonymous to anonymous user table ended. Invitations is null.");
        }
        logger.log(Level.INFO,"Insert invited anonymous to anonymous user table ended");
    }

    private boolean checkIfAnonymousUserExists(String email) {
        List<AnonymousUser> user = jdbcTemplate.query(FIND_ANONYMOUS_USER_BY_EMAIL, new Object[]{email}, anonymousUserMapper);
        return !user.isEmpty();
    }

    @Override
    public void insertPeopleToInvite(AuctionBody auctionBody) {

        if (auctionBody.getInvitations() != null)
            for (Invitation invitation : auctionBody.getInvitations()) {
                SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("invitations")
                        .usingGeneratedKeyColumns("id");
                Map<String, Object> parameters = new HashMap<>();
                parameters.put("auction_id", auctionBody.getId());
                parameters.put("email", invitation.getEmail());
                parameters.put("status", InvitationStatusEnum.NO_RESPONSE.toString());
                parameters.put("insert_date", Timestamp.valueOf(LocalDateTime.now()));
                simpleJdbcInsert.execute(parameters);
                logger.log(Level.INFO,"Insert people to invite. Invited " + invitation.getEmail());
            }
        logger.log(Level.INFO,"Insert people to invite ended");
    }

    @Override
    public void insertPeopleToInvite(int auctionId, List<Invitation> invitations) {
        logger.log(Level.INFO,"Insert people to invite");
        if (invitations != null)
            for (Invitation invitation : invitations) {
                SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("invitations")
                        .usingGeneratedKeyColumns("id");

                Map<String, Object> parameters = new HashMap<>();
                parameters.put("auction_id", auctionId);
                parameters.put("email", invitation.getEmail());
                parameters.put("status", InvitationStatusEnum.NO_RESPONSE.toString());
                parameters.put("insert_date", Timestamp.valueOf(LocalDateTime.now()));
                simpleJdbcInsert.execute(parameters);
                logger.log(Level.INFO,"Insert people to invite. Invited " + invitation.getEmail());
            }
        logger.log(Level.INFO,"Insert people to invite ended");
    }
    
    @Override
    public boolean invitationExists(int auctionId, String userEmail) {
        logger.log(Level.INFO,"Query for invitation Exists");
        List<Invitation> invitationList = jdbcTemplate.query(FIND_AUCTION_INVITATIONS_QUERY, new Object[]{auctionId}, invitationMapper);
        for (Invitation invitation : invitationList){
            if (invitation.getEmail().equals(userEmail)){
                logger.log(Level.INFO,"Query for invitation Exists returns true ended");
                return true;
            }
        }
        logger.log(Level.INFO,"Query for invitation Exists returns false ended");
        return false;
    }

    @Override
    public void insertInvitedAnonymous(List<Invitation> invitations, int auctionId) {
        logger.log(Level.INFO,"Insert invited anonymous");
        boolean isAnonymous = true;
        if (invitations != null) {
            for (Invitation invitation : invitations) {
                if (!checkIfUserExistsInInvitation(invitation.getEmail(), auctionId,isAnonymous)) {
                    int invitedUserIdIsMoreThenZeroIfUserExistsInAllUserTable = anonymousAuctionService.checkIfAnonymousUserExists(invitation);

                    SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("invitations_anonymous")
                            .usingGeneratedKeyColumns("id");
                    Map<String, Object> parameters = new HashMap<>();
                    parameters.put("auction_id", auctionId);
                    parameters.put("invited_email", invitation.getEmail());
                    if (invitedUserIdIsMoreThenZeroIfUserExistsInAllUserTable < 0) {
                        Integer allUsersId = insertNewAllUsers();
                        invitation.setId(allUsersId);
                        parameters.put("user_id", allUsersId);
                    } else
                        parameters.put("user_id", invitedUserIdIsMoreThenZeroIfUserExistsInAllUserTable);
                    UUID generatedID = urlIdCoder.createUUID();
                    invitation.setInvitedUuid(generatedID);
                    parameters.put("uuid", generatedID);
                    parameters.put("notified_about_auction_end", false);
                    simpleJdbcInsert.execute(parameters);
                    logger.log(Level.INFO,"Insert invited anonymous inserted " + invitation.getEmail());
                }
            }
        }
        logger.log(Level.INFO,"Insert invited anonymous ended");
    }


    @Override
    public boolean checkIfUserExistsInInvitation(String email, int auctionId, boolean isAnonymous) {
        logger.log(Level.INFO,"Query for checking if users exists in invitation");
        if  (isAnonymous){
            List<Invitation> invitation = jdbcTemplate.query(FIND_ANONYMOUS_USER_BY_EMAIL_IN_INVITATIONS_TABLE, new Object[]{email,auctionId}, anonymousInvitationMapper);
            logger.log(Level.INFO,"Query for checking if users exists in invitation ended");
            return !invitation.isEmpty();
        } else{
            List<Invitation> invitation = jdbcTemplate.query(FIND_USER_BY_EMAIL_IN_INVITATION_TABLE, new Object[]{email,auctionId}, invitationMapper);
            logger.log(Level.INFO,"Query for checking if users exists in invitation ended");
            return !invitation.isEmpty();
        }
    }

    @Override
    public List<Invitation> getAllInvitations() {
        logger.log(Level.INFO,"Query for all invitations");
        List<Invitation> allInvitations = jdbcTemplate.query(FIND_ALL_INVITATIONS, invitationMapper);
        logger.log(Level.INFO,"Query for all invitations ended");
        return allInvitations;
    }

    @Override
    public void deleteInvitation(int id) {
        logger.log(Level.INFO,"Query for delete invitation. Deleting user id " + id);
        jdbcTemplate.update(DELETE_INVITATION_BY_ID, id);
        logger.log(Level.INFO,"Query for delete invitation ended");
    }

    private Integer insertNewAllUsers() {
        SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource).withTableName("all_users")
                .usingGeneratedKeyColumns("id");
        Map<String, Object> parameters = new HashedMap();
        parameters.put("type", "anonymous");
        return (Integer) simpleJdbcInsert.executeAndReturnKey(parameters);
    }

}
