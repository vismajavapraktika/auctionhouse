package com.visma.seli.server.data.access.repository.database.auction.invitations;

import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.Invitation;
import com.visma.seli.service.enums.InvitationStatusEnum;

import java.util.List;

public interface InvitationRepository {

    void updateInvitation(int auctionId, String userEmail, InvitationStatusEnum invitStatus);
    List<Invitation> findInvitations(int auctionId);
    List<Invitation> findAnonymousInvitations(int auctionId);
    void insertPeopleToInvite(AuctionBody auctionBody);
    boolean invitationExists(int auctionId, String userEmail);
	void insertPeopleToInvite(int auctionId, List<Invitation> invitations);
	void insertInvitedAnonymous(List<Invitation> invitations, int auctionId);
	void insertInvitedAnonymousToAnonymousUserTable(List<Invitation> invitations);
    boolean checkIfUserExistsInInvitation(String email, int id, boolean isAnonymous);

    List<Invitation> getAllInvitations();

    void deleteInvitation(int id);
}
