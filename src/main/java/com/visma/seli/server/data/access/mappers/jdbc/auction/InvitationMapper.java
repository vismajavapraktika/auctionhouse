package com.visma.seli.server.data.access.mappers.jdbc.auction;

import com.visma.seli.service.auction.Invitation;
import com.visma.seli.service.enums.InvitationStatusEnum;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class InvitationMapper implements RowMapper<Invitation> {

    @Override
    public Invitation mapRow(ResultSet rs, int arg1) throws SQLException {
        Invitation invitation = new Invitation();
        invitation.setId(rs.getInt("id"));
        invitation.setAuctionId(rs.getInt("auction_id"));
        invitation.setEmail(rs.getString("email"));
        invitation.setEmailSent(rs.getBoolean("email_sent"));
        invitation.setStatus(InvitationStatusEnum.valueOf(rs.getString("status")));
        invitation.setInsertDate(rs.getTimestamp("insert_date").toLocalDateTime());
        return invitation;
    }

}