package com.visma.seli.server.login.google;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class GoogleAuth {

    public GoogleLoginInfo getGoogleUser() {

        GoogleLoginInfo googleUserInfo = null;
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            ObjectMapper mapper = new ObjectMapper();
            Map<String, String> details = (Map<String, String>) ((OAuth2Authentication) auth).getUserAuthentication()
                    .getDetails();
            String json;
            json = mapper.writeValueAsString(details);
            googleUserInfo = mapper.readValue(json, GoogleLoginInfo.class);

        } catch (Exception e) {
            return null;
        }

        return googleUserInfo;
    }
}
