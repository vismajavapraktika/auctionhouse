package com.visma.seli.config.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.time.Duration;

@ConfigurationProperties(prefix = "duration.hours")
@Component
public class DurationProperties {
    private Duration answerWaitDuration;

    public Duration getAnswerWaitDuration() {
        return answerWaitDuration;
    }

    public void setAnswerWaitDuration(int answerWaitDuration) {
        this.answerWaitDuration = Duration.ofHours(answerWaitDuration);
    }
}




