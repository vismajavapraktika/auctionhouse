package com.visma.seli.config;

import com.visma.seli.config.auth.oauth2.OAuth2Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private OAuth2Config oAuth2Config;

    @Autowired
    private DataSource dataSource;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.antMatcher("/**").addFilterBefore(oAuth2Config.ssoFilter(), BasicAuthenticationFilter.class)
                .authorizeRequests().antMatchers("/", "/registration", "/login/**", "/error", "/report-bug",
                "/auction/picture/**", "/dist/**" ,"/css/**", "/js/**",  "/fonts/**",
                "/create-auction/picture-check", "/auction/refresh-bid-information/**").permitAll()
                .anyRequest().authenticated().and()
                .csrf().csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse()).ignoringAntMatchers("/create-auction/picture-check")
                .and().formLogin().loginPage("/")
                .defaultSuccessUrl("/").usernameParameter("userEmail").passwordParameter("userPassword")
                .failureUrl("/?login-error").and().logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .logoutSuccessUrl("/").deleteCookies("JSESSIONID").invalidateHttpSession(true);
// "/createAnonymousAuction", "/newAnonymousAuction/", "/anonymousAuctionUser", "/anonymousOwnerAuction/**", "/auction/deleteAnonymous/**", "/anonymousAuction/**",
//        "/anonymousAuctionOwner/anonymousInvite/**", "/go-back/anonymous/**","/auction/anonymous/picture/{uuid}/{pictureId}", "/go-back/anonymousAuction/anonymousInvited/**" ,
//        "/create-anonymous-auction/**",                 "/auction/placeBidAnonymously/**", "/anonymousAuction/anonymousInvite/**",
    }

    @Autowired
    public void configAuthentication(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication().dataSource(dataSource)
                .usersByUsernameQuery("select email, password, enabled from test_user where email=?")
                .authoritiesByUsernameQuery("select email, user_role from test_user where email=?")
                .passwordEncoder(passwordEncoder);
    }
}
