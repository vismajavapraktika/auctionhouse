package com.visma.seli.config.auth;

import com.visma.seli.config.properties.ApplicationProperties;
import com.visma.seli.server.data.access.repository.database.user.logedInUser.UserRepository;
import com.visma.seli.server.login.google.GoogleAuth;
import com.visma.seli.server.login.google.GoogleLoginInfo;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.event.InteractiveAuthenticationSuccessEvent;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;

@Configuration
public class AuthenticationListenerConfig implements ApplicationListener<InteractiveAuthenticationSuccessEvent> {

    private static final java.util.logging.Logger logger = java.util.logging.Logger.getLogger(Class.class.getName());

    @Autowired(required = true)
    private HttpServletRequest request;
    @Autowired(required = true)
    private HttpServletResponse response;
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
    @Autowired
    private ApplicationProperties applicationProperties;
    @Autowired
    private GoogleAuth googleAuth;
    @Autowired
    private UserRepository userRepository;

    @Override
    public void onApplicationEvent(final InteractiveAuthenticationSuccessEvent event) {
        if (event.getGeneratedBy().getName().toLowerCase().contains("oauth2")) {
            try {
                GoogleLoginInfo googleUser = googleAuth.getGoogleUser();
                if (googleUser != null) {
                    redirectToRegistrationIfNotRegistered(googleUser);
                }
            } catch (IOException e) {
                logger.log(Level.SEVERE, e.toString(),e);
            }
        }
    }

    private void redirectToRegistrationIfNotRegistered(GoogleLoginInfo googleUser) throws IOException {
        String email = googleUser.getEmails().iterator().next().getValue();
        if (userRepository.getUserByEmail(email) == null) {
            redirectStrategy.sendRedirect(request, response, applicationProperties.getUrl() + "googleRegistration");
        } else {
            redirectStrategy.sendRedirect(request, response, applicationProperties.getUrl());
        }
    }
}
