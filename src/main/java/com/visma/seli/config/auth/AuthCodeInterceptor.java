package com.visma.seli.config.auth;

import com.visma.seli.config.properties.ApplicationProperties;
import com.visma.seli.server.data.access.repository.database.user.logedInUser.UserRepository;
import com.visma.seli.server.login.google.GoogleAuth;
import com.visma.seli.server.login.google.GoogleLoginInfo;
import com.visma.seli.service.user.UserBody;
import com.visma.seli.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class AuthCodeInterceptor extends HandlerInterceptorAdapter {

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Autowired
    private ApplicationProperties applicationProperties;

    @Autowired
    private GoogleAuth googleAuth;

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        request.setAttribute("loggedInUserEmail", userService.getLoggedInUserEmail());
        String userLoggedInCredentials = userService.getLoggedInUserNameAndSurname();
        String tmp [] = userLoggedInCredentials.split("@");
        if (tmp.length > 1){
            UserBody userBody = userRepository.getUserById(userService.getLoggedInUserId());
            request.setAttribute("loggedInUserNameAndSurname", userBody.getName()+" "+userBody.getSurname());
        } else {
            request.setAttribute("loggedInUserNameAndSurname", userService.getLoggedInUserNameAndSurname());
        }

        GoogleLoginInfo googleUser = googleAuth.getGoogleUser();
        if (googleUser != null) {
            if (!request.getRequestURI().equals("/googleRegistration"))
                if (!userService.isGoogleUserRegistered(googleUser)) {
                    redirectStrategy.sendRedirect(request, response,
                            applicationProperties.getUrl() + "googleRegistration");
                    return false;
                }
        }
        return super.preHandle(request, response, handler);
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {

        super.postHandle(request, response, handler, modelAndView);
    }
}