package com.visma.seli.service;

import com.visma.seli.server.data.access.repository.database.auction.AuctionRepository;
import com.visma.seli.server.data.access.repository.database.auction.bids.BidRepository;
import com.visma.seli.server.data.access.repository.database.auction.won.WonAuctionsRepository;
import com.visma.seli.server.data.access.repository.database.user.logedInUser.UserRepository;
import com.visma.seli.service.auction.*;
import com.visma.seli.service.auction.bid.Bid;
import com.visma.seli.service.enums.AuctionStatusEnum;
import com.visma.seli.service.enums.AuctionTypeEnum;
import com.visma.seli.service.enums.InvitationStatusEnum;
import com.visma.seli.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.visma.seli.service.enums.AuctionTypeEnum.*;
import static java.util.Arrays.asList;

@Component
public class AuctionSearchService {

    @Autowired
    private AuctionRepository auctionRepository;
    @Autowired
    private AuctionService auctionService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private BidRepository bidRepository;
    @Autowired
    private WonAuctionsRepository wonAuctionsRepository;

    @Transactional(propagation = Propagation.REQUIRED)
    public List<AuctionBody> findByType(String type) {
        switch(AuctionTypeEnum.valueOf(type.toUpperCase()))
        {
            case PRIVATE:
                return findNotDeclinedAuctions(asList(AuctionStatusEnum.ACTIVE));
            case ALL:
                List<AuctionBody> auctions = findNotDeclinedAuctions(asList(AuctionStatusEnum.ACTIVE));
                auctions.addAll(auctionRepository.findAllAuctionsWithTypesAndStatuses(asList(PUBLIC),
                        asList(AuctionStatusEnum.ACTIVE)));
                return auctions;
            case MY:
                return auctionRepository.findMyAuctions(userRepository.getUserByEmail(userService.getLoggedInUserEmail()).getId());
            case MYBIDS:
                return bidRepository.findMyBiddedAuctions(userRepository.getUserByEmail(userService.getLoggedInUserEmail()).getId());
            case WON:
                return findUserWonAuctions(userRepository.getUserByEmail(userService.getLoggedInUserEmail()).getId());
            case PUBLIC:
                return auctionRepository.findAllAuctionsWithTypesAndStatuses(asList(PUBLIC),
                        asList(AuctionStatusEnum.ACTIVE));
        }

        return new ArrayList<>();
    }

    public List<AuctionBody> findNotDeclinedAuctions(List<AuctionStatusEnum> auctionStatusEnums) {
        List<AuctionBody> ret;
        ret = auctionRepository.findUserPrivateAuctions(userService.getLoggedInUserEmail(), InvitationStatusEnum.NO_RESPONSE, auctionStatusEnums);
        ret.addAll(auctionRepository.findUserPrivateAuctions(userService.getLoggedInUserEmail(),InvitationStatusEnum.ACCEPTED, auctionStatusEnums));
        return ret;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<AuctionBody> findUserWonAuctions(int userId) {
        List<AuctionBody> wonAuctions = new ArrayList<AuctionBody>();
        for (Bid wonBid : wonAuctionsRepository.findByUser(userId)) {
            wonAuctions.add(auctionRepository.findAuction(wonBid.getAuctionId()));
        }
        return wonAuctions;
    }
}
