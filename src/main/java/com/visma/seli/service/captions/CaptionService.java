package com.visma.seli.service.captions;

public interface CaptionService {
    String getCaption(final String key);
    String getCaptionWithVariables(final String key, Object[] variables);
    String getCaption(final String key, String ... args);
}
