package com.visma.seli.service;

import com.visma.seli.service.enums.NotificationEnum;

import java.time.LocalDateTime;

public class Notifications {
    private int id, notificationId;
    private String type, email, title;
    private boolean newNotification;
    private LocalDateTime insertDate;

    public Notifications(int id, int notificationId, String type, String email, String title, boolean newNotification, LocalDateTime insertDate) {
        this.id = id;
        this.notificationId = notificationId;
        this.type = type;
        this.email = email;
        this.title = title;
        this.newNotification = newNotification;
        this.insertDate = insertDate;
    }

    public Notifications() {

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {

        this.notificationId = notificationId;
    }

    public boolean isNewNotification() {
        return newNotification;
    }

    public void setNewNotification(boolean newNotification) {

        this.newNotification = newNotification;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {

        this.id = id;
    }

    public LocalDateTime getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(LocalDateTime insertDate) {
        this.insertDate = insertDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void getNotificationTitle(String auctionTitle){
        switch (NotificationEnum.valueOf(this.getType())) {
            case INVITATION:
                this.setTitle("You have been invited to auction named \"" + auctionTitle + "\".");
                break;
            case AUCTION_WINNER:
                this.setTitle("You are a winner of auction \"" + auctionTitle + "\".");
                break;
            case DELETED:
                this.setTitle("Auction \"" + auctionTitle + "\" has been deleted");
                break;
            case UPDATED:
                this.setTitle("Auction \"" + auctionTitle + "\" has been updated");
                break;
            case OVERBID:
                this.setTitle("You have been overbidded in auction \"" + auctionTitle + "\".");
                break;
            default:
                this.setTitle("");
                break;
        }
    }

}
