package com.visma.seli.service.enums;

public enum AuctionTypeEnum {
    PUBLIC, PRIVATE, ALL, MY, WON, MYBIDS, ANONYMOUS;
}
