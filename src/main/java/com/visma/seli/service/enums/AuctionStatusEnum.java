package com.visma.seli.service.enums;

public enum AuctionStatusEnum {
    ACTIVE,
    ENDED,
    DELETED
}
