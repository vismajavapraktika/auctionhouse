package com.visma.seli.service.enums;

public enum InvitationStatusEnum {
    ACCEPTED, DECLINED, NO_RESPONSE;
}
