package com.visma.seli.service.enums;

public enum UserTypes {
    GOOGLE_USER, FULL_ACCESS_USER, ANONYMOUS_USER
}
