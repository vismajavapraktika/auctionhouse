package com.visma.seli.service.enums;

public enum SortingEnum {
    BY_END_TIME,
    BY_START_TIME,
    BY_PRICE,
    BY_TITLE
}
