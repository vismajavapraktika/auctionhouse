package com.visma.seli.service.enums;

public enum NotificationEnum {
    INVITATION,
    DELETED,
    UPDATED,
    AUCTION_WINNER,
    OVERBID
}
