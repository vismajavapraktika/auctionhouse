package com.visma.seli.service.enums;

import java.math.BigDecimal;

public enum AuctionCurrencyEnum {
    EUR(new BigDecimal("0.01")),
    KUDOS(BigDecimal.ONE);

    private BigDecimal step;

    AuctionCurrencyEnum(BigDecimal step) {
        this.step = step;
    }

    public BigDecimal getStep() {
        return step;
    }
}
