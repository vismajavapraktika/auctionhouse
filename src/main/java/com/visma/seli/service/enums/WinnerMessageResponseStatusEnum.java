package com.visma.seli.service.enums;


public enum WinnerMessageResponseStatusEnum {
    NOT_SENT,
    WAITING_ANSWER,
    ACCEPTED,
    DECLINED,
    EXPIRED,
    STOPPED,
    OUT_OF_STOCK,
    ANONYMOUS
}



