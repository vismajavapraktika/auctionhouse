package com.visma.seli.service.mailing;

import com.visma.seli.server.data.access.repository.database.NotificationsRepository;
import com.visma.seli.service.Notifications;
import com.visma.seli.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Service;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

@Service
@EnableAsync
public class NotificationService {

    private static final Logger logger = Logger.getLogger(Class.class.getName());
    @Autowired
    private JavaMailSender javaMailSender;
    @Autowired
    private MailProperties mailProperties;
    @Autowired
    private NotificationsRepository notificationsRepository;
    @Autowired
    private UserService userService;
    
    @Async
    public void sendNotification(UUID number, String subject, String text, String email) throws MailException {
        try {
            logger.log(Level.INFO, "Sending messages");
            Properties properties = new Properties();
            properties.put("mail.smtp.starttls.enable", "true");
            properties.put("mail.smtp.host", mailProperties.getHost());
            properties.put("mail.smtp.user", mailProperties.getUsername());
            properties.put("mail.smtp.password", mailProperties.getPassword());
            properties.put("mail.smtp.port", mailProperties.getPort());
            properties.put("mail.smtp.socketFactory.port", mailProperties.getPort());
            properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            properties.put("mail.smtp.socketFactory.fallback", "false");
            properties.put("mail.smtp.auth", "true");
            properties.put("mail.debug", "true");

            Session session = Session.getInstance(properties,
                    new GMailAuthenticator(mailProperties.getUsername(), mailProperties.getPassword()));
            session.setDebug(true);
            MimeMessage message = new MimeMessage(session);
            Address fromAddress = new InternetAddress(mailProperties.getUsername());
            Address toAddress = new InternetAddress(email);
            message.setFrom(fromAddress);
            message.setRecipients(Message.RecipientType.TO, String.valueOf(toAddress));
            message.setSubject(subject);
            if (number != null) {
                message.setText(text + number);
            } else
                message.setText(text);
            javaMailSender.send(message);
            logger.log(Level.INFO,"Message was sent to " + email);
        } catch (Exception e) {
            logger.log(Level.SEVERE, e.toString(), e);
        }
    }

    @Async
    public void sendNotification(int number, String subject, String text, String email) throws MailException {
        try {
            logger.log(Level.INFO, "Sending messages");
            Properties properties = new Properties();
            properties.put("mail.smtp.starttls.enable", "true");
            properties.put("mail.smtp.host", mailProperties.getHost());
            properties.put("mail.smtp.user", mailProperties.getUsername());
            properties.put("mail.smtp.password", mailProperties.getPassword());
            properties.put("mail.smtp.port", mailProperties.getPort());
            properties.put("mail.smtp.socketFactory.port", mailProperties.getPort());
            properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
            properties.put("mail.smtp.socketFactory.fallback", "false");
            properties.put("mail.smtp.auth", "true");
            properties.put("mail.debug", "true");

            Session session = Session.getInstance(properties,
                    new GMailAuthenticator(mailProperties.getUsername(), mailProperties.getPassword()));
            session.setDebug(true);
            MimeMessage message = new MimeMessage(session);
            Address fromAddress = new InternetAddress(mailProperties.getUsername());
            Address toAddress = new InternetAddress(email);
            message.setFrom(fromAddress);
            message.setRecipients(Message.RecipientType.TO, String.valueOf(toAddress));
            message.setSubject(subject);
            if (number >= 0) {
                message.setText(text + number +"/");
            } else
                message.setText(text);
            javaMailSender.send(message);
            logger.log(Level.INFO,"Message was sent to " + email);
        } catch (Exception e) {
            System.out.println("some kind of error");
            logger.log(Level.SEVERE, e.toString(), e);
        }
    }
    
    private boolean isLoggedInUserOwnsNotification(int notificationId) {
    	Notifications notification = notificationsRepository.findNotification(notificationId);
        return userService.getLoggedInUserEmail().equals(notification.getEmail());
    }

	public void changeNotificationStatus(int notificationId) {
		if(isLoggedInUserOwnsNotification(notificationId))
			notificationsRepository.changeNotificationsStatus(notificationId, false);
	}

}