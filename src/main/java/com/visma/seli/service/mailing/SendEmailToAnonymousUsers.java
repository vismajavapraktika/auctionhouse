package com.visma.seli.service.mailing;

import com.visma.seli.server.data.access.repository.database.auction.AuctionRepository;
import com.visma.seli.server.data.access.repository.database.auction.bids.BidRepository;
import com.visma.seli.server.data.access.repository.database.user.anonymousUser.AnonymousUserRepository;
import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.bid.Bid;
import com.visma.seli.service.captions.DefaultCaptionService;
import com.visma.seli.service.enums.AuctionStatusEnum;
import com.visma.seli.service.user.AnonymousUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SendEmailToAnonymousUsers {

    @Autowired
    NotificationService notificationSend;
    @Autowired
    AnonymousUserRepository anonymousUserRepository;
    @Autowired
    AuctionRepository auctionRepository;
    @Autowired
    private BidRepository bidRepository;
    @Autowired
    private DefaultCaptionService defaultCaptionService;

    public void sendEmailToAnonymousUsers(AuctionBody auction) {
    	
        List<Bid> bids = bidRepository.findAllEndedAuctionBids(auction.getId());
        if (isAnonymousBid(bids)) {
            List<String> ownerInformation = new ArrayList<>();
            for (Bid bid : bids) {
                AnonymousUser anonymousUser = anonymousUserRepository.getAnonymousUserById(bid.getUserId());
                if (anonymousUser != null) {
                    if (!anonymousUserRepository.getIfNotifiedAboutAuctionEnd(anonymousUser.getId(),auction.getId())) {
                        notificationSend.sendNotification(null, defaultCaptionService.getCaptionWithVariables("message.title.for.anonymous.user.after.auction.ended", new Object[]{auction.getTitle()}),
                                defaultCaptionService.getCaptionWithVariables("message.for.anonymous.user.after.auction.ended", new Object[]{bid.getAmount().toString(), bid.getBiddingQuantity(), bids.indexOf(bid) + 1}), anonymousUser.getEmail());
                        ownerInformation.add("Bid: " + bid.getAmount() + " quantity: " + bid.getBiddingQuantity() + " Email: " + anonymousUser.getEmail() + " phone: " + anonymousUser.getPhone() + "\r\n");
                        anonymousUserRepository.notificationHasBeenSendToUser(anonymousUser.getId(), auction.getId());
                    }
                }
            }
            String textForAuctionOwner = createTextMessage(ownerInformation);
            AnonymousUser anonymousUser = anonymousUserRepository.getAnonymousUserById(auction.getOwnerId());
            if (anonymousUser != null) {
                if (!anonymousUserRepository.getIfUserExistsInNotifiedAnonymousAuctionUserTable(anonymousUser.getId(),auction.getId())) {
                    notificationSend.sendNotification(null, "auction " + auction.getTitle() + " has ended", textForAuctionOwner, anonymousUser.getEmail());
                    anonymousUserRepository.insertToAnonymousAuctionOwnerTable(anonymousUser.getId(), auction.getId());
                }
            }
        }
    }

    public String createTextMessage(List<String> ownerInformationList) {
        String information = "";
        for (String ownerInformation : ownerInformationList) {
            information += ownerInformation;
        }
        return information;
    }

    public boolean isAnonymousBid(List<Bid> bids) {
        List<Integer> listOfUserIds = new ArrayList<>();
        List<AnonymousUser> anonymouseUsers = new ArrayList<>();
        for (Bid bid : bids) {
            listOfUserIds.add(bid.getUserId());
        }
        for (Integer userId : listOfUserIds) {
            anonymouseUsers.add(anonymousUserRepository.getAnonymousUserById(userId));
        }
        if (anonymouseUsers.isEmpty()) {
            return false;
        } else {
            return true;
        }

    }
}
