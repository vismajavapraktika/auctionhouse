package com.visma.seli.service.exceptions;

public class SeliException extends Exception {
    public SeliException(String message) {
        super(message);
    }
}
