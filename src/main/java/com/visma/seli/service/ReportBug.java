package com.visma.seli.service;

import javax.validation.constraints.Size;

public class ReportBug {
    @Size(min = 1, max = 100, message = "Title must be between 1 and 100 letters")
    private String subject;
    @Size(min = 1, max = 999, message = "Description must be between 1 and  999 letters")
    private String  text;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
