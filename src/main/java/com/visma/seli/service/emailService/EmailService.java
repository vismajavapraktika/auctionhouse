package com.visma.seli.service.emailService;

import org.springframework.stereotype.Component;
import org.xbill.DNS.*;

@Component
public class EmailService {

    public boolean checkIfEmailIsGoogleMX(String email) throws TextParseException {
        String[] emailDomain = email.split("@");
        if (!email.contains("@"))
            return false;
        Record[] records = new Lookup(emailDomain[1], Type.MX).run();
        if(records == null)
        	return false;
        for (Record record : records){
            MXRecord mxRecord = (MXRecord) record;
            String temp = mxRecord.getTarget().toString().toLowerCase();
            if (mxRecord.getTarget().toString().toLowerCase().endsWith("google.com.")
                    || mxRecord.getTarget().toString().toLowerCase().endsWith("googlemail.com.")){
                return true;
            }
        }
        return emailDomain[1].equals("visma.com");
    }
}
