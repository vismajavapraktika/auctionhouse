package com.visma.seli.service.auction;

import com.visma.seli.config.properties.ApplicationProperties;
import com.visma.seli.server.data.access.repository.database.NotificationsRepository;
import com.visma.seli.server.data.access.repository.database.auction.AuctionRepository;
import com.visma.seli.server.data.access.repository.database.auction.bids.BidRepository;
import com.visma.seli.server.data.access.repository.database.auction.invitations.InvitationRepository;
import com.visma.seli.server.data.access.repository.database.auction.won.WonAuctionsRepository;
import com.visma.seli.server.data.access.repository.database.pictures.PictureRepository;
import com.visma.seli.server.data.access.repository.database.user.logedInUser.UserRepository;
import com.visma.seli.server.data.access.repository.database.user.rating.UserRatingRepository;
import com.visma.seli.service.auction.bid.Bid;
import com.visma.seli.service.captions.CaptionService;
import com.visma.seli.service.captions.DefaultCaptionService;
import com.visma.seli.service.enums.AuctionCurrencyEnum;
import com.visma.seli.service.enums.AuctionStatusEnum;
import com.visma.seli.service.enums.AuctionTypeEnum;
import com.visma.seli.service.exceptions.SeliException;
import com.visma.seli.service.mailing.NotificationService;
import com.visma.seli.service.user.UserBody;
import com.visma.seli.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.visma.seli.service.enums.AuctionStatusEnum.ENDED;
import static com.visma.seli.service.enums.NotificationEnum.*;

@Component
public class AuctionService {

    public static int AUCTION_IS_NOT_CREATED_ID = 0;
    public static final int NO_NUMBER = -1;
    @Autowired
    private WonAuctionsRepository wonAuctionsRepository;
    @Autowired
    private AuctionRepository auctionRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private BidRepository bidRepository;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private CaptionService captionService;
    @Autowired
    private NotificationsRepository notificationsRepository;
    @Autowired
    private MessagingService messagingService;
    @Autowired
    private UrlIdCoder urlIdCoder;
    @Autowired
    private InvitationRepository invitationRepository;
    @Autowired
    private PictureRepository pictureRepository;
    @Autowired
    private UserRatingRepository userRatingRepository;
    @Autowired
    private ApplicationProperties applicationProperties;
    @Autowired
    private DefaultCaptionService defaultCaptionService;

    private Map<String, List<MultipartFile>> userEmailToPictureFiles = new HashMap<>();


    /*
    * Auction stuff
    * Create auction stuff
    */

    @Transactional(propagation = Propagation.REQUIRED)
    public void saveCreatedAuction(AuctionBody auctionBody, Model model, final RedirectAttributes redirectAttributes,
                                   String AUCTION_TYPE) throws Exception {
        if (!auctionBody.getType().equals(AuctionTypeEnum.PUBLIC) || !auctionBody.getCurrency().equals(AuctionCurrencyEnum.KUDOS)) {
            throw new SeliException(defaultCaptionService.getCaption("create.auction.exception.public.and.kudos.only"));
        }
        int auctionOwnerId = userRepository.getUserByEmail(userService.getLoggedInUserEmail()).getId();
        auctionBody = setAuctionValues(auctionBody);
        model.addAttribute("auctionBody", auctionBody);
        auctionRepository.insertNewAuction(auctionBody, auctionOwnerId, AUCTION_TYPE);
        if (auctionBody.getListOfPhotosUUUID() != null)
            pictureRepository.deleteAuctionTemporaryPictures(auctionBody);
        if (auctionBody.getInvitations() != null)
            for (Invitation invitationListElement : auctionBody.getInvitations()) {
                notificationsRepository.insertToNotificationsTable(INVITATION, invitationListElement.getEmail(), auctionBody.getId());
            }
        sendCreateNotificationsToAuctionUsers(auctionBody, userService.getLoggedInUserEmail());
        userEmailToPictureFiles.remove(userService.getLoggedInUserEmail());
        redirectAttributes.addFlashAttribute("removePicturesFromLocalStorage", true);
    }

    private AuctionBody setAuctionValues(AuctionBody auctionBody) {
        if (auctionBody.getLink().length() > 0 && !auctionBody.getLink().startsWith("http")) {
            auctionBody.setLink("http://" + auctionBody.getLink());
        }
        auctionBody.setStatus(AuctionStatusEnum.ACTIVE);
        auctionBody.setAuctionIdUrlOwner(urlIdCoder.createUUID());
        if (auctionBody.getBuyOutPrice() == null) {
            auctionBody.setBuyOutPrice(BigDecimal.ZERO);
        }
        if (auctionBody.getLink().equals("")) {
            auctionBody.setLink(null);
        }
        return auctionBody;
    }

    /*
    * Update auction stuff
    */

    public void saveUpdatedAuction(AuctionBody auctionBody, int id) {
        auctionBody.setId(id);
        invitationRepository.findInvitations(id);
        setAuctionValues(auctionBody);
        auctionRepository.updateAuction(auctionBody);
        sendNotificationsToAuctionUsers(auctionBody, NO_NUMBER);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public String deleteAuction(int id) {
        if (isLoggedInUserAuctionOwner(id))
            return "redirect:/home";
        AuctionBody auctionBody = auctionRepository.findAuction(id);
        if (isAuctionEnded(auctionBody))
            return "redirect:/auction/" + id + "/";
        if (isLoggedInUserAuctionOwner(id) && !isAuctionEnded(auctionBody)) {
            auctionRepository.changeAuctionStatus(AuctionStatusEnum.DELETED, id);
            userRatingRepository.deleteRatingByAuctionId(id);
            if (auctionBody.getInvitations() != null) {
                sendNotificationsAboutDeletedAuction(auctionBody, NO_NUMBER);
            }
        }
        return "redirect:/home";
    }

    /*
    * Auction Bid stuff
    */

    public void insertNewBid(int id, Bid auctionBid) {
        auctionBid.setAuctionId(id);
        auctionBid.setUserId(userRepository.getUserByEmail(userService.getLoggedInUserEmail()).getId());
        List<Bid> listOfBids = bidRepository.findAllAuctionBids(id);
        bidRepository.insertNewBid(auctionBid);
        for (Bid bid : listOfBids) {
            if (bid.getUserId() != auctionBid.getUserId()) {
                if (notificationsRepository.checkIfUserHasNotificationAboutOverbid(bid.getAuctionId(), userRepository.getUserById(bid.getUserId()).getEmail()))
                    notificationsRepository.insertToNotificationsTable(OVERBID, userRepository.getUserById(bid.getUserId()).getEmail(), bid.getAuctionId());
            }
        }
    }

    List<Bid> getAllAuctionBids(int auctionId) {
        return bidRepository.findAllAuctionBids(auctionId);
    }

    public int getAmountOfBids(int auctionId) {
        return getAllAuctionBids(auctionId).size();
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void insertAuctionWinners(List<Bid> auctionHighestBidsForDiffUsers) {
        for (Bid bid : auctionHighestBidsForDiffUsers) {
            wonAuctionsRepository.insertNewBidWinner(bid);
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<AuctionBody> findUserWonAuctions(int userId) {
        List<AuctionBody> wonAuctions = new ArrayList<>();
        for (Bid wonBid : wonAuctionsRepository.findByUser(userId)) {
            wonAuctions.add(auctionRepository.findAuction(wonBid.getAuctionId()));
        }
        return wonAuctions;
    }

    /*
    * Auction Picture stuff
    */

    public void addNewPictures(MultipartFile[] multipartFiles, String loggedInUserEmail, Model model) {
        List<MultipartFile> newPictures = new ArrayList<>(Arrays.stream(multipartFiles).filter(Objects::nonNull).collect(Collectors.toList()));
        List<MultipartFile> loggedInUserAlreadyUploadedPictures = userEmailToPictureFiles.get(loggedInUserEmail);
        if (loggedInUserAlreadyUploadedPictures == null) {
            userEmailToPictureFiles.put(loggedInUserEmail, newPictures);
        } else {
            loggedInUserAlreadyUploadedPictures.addAll(newPictures);
        }
        model.addAttribute("allPictureFiles", userEmailToPictureFiles.get(userService.getLoggedInUserEmail()));
    }

    private MultipartFile resizePictureByBytes(byte[] pictureBytes, String pictureOriginalName) {
        try {
            InputStream in = new ByteArrayInputStream(pictureBytes);
            BufferedImage image = ImageIO.read(in);
            BufferedImage newSmallerImg = scaleImage(image);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(newSmallerImg, "jpg", baos);
            baos.flush();
            return new MockMultipartFile(pictureOriginalName, baos.toByteArray());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    byte[] getAuctionPictureBytes(int auctionId, int pictureId) {
        byte[] pictureBytes = auctionRepository.findAuctionPictureBytes(auctionId, pictureId);
        if (pictureBytes == null) {
            return auctionRepository.getDefaultPicture();
        }
        return pictureBytes;
    }

    public boolean checkIfUserCanSeePictures(AuctionBody auctionBody) {
        return auctionBody.getType() != AuctionTypeEnum.PRIVATE || auctionBody.getOwnerId() == userService.getLoggedInUserId() || invitationRepository.invitationExists(auctionBody.getId(), userService.getLoggedInUserEmail());
    }

    public MultipartFile resize(MultipartFile picture) throws IOException {
        return resizePictureByBytes(picture.getBytes(), picture.getOriginalFilename());
    }

    private BufferedImage scaleImage(BufferedImage bufferedImage) {
        int origWidth = bufferedImage.getWidth();
        int origHeight = bufferedImage.getHeight();
        double scale;
        if (origHeight > origWidth)
            scale = (double) 300 / origHeight;
        else
            scale = (double) 300 / origWidth;
        if (scale > 1.0)
            return (bufferedImage);
        int scaledWidth = (int) (scale * origWidth);
        int scaledHeight = (int) (scale * origHeight);
        Image scaledImage = bufferedImage.getScaledInstance(scaledWidth, scaledHeight, Image.SCALE_SMOOTH);
        BufferedImage scaledBI = new BufferedImage(scaledWidth, scaledHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = scaledBI.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.drawImage(scaledImage, 0, 0, null);
        g.dispose();
        return (scaledBI);
    }

    private String calculateEtag(byte[] pictureBytes) throws NoSuchAlgorithmException {
        MessageDigest m = MessageDigest.getInstance("MD5");
        m.reset();
        m.update(pictureBytes);
        byte[] digest = m.digest();
        BigInteger bigInt = new BigInteger(1, digest);
        String hashtext = bigInt.toString(16);
        while (hashtext.length() < 32) {
            hashtext = "0" + hashtext;
        }
        return hashtext;
    }

    public ResponseEntity<byte[]> getImageResponseEntity(@PathVariable int id, @PathVariable int pictureId) throws NoSuchAlgorithmException {
        byte[] pictureBytes = getAuctionPictureBytes(id, pictureId);

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_PNG);

        return ResponseEntity
                .ok()
                .cacheControl(
                        CacheControl
                                .maxAge(15, TimeUnit.SECONDS)
                                .noTransform()
                ).headers(headers)
                .eTag(calculateEtag(pictureBytes))
                .body(pictureBytes);
    }

    /*
    * Notification
    */

    private void sendCreateNotificationsToAuctionUsers(AuctionBody auctionBody, String loggedInUserEmail) {
        notificationService.sendNotification(auctionBody.getId(), captionService.getCaption("create.auction.subject"),
                captionService.getCaption("create.auction.text", applicationProperties.getUrl()), loggedInUserEmail);

        if (auctionBody.getInvitations() != null)
            for (Invitation anEmailsList : auctionBody.getInvitations()) {
                if (anEmailsList.getEmail().equals(loggedInUserEmail)) {
                    auctionBody.getInvitations().remove(anEmailsList);
                }
                notificationService.sendNotification(auctionBody.getId(),
                        captionService.getCaption("invite.to.auction.subject"),
                        captionService.getCaption("invite.to.auction.text", applicationProperties.getUrl()), anEmailsList.getEmail());
            }
    }

    private void sendNotificationsAboutDeletedAuction(AuctionBody auctionBody, int NO_NUMBER) {
        for (Invitation anEmailsList : auctionBody.getInvitations()) {
            notificationService.sendNotification(NO_NUMBER, captionService.getCaption("delete.auction.subject") + auctionBody.getTitle(),
                    captionService.getCaption("delete.auction.text") + auctionBody.getTitle(), anEmailsList.getEmail());
            notificationsRepository.insertToNotificationsTable(DELETED, anEmailsList.getEmail(), auctionBody.getId());
        }
        notificationService.sendNotification(NO_NUMBER, captionService.getCaption("delete.auction.subject") + auctionBody.getTitle(),
                captionService.getCaption("delete.auction.text") + auctionBody.getTitle(), userService.getLoggedInUserEmail());
    }


    private void sendNotificationsToAuctionUsers(AuctionBody auctionBody, int NO_NUMBER) {
        if (auctionBody.getInvitations() != null) {
            auctionBody.getInvitations().stream().filter(anEmailsList -> !anEmailsList.getEmail().equals("")).forEach(anEmailsList -> {
                notificationService.sendNotification(NO_NUMBER, captionService.getCaption("update.auction.subject") + auctionBody.getTitle(),
                        captionService.getCaption("update.auction.text") + auctionBody.getTitle(), anEmailsList.getEmail());
                notificationsRepository.insertToNotificationsTable(UPDATED, anEmailsList.getEmail(), auctionBody.getId());
            });
            notificationService.sendNotification(NO_NUMBER, captionService.getCaption("update.auction.subject") + auctionBody.getTitle(),
                    captionService.getCaption("update.auction.text") + auctionBody.getTitle(), userService.getLoggedInUserEmail());
        }
    }
    /*
    * Other stuff
    */


    public String getTimeRemaining(int auctionId) {
        AuctionBody auction = auctionRepository.findAuction(auctionId);

        LocalDateTime fromDateTime = LocalDateTime.now();
        LocalDateTime toDateTime = auction.getEndDate();

        LocalDateTime tempDateTime = LocalDateTime.from(fromDateTime);

        long years = tempDateTime.until(toDateTime, ChronoUnit.YEARS);
        tempDateTime = tempDateTime.plusYears(years);

        long months = tempDateTime.until(toDateTime, ChronoUnit.MONTHS);
        tempDateTime = tempDateTime.plusMonths(months);

        long days = tempDateTime.until(toDateTime, ChronoUnit.DAYS);
        tempDateTime = tempDateTime.plusDays(days);

        long hours = tempDateTime.until(toDateTime, ChronoUnit.HOURS);
        tempDateTime = tempDateTime.plusHours(hours);

        long minutes = tempDateTime.until(toDateTime, ChronoUnit.MINUTES);
        tempDateTime = tempDateTime.plusMinutes(minutes);

        long seconds = tempDateTime.until(toDateTime, ChronoUnit.SECONDS);

        return days + " days " + hours + " hours " + minutes + " mins " + seconds + " secs";
    }

    public long getSecondsRemaining(AuctionBody auction) {
        return ChronoUnit.SECONDS.between(LocalDateTime.now(), auction.getEndDate());
    }


    @Transactional(propagation = Propagation.REQUIRED)
    public boolean isLoggedInUserAuctionOwner(int auctionId) {
        AuctionBody auction = auctionRepository.findAuction(auctionId);
        int auctionOwnerId = auction.getOwnerId();
        return userService.getLoggedInUserId() == auctionOwnerId;
    }


    int getItemQuantityYouCanBuy(int quantityBuyWish, int auctionId) {
        int quantityLeft = auctionRepository.getItemQuantityLeft(auctionId);
        if (quantityBuyWish <= quantityLeft) {
            return quantityBuyWish;
        } else {
            return quantityLeft;
        }
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public boolean isLoggedInUserInvitedToAuction(int auctionId) {
        AuctionBody auction = auctionRepository.findAuction(auctionId);
        for (Invitation invitation : auction.getInvitations()) {
            if (invitation.getEmail().equals(userService.getLoggedInUserEmail()))
                return true;
        }
        return false;
    }

    public boolean isAuctionEnded(AuctionBody auctionBody) {
        return auctionBody.getEndDate().isBefore(LocalDateTime.now());
    }

    public UserBody getAuctionOwner(int auctionId) {
        return userRepository.getOwnerByAuctionId(auctionId);
    }






    @Transactional(propagation = Propagation.REQUIRED)
    public void buyOutAuction(AuctionBody auction) {
        auctionRepository.changeAuctionStatus(ENDED, auction.getId());

        messagingService.notifyBuyerAboutBuyout(auction, userService.getLoggedInUserEmail());
        messagingService.notifyOwnerAboutBuyout(auction, userRepository.getUserByEmail(userService.getLoggedInUserEmail()));

        notificationService.sendNotification(NO_NUMBER, captionService.getCaption("buyout.congratulation.winner.subject"),
                captionService.getCaption("buyout.congratulation.winner.text"), userService.getLoggedInUserEmail());

        notificationService.sendNotification(NO_NUMBER,
                captionService.getCaption("buyout.congratulation.owner.subject"), captionService.getCaption("buyout.congratulation.owner.text"),
                userRepository.getUserById(auction.getOwnerId()).getEmail());
    }





    public boolean invitationIsValid(AuctionBody auction) {
        if (isAuctionEnded(auction) || !auction.getStatus().equals(AuctionStatusEnum.ACTIVE))
            return false;

        if (!auction.getIsAnonymous()) {
            int id = auction.getId();
            return id == AUCTION_IS_NOT_CREATED_ID || isLoggedInUserAuctionOwner(id) || isLoggedInUserInvitedToAuction(id);
        }

        return auction.getIsAnonymous();

    }





}