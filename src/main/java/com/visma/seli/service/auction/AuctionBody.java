package com.visma.seli.service.auction;

import com.visma.seli.controllers.customValidators.CustomAuctionValidators.*;
import com.visma.seli.service.captions.CaptionService;
import com.visma.seli.service.enums.AuctionCurrencyEnum;
import com.visma.seli.service.enums.AuctionStatusEnum;
import com.visma.seli.service.enums.AuctionTypeEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@ValidStartingPrice
@ValidInvitations
@ValidAuctionQuantity
@ValidBuyOut
@ValidEndDate.List({@ValidEndDate(first = "startDate", second = "endDate", third = "id")})
@ValidStartDate.List({@ValidStartDate(first = "startDate", second = "id")})
public class AuctionBody {
    @Autowired
    private CaptionService captionService;
    @Size(min = 1, max = 100, message = "Title must be between 1 and 100 characters")
    private String title;
    @Size(min = 1, max = 999, message = "Description must be between 1 and  999 characters")
    private String description;
    @NotNull(message = "Select type")
    private AuctionTypeEnum type;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime startDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime endDate;
    private Integer itemQuantity;
    @DecimalMin(value = "0.01", message = "Value needs to be more then 0")
    @NotNull(message = "Please enter starting price")
    @DecimalMax(value = "100000000", message = "Value needs to be less than 100000000")
    private BigDecimal startingPrice;
    @DecimalMin(value = "0.01", message = "Value needs to be more then 0")
    private BigDecimal buyOutPrice;
    @NotNull(message = "Select currency")
    private AuctionCurrencyEnum currency;
    private AuctionStatusEnum status;
    private int id;
    @ValidPictureFileFormat(message = "Invalid picture format. Only JPEG,JPG,PNG are supported")
    private MultipartFile[] pictureFiles = new MultipartFile[5];
    private int ownerId;
    private List<Invitation> invitations;
    private boolean allowToBidForQuantity;
    private boolean allowBuyOut;
    private UUID auctionIdUrlOwner;
    private int bidsQuantity;
    private String link;
    private List<UUID> listOfPhotosUUUID;
    private boolean isAnonymous;




	public List<UUID> getListOfPhotosUUUID() {
        return listOfPhotosUUUID;
    }

    public void setListOfPhotosUUUID(List<UUID> listOfPhotosUUUID) {
        this.listOfPhotosUUUID = listOfPhotosUUUID;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getLink() {

        return link;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int auctionOwnerId) {
        this.ownerId = auctionOwnerId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public Integer getItemQuantity() {
        return itemQuantity;
    }

    public void setItemQuantity(Integer itemQuantity) {
        this.itemQuantity = itemQuantity;
    }

    public BigDecimal getStartingPrice() {
        return startingPrice;
    }

    public void setStartingPrice(BigDecimal startingPrice) {
        this.startingPrice = startingPrice;
    }

    public AuctionTypeEnum getType() {
        return type;
    }

    public void setType(AuctionTypeEnum type) {
        this.type = type;
    }

    public AuctionCurrencyEnum getCurrency() {
        return currency;
    }

    public void setCurrency(AuctionCurrencyEnum currency) {
        this.currency = currency;
    }

    public AuctionStatusEnum getStatus() {
        return status;
    }

    public void setStatus(AuctionStatusEnum status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Invitation> getInvitations() {
        return invitations;
    }

    public void setInvitations(List<Invitation> invitations) {
        this.invitations = invitations;
    }

    public boolean isAllowToBidForQuantity() {
        return allowToBidForQuantity;
    }

    public void setAllowToBidForQuantity(boolean allowToBidForQuantity) {
        this.allowToBidForQuantity = allowToBidForQuantity;
    }

    public boolean isAllowBuyOut() {
        return allowBuyOut;
    }

    public void setAllowBuyOut(boolean allowBuyOut) {
        this.allowBuyOut = allowBuyOut;
    }

    public BigDecimal getBuyOutPrice() {
        return buyOutPrice;
    }

    public void setBuyOutPrice(BigDecimal buyOutPrice) {
        this.buyOutPrice = buyOutPrice;
    }

    public UUID getAuctionIdUrlOwner() {
        return auctionIdUrlOwner;
    }

    public void setAuctionIdUrlOwner(UUID auctionIdUrl) {
        this.auctionIdUrlOwner = auctionIdUrl;
    }

    public int getBidsQuantity() {
        return bidsQuantity;
    }

    public void setBidsQuantity(int bidsQuantity) {
        this.bidsQuantity = bidsQuantity;
    }

    public MultipartFile[] getPictureFiles() {
        return pictureFiles;
    }

    public void setPictureFiles(MultipartFile[] pictureFiles) {
        this.pictureFiles = pictureFiles;
    }

    public boolean getLastTimeForInvitations(){
        return this.startDate.plusHours(12).isAfter(LocalDateTime.now());
    }
    
    public boolean getIsAnonymous() {
    	return isAnonymous;
    }
    
    public void setAnonymous(boolean isAnonymous) {
    	this.isAnonymous = isAnonymous;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AuctionBody that = (AuctionBody) o;

        if (getId() != that.getId()) return false;
        if (getOwnerId() != that.getOwnerId()) return false;
        if (isAllowToBidForQuantity() != that.isAllowToBidForQuantity()) return false;
        if (isAllowBuyOut() != that.isAllowBuyOut()) return false;
        if (getBidsQuantity() != that.getBidsQuantity()) return false;
        if (getTitle() != null ? !getTitle().equals(that.getTitle()) : that.getTitle() != null) return false;
        if (getDescription() != null ? !getDescription().equals(that.getDescription()) : that.getDescription() != null)
            return false;
        if (getType() != that.getType()) return false;
        if (getStartDate() != null ? !getStartDate().equals(that.getStartDate()) : that.getStartDate() != null)
            return false;
        if (getEndDate() != null ? !getEndDate().equals(that.getEndDate()) : that.getEndDate() != null) return false;
        if (getItemQuantity() != null ? !getItemQuantity().equals(that.getItemQuantity()) : that.getItemQuantity() != null)
            return false;
        if (getStartingPrice() != null ? !getStartingPrice().equals(that.getStartingPrice()) : that.getStartingPrice() != null)
            return false;
        if (getBuyOutPrice() != null ? !getBuyOutPrice().equals(that.getBuyOutPrice()) : that.getBuyOutPrice() != null)
            return false;
        if (getCurrency() != that.getCurrency()) return false;
        if (getStatus() != that.getStatus()) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        if (!Arrays.equals(getPictureFiles(), that.getPictureFiles())) return false;
        if (getInvitations() != null ? !getInvitations().equals(that.getInvitations()) : that.getInvitations() != null)
            return false;
        if (getAuctionIdUrlOwner() != null ? !getAuctionIdUrlOwner().equals(that.getAuctionIdUrlOwner()) : that.getAuctionIdUrlOwner() != null)
            return false;
        return getLink() != null ? getLink().equals(that.getLink()) : that.getLink() == null;

    }

    @Override
    public int hashCode() {
        int result = getTitle() != null ? getTitle().hashCode() : 0;
        result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
        result = 31 * result + (getType() != null ? getType().hashCode() : 0);
        result = 31 * result + (getStartDate() != null ? getStartDate().hashCode() : 0);
        result = 31 * result + (getEndDate() != null ? getEndDate().hashCode() : 0);
        result = 31 * result + (getItemQuantity() != null ? getItemQuantity().hashCode() : 0);
        result = 31 * result + (getStartingPrice() != null ? getStartingPrice().hashCode() : 0);
        result = 31 * result + (getBuyOutPrice() != null ? getBuyOutPrice().hashCode() : 0);
        result = 31 * result + (getCurrency() != null ? getCurrency().hashCode() : 0);
        result = 31 * result + (getStatus() != null ? getStatus().hashCode() : 0);
        result = 31 * result + getId();
        result = 31 * result + Arrays.hashCode(getPictureFiles());
        result = 31 * result + getOwnerId();
        result = 31 * result + (getInvitations() != null ? getInvitations().hashCode() : 0);
        result = 31 * result + (isAllowToBidForQuantity() ? 1 : 0);
        result = 31 * result + (isAllowBuyOut() ? 1 : 0);
        result = 31 * result + (getAuctionIdUrlOwner() != null ? getAuctionIdUrlOwner().hashCode() : 0);
        result = 31 * result + getBidsQuantity();
        result = 31 * result + (getLink() != null ? getLink().hashCode() : 0);
        return result;
    }
    
}