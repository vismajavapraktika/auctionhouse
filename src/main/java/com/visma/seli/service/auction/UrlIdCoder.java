package com.visma.seli.service.auction;

import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UrlIdCoder {
    public UUID createUUID() {
        return UUID.randomUUID();
    }
}
