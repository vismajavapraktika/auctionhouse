package com.visma.seli.service.auction;

import com.visma.seli.service.enums.InvitationStatusEnum;

import java.time.LocalDateTime;
import java.util.UUID;

public class Invitation {
    private int id;
    private int auctionId;
    private String email;
    private boolean emailSent;
    private InvitationStatusEnum status;
    private UUID invitedUuid;
    private int userId;
    private boolean notifiedAboutAuctionEnd;
    private LocalDateTime insertDate;

    public boolean isNotifiedAboutAuctionEnd() {
        return notifiedAboutAuctionEnd;
    }

    public void setNotifiedAboutAuctionEnd(boolean notifiedAboutAuctionEnd) {
        this.notifiedAboutAuctionEnd = notifiedAboutAuctionEnd;
    }

    public UUID getInvitedUuid() {
        return invitedUuid;
    }

    public void setInvitedUuid(UUID invitedUuid) {
        this.invitedUuid = invitedUuid;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public InvitationStatusEnum getStatus() {
        return status;
    }

    public void setStatus(InvitationStatusEnum status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(int auction_id) {
        this.auctionId = auction_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEmailSent() {
        return emailSent;
    }

    public void setEmailSent(boolean emailSent) {
        this.emailSent = emailSent;
    }

    public LocalDateTime getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(LocalDateTime insertDate) {
        this.insertDate = insertDate;
    }
}
