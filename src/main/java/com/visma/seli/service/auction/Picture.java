package com.visma.seli.service.auction;

import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
public class Picture {
    private byte[] pictureBytes;
    private String pictureName;
    private int photoAuctionId, pictureId;
    private UUID pictureUUID;
    private LocalDateTime insertDate;

    public LocalDateTime getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(LocalDateTime insertDate) {
        this.insertDate = insertDate;
    }

    public UUID getPictureUUID() {
        return pictureUUID;
    }

    public void setPictureUUID(UUID pictureUUID) {
        this.pictureUUID = pictureUUID;
    }

    public int getPictureId() {
        return pictureId;
    }

    public void setPictureId(int pictureId) {
        this.pictureId = pictureId;
    }

    public int getPhotoAuctionId() {
        return photoAuctionId;
    }

    public void setPhotoAuctionId(int photoAuctionId) {
        this.photoAuctionId = photoAuctionId;
    }

    public byte[] getPictureBytes() {
        return pictureBytes;
    }

    public void setPictureBytes(byte[] pictureBytes) {
        this.pictureBytes = pictureBytes;
    }

    public String getPictureName() {
        return pictureName;
    }

    public void setPictureName(String pictureName) {
        this.pictureName = pictureName;
    }
}
