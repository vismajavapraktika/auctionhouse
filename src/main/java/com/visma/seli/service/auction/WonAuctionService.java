package com.visma.seli.service.auction;

import com.visma.seli.config.properties.ApplicationProperties;
import com.visma.seli.config.properties.DurationProperties;
import com.visma.seli.server.data.access.repository.database.auction.AuctionRepository;
import com.visma.seli.server.data.access.repository.database.auction.won.WonAuctionsRepository;
import com.visma.seli.server.data.access.repository.database.user.anonymousUser.AnonymousUserRepository;
import com.visma.seli.server.data.access.repository.database.user.logedInUser.UserRepository;
import com.visma.seli.service.auction.bid.WinMessageData;
import com.visma.seli.service.captions.CaptionService;
import com.visma.seli.service.mailing.NotificationService;
import com.visma.seli.service.user.UserBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.visma.seli.service.enums.WinnerMessageResponseStatusEnum.*;

@Service
public class WonAuctionService {

    @Autowired
    private WonAuctionsRepository wonAuctionsRepository;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private CaptionService captionService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private DurationProperties durationProperties;
    @Autowired
    private AuctionRepository auctionRepository;
    @Autowired
    private AuctionService auctionService;
    private static final int NO_NUMBER = -1;
    @Autowired
    private AnonymousUserRepository anonymousUserRepository;
    @Autowired
    private ApplicationProperties applicationProperties;

    @Transactional
    public void doEndedAutionsMessaging() {
        wonAuctionsRepository.updateAllExpiredWinStatus();

        List<WinMessageData> winnerList = wonAuctionsRepository.selectAllWinnersToNotify();
        notifyAllSelectedWinners(winnerList);
    }

    private void notifyAllSelectedWinners(List<WinMessageData> winnerList) {
        for (WinMessageData winner : winnerList) {
            winner.setExpirationTime(LocalDateTime.now().plus(durationProperties.getAnswerWaitDuration()));
            sendMessageForWinner(winner);
            wonAuctionsRepository.setNewMessageExpirationTimeStatus(winner);
        }
    }

    private void sendMessageForWinner(WinMessageData winner) {

        UserBody winnerBody = userRepository.getUserById(winner.getBid().getUserId());

        notificationService.sendNotification(NO_NUMBER, captionService.getCaption("winner.message.subject"),
                makeMessageBodyForWinner(winner), winnerBody.getEmail());

    }

    private String makeMessageBodyForWinner(WinMessageData winner) {


        String messageBody = captionService.getCaption("winner.message.congratulations") + "\n";
        messageBody = messageBody.concat(captionService.getCaption("winner.message.quantity") +
                auctionService.getItemQuantityYouCanBuy(winner.getBid().getBiddingQuantity(), winner.getBid().getAuctionId()) + "\n");
        messageBody = messageBody.concat(captionService.getCaption("winner.message.bid") + winner.getBid().getAmount() + "\n");
        messageBody = messageBody.concat(captionService.getCaption("winner.message.details") + winner.getBid().getAuctionId() + "\n");
        messageBody = messageBody.concat(captionService.getCaption("winner.message.autodecline",
                applicationProperties.getUrl()+"auction/"+winner.getBid().getAuctionId())+
                winner.getExpirationTime().format(DateTimeFormatter.ofPattern(captionService.getCaption("datetimeformater.format"))));
        return messageBody;
    }

    public void acceptWin(int auctionId, int userId) {
    	WinMessageData winMessageData = wonAuctionsRepository.getWinMessageData(auctionId, userId);
    	if(winMessageData == null)
    		return;
        wonAuctionsRepository.updateStatusAfterWinnerAnswer(ACCEPTED, auctionId, userId);
        notifyOwnerAboutWinnerAccept(winMessageData);
        accountBoughItemQuantity(winMessageData);
    }

    public void declineWin(int auctionId, int userId) {
        if(wonAuctionsRepository.updateStatusAfterWinnerAnswer(DECLINED, auctionId, userId) != 1)
        	return;
        if (!isWinnersLeft(auctionId)) {
            notifyOwnerAboutEndedWinners(auctionId);
        } else {
            // TODO: 2017-03-02 next winner in the line
            List<WinMessageData> listOfWinners= wonAuctionsRepository.setNextWinnersStatusAndExpirationDate(auctionId);
            notifyAllSelectedWinners(listOfWinners);
        }
    }

    private boolean isWinnersLeft(int id) {
        if (wonAuctionsRepository.getWinnersLeft(id) == 0) {
            return false;
        } else {
            return true;
        }
    }

    private boolean isItemsLeft(int id) {
        if (auctionRepository.getItemQuantityLeft(id) == 0) {
            return false;
        } else {
            return true;
        }
    }

    private void notifyOwnerAboutWinnerAccept(WinMessageData winMessageData) {
        UserBody owner = userRepository.getOwnerByAuctionId(winMessageData.getBid().getAuctionId());
        notificationService.sendNotification(NO_NUMBER, captionService.getCaption("owner.message.subject.accepted"),
                messageBodyForOwnerAboutAccept(winMessageData), owner.getEmail());
    }

    private void notifyOwnerAboutEndedWinners(int auctionId) { // TODO: 2016-12-14 Change string to caption
        notificationService.sendNotification(NO_NUMBER, captionService.getCaption("notify.owner.about.auction.ended"), "Your auction ended, there is no winners, all winners declined.",
                userRepository.getOwnerByAuctionId(auctionId).getEmail());
    }

    private void notifyOwnerAboutEndedGoods(WinMessageData winMessageData) { // TODO: 2016-12-14 Change string to caption
        notificationService.sendNotification(NO_NUMBER, "Sold Out", "Your auction has completed, your goods have been sold out!",
                userRepository.getOwnerByAuctionId(winMessageData.getBid().getAuctionId()).getEmail());
    }

    private String messageBodyForOwnerAboutAccept(WinMessageData winData) {

        UserBody winner = userRepository.getUserById(winData.getBid().getUserId());
        String messageBody = captionService.getCaption("owner.message.title") + "\n";
        messageBody = messageBody.concat(captionService.getCaption("owner.message.position") +
                wonAuctionsRepository.getWinnersPosition(winData.getBid().getAuctionId(), winData.getBid().getUserId()) + "\n");
        messageBody = messageBody.concat(captionService.getCaption("owner.message.bid") + winData.getBid().getAmount() + "\n");
        messageBody = messageBody.concat(captionService.getCaption("owner.message.quantity") +
                auctionService.getItemQuantityYouCanBuy(winData.getBid().getBiddingQuantity(), winData.getBid().getAuctionId()) + "\n\n");
        messageBody = messageBody.concat(captionService.getCaption("owner.message.winner.details") + "\n\n");
        messageBody = messageBody.concat(captionService.getCaption("owner.message.email") + winner.getEmail() + "\n");
        messageBody = messageBody.concat(captionService.getCaption("owner.message.phone") + winner.getPhone() + "\n");
        messageBody = messageBody.concat(captionService.getCaption("owner.message.link", applicationProperties.getUrl()) + winData.getBid().getAuctionId());
        return messageBody;
    }

    private void accountBoughItemQuantity(WinMessageData winMessageData) {
        int quantity = auctionService.getItemQuantityYouCanBuy(winMessageData.getBid().getBiddingQuantity(), winMessageData.getBid().getAuctionId());
        auctionRepository.setSubtractSoldItemQuantity(quantity, winMessageData.getBid().getAuctionId());
        checkForMessageClosing(winMessageData);
    }

    private void checkForMessageClosing(WinMessageData winMessageData) {
        if (!isItemsLeft(winMessageData.getBid().getAuctionId())) {
            wonAuctionsRepository.updateAuctionStatuses(NOT_SENT, OUT_OF_STOCK, winMessageData.getBid().getAuctionId());
            notifyOwnerAboutEndedGoods(winMessageData);
        }
    }

    public void stopEndedAuctionFromMessaging(int auctionId) {
        wonAuctionsRepository.updateAuctionStatuses(NOT_SENT, STOPPED, auctionId);
    }


}
