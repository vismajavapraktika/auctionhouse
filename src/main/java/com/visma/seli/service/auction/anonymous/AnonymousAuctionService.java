package com.visma.seli.service.auction.anonymous;

import com.visma.seli.config.properties.ApplicationProperties;
import com.visma.seli.controllers.customValidators.CustomAuctionValidators.AuctionInvitationsValidator;
import com.visma.seli.server.data.access.repository.database.auction.AuctionRepository;
import com.visma.seli.server.data.access.repository.database.auction.bids.BidRepository;
import com.visma.seli.server.data.access.repository.database.auction.invitations.InvitationRepository;
import com.visma.seli.server.data.access.repository.database.pictures.PictureRepository;
import com.visma.seli.server.data.access.repository.database.user.anonymousUser.AnonymousUserRepository;
import com.visma.seli.server.data.access.repository.database.user.logedInUser.UserRepository;
import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.AuctionService;
import com.visma.seli.service.auction.Invitation;
import com.visma.seli.service.auction.UrlIdCoder;
import com.visma.seli.service.auction.bid.Bid;
import com.visma.seli.service.captions.CaptionService;
import com.visma.seli.service.enums.AuctionCurrencyEnum;
import com.visma.seli.service.enums.AuctionStatusEnum;
import com.visma.seli.service.enums.AuctionTypeEnum;
import com.visma.seli.service.enums.UserEnum;
import com.visma.seli.service.mailing.NotificationService;
import com.visma.seli.service.user.AnonymousUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
public class AnonymousAuctionService {

    @Autowired
    private NotificationService notificationService;
    @Autowired
    private CaptionService captionService;
    @Autowired
    private AuctionRepository auctionRepository;
    @Autowired
    private AnonymousUserRepository anonymousUserRepository;
    @Autowired
    private BidRepository bidRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AuctionInvitationsValidator auctionInvitationsValidator;
    @Autowired
    private AuctionService auctionService;
    @Autowired
    private UrlIdCoder urlIdCoder;
    @Autowired
    private InvitationRepository invitationRepository;
    @Autowired
    private PictureRepository pictureRepository;
    @Autowired
    private AuctionInvitationsValidator auctionInvitationValidator;
    @Autowired
    private ApplicationProperties applicationProperties;
    public static final int NO_NUMBER = -1;


    public void sendNotificationsAboutDeletedAnonymousAuction(AuctionBody auctionBody, int NO_NUMBER) {
        for (Invitation invitation : auctionBody.getInvitations()) {
            notificationService.sendNotification(NO_NUMBER, captionService.getCaption("delete.auction.subject") + auctionBody.getTitle(),
                    captionService.getCaption("delete.auction.text") + auctionBody.getTitle(), invitation.getEmail());
        }
        notificationService.sendNotification(NO_NUMBER, captionService.getCaption("delete.auction.subject") + auctionBody.getTitle(),
                captionService.getCaption("delete.auction.text") + auctionBody.getTitle(), auctionRepository.findAnonymousOwnerEmailById(auctionBody.getOwnerId()));
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public void insertAnonymousAuctionValuesToJDBC(AuctionBody auctionBody, int anonymousUserID, String AUCTION_TYPE) {
        auctionRepository.insertNewAuction(auctionBody, anonymousUserID, AUCTION_TYPE);
        
        if (auctionBody.getListOfPhotosUUUID() != null)
            pictureRepository.deleteAuctionTemporaryPictures(auctionBody);
        
        invitationRepository.insertInvitedAnonymous(auctionBody.getInvitations(), auctionBody.getId());
        List<Invitation> temporaryInvitationList = new ArrayList<>();
        
        if (auctionBody.getInvitations() != null) {
            for (Invitation invitation : auctionBody.getInvitations()) {
                AnonymousUser temporaryInvitedUser = anonymousUserRepository.checkIfUserExistsInJDBC(invitation.getEmail());
                if (temporaryInvitedUser == null)
                    temporaryInvitationList.add(invitation);
            }
            if (!temporaryInvitationList.isEmpty()) {
                temporaryInvitationList = auctionBody.getInvitations();
                auctionBody.setInvitations(temporaryInvitationList);
                invitationRepository.insertInvitedAnonymousToAnonymousUserTable(auctionBody.getInvitations());
            }
        }
    }

    public void sendNotificationsForAuctionUsers(AuctionBody auctionBody, int anonymousUserID) {
        notificationService.sendNotification(auctionBody.getAuctionIdUrlOwner(),
                captionService.getCaption("create.anonymous.auction.subject"),
                captionService.getCaption("create.anonymous.auction.text", applicationProperties.getUrl()),
                anonymousUserRepository.getAnonymousUserById(anonymousUserID).getEmail());

        if (auctionBody.getInvitations() != null) {
            for (Invitation anEmailsList : auctionBody.getInvitations()) {
                notificationService.sendNotification(anEmailsList.getInvitedUuid(),
                        captionService.getCaption("invite.anonymous.auction.subject"),
                        captionService.getCaption("invite.anonymous.auction.text", applicationProperties.getUrl()), anEmailsList.getEmail());
            }
        }
    }

    public BindingResult checkAnonymousAuctionBindingResult(AuctionBody auctionBody, BindingResult bindingResult, int anonymousAuctionUserId) {
        if (!auctionBody.getCurrency().equals(AuctionCurrencyEnum.EUR)) {
            ObjectError error = new ObjectError("currency",
                    captionService.getCaption("error.anonymous.auction.currency.set.via.html"));
            bindingResult.addError(error);
        }
        if (!auctionBody.getType().equals(AuctionTypeEnum.PRIVATE)) {
            ObjectError error = new ObjectError("type",
                    captionService.getCaption("error.anonymous.auction.type.set.via.html"));
            bindingResult.addError(error);
        }
        if (auctionBody.getInvitations() != null)
            for (Invitation invitation : auctionBody.getInvitations()){
                if (invitation.getEmail().equals(anonymousUserRepository.getAnonymousUserById(anonymousAuctionUserId).getEmail())){
                    FieldError error = new FieldError("auction", "invitations",
                            invitation.getEmail() + " " +captionService.getCaption("auction.owner.self.invitation.error"));
                    bindingResult.addError(error);
                }
            }
        return bindingResult;
    }


    public void insertNewAnonymousBid(UUID uuid, Bid auctionBid) {
        auctionBid.setAuctionId(auctionRepository.findAnonymousAuctionIdByUUID(uuid));
        auctionBid.setUserId(userRepository.getInvitedUserIdByUUID(uuid));
        bidRepository.insertNewBid(auctionBid);
    }

    public AuctionBody findAuction(UUID uuid, UserEnum userEnum) {
        return auctionRepository.findAnonymousAuction(uuid, userEnum);
    }

    public int checkIfAnonymousUserExists(Invitation invitation) {
        AnonymousUser existingInvitation = anonymousUserRepository.checkIfUserExistsInJDBC(invitation.getEmail());
        if (existingInvitation == null)
            return -1;
        else
            return existingInvitation.getId();
    }

    public void setAnonymousAuctionDefaultValues(AuctionBody auctionBody) {
        auctionBody.setStatus(AuctionStatusEnum.ACTIVE);
        auctionBody.setBuyOutPrice(BigDecimal.ZERO);
        auctionBody.setAuctionIdUrlOwner(urlIdCoder.createUUID());
        auctionBody.setType(AuctionTypeEnum.PRIVATE);
        auctionBody.setCurrency(AuctionCurrencyEnum.EUR);
        auctionBody.setAnonymous(true);
    }

    public void deleteAnonymousAuction(UUID userUuid) {
        if(auctionRepository.checkIfAnonymousAuctionOwner(userUuid)) {
            AuctionBody auctionBody = auctionRepository.findAnonymousAuction(userUuid,UserEnum.OWNER);
            if (!auctionService.isAuctionEnded(auctionBody)) {
                auctionRepository.changeAuctionStatus(AuctionStatusEnum.DELETED, auctionBody.getId());
                sendNotificationsAboutDeletedAnonymousAuction(auctionBody, NO_NUMBER);
            }
        }
    }

    public AuctionBody getAuction(UUID uuid)
    {
        boolean isAnonymousAuctionOwner = auctionRepository.checkIfAnonymousAuctionOwner(uuid);
        AuctionBody auction = getAuction(isAnonymousAuctionOwner, uuid);
        return auction;
    }

    public AuctionBody getAuction(boolean isAnonymousAuctionOwner, UUID uuid) {
        if (isAnonymousAuctionOwner) {
            return auctionRepository.findAnonymousAuction(uuid, UserEnum.OWNER);
        } else {
            return auctionRepository.findAnonymousAuction(uuid, UserEnum.INVITED);
        }
    }
}