package com.visma.seli.service.auction.invitations;

import com.visma.seli.config.properties.ApplicationProperties;
import com.visma.seli.controllers.customValidators.CustomAuctionValidators.AuctionInvitationsValidator;
import com.visma.seli.server.data.access.repository.database.NotificationsRepository;
import com.visma.seli.server.data.access.repository.database.auction.AuctionRepository;
import com.visma.seli.server.data.access.repository.database.auction.invitations.InvitationRepository;
import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.AuctionService;
import com.visma.seli.service.auction.Invitation;
import com.visma.seli.service.auction.anonymous.AnonymousAuctionService;
import com.visma.seli.service.captions.CaptionService;
import com.visma.seli.service.enums.AuctionStatusEnum;
import com.visma.seli.service.enums.InvitationStatusEnum;
import com.visma.seli.service.enums.UserEnum;
import com.visma.seli.service.mailing.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.visma.seli.service.enums.NotificationEnum.INVITATION;

/**
 * Created by justas.rutkauskas on 2/8/2017.
 */
@Service
public class AuctionInviteService {

    @Autowired
    private AuctionRepository auctionRepository;
    @Autowired
    private InvitationRepository invitationRepository;
    @Autowired
    private AuctionInvitationsValidator auctionInvitationsValidator;
    @Autowired
    private NotificationsRepository notificationsRepository;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private CaptionService captionService;
    @Autowired
    private ApplicationProperties applicationProperties;
    @Transactional(propagation = Propagation.REQUIRED)
    public void reinviteToAuction(int auctionId, int invitationId) {
        AuctionBody auction = auctionRepository.findAuction(auctionId);
        Invitation invitation = findInvitation(auction, invitationId);
        if(invitation != null || !auction.getStatus().equals(AuctionStatusEnum.ACTIVE))
        {
            sendNotificationsAboutInvitationToAuction(auction,invitation, auctionId);
            invitationRepository.updateInvitation(auctionId, invitation.getEmail(), InvitationStatusEnum.NO_RESPONSE);
        }
    }
    @Transactional(propagation = Propagation.REQUIRED)
    public void inviteToAuction(@PathVariable int id, @ModelAttribute("auction") AuctionBody auctionBody) throws Exception {
        AuctionBody auction = auctionRepository.findAuction(id);
        List<Invitation> newInvitations = auctionBody.getInvitations();

        auctionInvitationsValidator.validateInvitation(auction, newInvitations);

        invitationRepository.insertPeopleToInvite(auction.getId(), newInvitations);
        for (Invitation invitation : newInvitations) {
            sendNotificationsAboutInvitationToAuction(auction, invitation, id);
            notificationsRepository.insertToNotificationsTable(INVITATION, invitation.getEmail(),
                    auction.getId());
        }
    }

    private void sendNotificationsAboutInvitationToAuction(AuctionBody auction, Invitation invitation, int id) {
        notificationService.sendNotification(id,captionService.getCaption("invite.to.auction.subject") + auction.getTitle(),
                captionService.getCaption("invite.to.auction.text", applicationProperties.getUrl()), invitation.getEmail());
    }

    public Invitation findInvitation(AuctionBody auction, int invitationId) {
        for (Invitation invitation : auction.getInvitations()) {
            if (invitation.getId() == invitationId) {
                return invitation;
            }
        }
        return null;
    }
    @Transactional(propagation = Propagation.REQUIRED)
    public List<Invitation> checkIfInvitationsExists(List<Invitation> newInvitations, int auctionId) {
        if(AuctionService.AUCTION_IS_NOT_CREATED_ID == auctionId)
            return newInvitations;

        List<Invitation> resultList = new ArrayList<>();
        AuctionBody auctionBody = auctionRepository.findAuction(auctionId);
        for (Invitation invitation : newInvitations){
            for (Invitation auctionInvitation : auctionBody.getInvitations()){
                if (invitation.getEmail().equals(auctionInvitation.getEmail())){
                    resultList.add(invitation);
                }
            }
        }
        return resultList;
    }

    public void removeEmptyInvitations(List<Invitation> invitations)
    {
        if(invitations != null)
            invitations.removeIf(i -> i.getEmail().isEmpty());
    }

    public void removeExistingInvitations(List<Invitation> invitations, int auctionId)
    {
        if(invitations != null)
            invitations.removeIf(invitation -> invitationRepository.invitationExists(auctionId, invitation.getEmail()));
    }

}
