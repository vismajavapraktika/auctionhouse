package com.visma.seli.service.auction;

import com.visma.seli.config.properties.ApplicationProperties;
import com.visma.seli.server.data.access.repository.database.user.logedInUser.UserRepository;
import com.visma.seli.service.captions.CaptionService;
import com.visma.seli.service.mailing.NotificationService;
import com.visma.seli.service.user.UserBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MessagingService {


    @Autowired
    private NotificationService notificationService;
    @Autowired
    private CaptionService captionService;
    @Autowired
    private UserRepository userRepository;
    private static final int NO_NUMBER = -1;
    @Autowired
    private ApplicationProperties applicationProperties;

    public void notifyBuyerAboutBuyout(AuctionBody auction, String email) {
        notificationService.sendNotification(NO_NUMBER, captionService.getCaption("buyout.buyer.subject"),
                makeBuyoutBuyerMessageBody(auction.getId()), email);
    }

    private String makeBuyoutBuyerMessageBody(int auctionId) {
        String messageBody = captionService.getCaption("buyout.buyer.title") + "\n";
        messageBody = messageBody.concat(captionService.getCaption("buyout.buyer.message") + "\n");
        messageBody = messageBody.concat(captionService.getCaption("owner.message.link", applicationProperties.getUrl()) + auctionId);
        return messageBody;
    }

    public void notifyOwnerAboutBuyout(AuctionBody auction, UserBody user) {
        String email = userRepository.getUserById(auction.getOwnerId()).getEmail();
        notificationService.sendNotification(NO_NUMBER, captionService.getCaption("buyout.owner.subject"),
                makeBuyoutOwnerMessageBody(auction, user), email);
    }

    private String makeBuyoutOwnerMessageBody(AuctionBody auction, UserBody user) {
        String messageBody = captionService.getCaption("buyout.owner.title") + "\n";
        messageBody = messageBody.concat(captionService.getCaption("buyout.owner.message") + "\n");
        messageBody = messageBody.concat(captionService.getCaption("owner.message.winner.details") + "\n");
        messageBody = messageBody.concat(captionService.getCaption("owner.message.email") + " " + user.getEmail() + "\n");
        messageBody = messageBody.concat(captionService.getCaption("owner.message.phone") + " " + user.getPhone() + "\n");
        messageBody = messageBody.concat(captionService.getCaption("owner.message.link", applicationProperties.getUrl()) + auction.getId());
        return messageBody;
    }


}
