package com.visma.seli.service.auction;

public class AuctionPictureBody {
    private int auctionId;
    private byte[] photoBytes;
    private String photoName;

    public AuctionPictureBody() {
    }

    public AuctionPictureBody(int auctionId, byte[] photoBytes, String photoName) {
        this.auctionId = auctionId;
        this.photoBytes = photoBytes;
        this.photoName = photoName;
    }

    public int getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(int auctionId) {
        this.auctionId = auctionId;
    }

    public byte[] getPhotoBytes() {
        return photoBytes;
    }

    public void setPhotoBytes(byte[] photoBytes) {
        this.photoBytes = photoBytes;
    }

    public String getPhotoName() {
        return photoName;
    }

    public void setPhotoName(String photoName) {
        this.photoName = photoName;
    }

}
