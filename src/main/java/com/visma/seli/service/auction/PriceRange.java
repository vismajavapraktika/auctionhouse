package com.visma.seli.service.auction;

public class PriceRange {
    private String priceRange;
    public int minPrice;
    public int maxPrice;

    public PriceRange(String priceRange) {
        this.priceRange = priceRange;
    }

    public PriceRange(int minPrice, int maxPrice)
    {
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
    }

    public int getMinPrice() {
        return minPrice;
    }

    public int getMaxPrice() {
        return maxPrice;
    }

    public PriceRange invoke() {
        String[] minAndMax = priceRange.split(",");

        minPrice = Integer.parseInt(minAndMax[0]);
        maxPrice = Integer.parseInt(minAndMax[1]);
        return this;
    }
}