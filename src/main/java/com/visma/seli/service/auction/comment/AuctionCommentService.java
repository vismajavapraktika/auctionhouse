package com.visma.seli.service.auction.comment;

import com.visma.seli.server.data.access.repository.database.auction.AuctionRepository;
import com.visma.seli.server.data.access.repository.database.auction.comment.CommentRepository;
import com.visma.seli.server.data.access.repository.database.user.logedInUser.UserRepository;
import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.enums.UserEnum;
import com.visma.seli.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import java.time.LocalDateTime;
import java.util.UUID;


@Component
public class AuctionCommentService {
    @Autowired
    private CommentRepository commentRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private AuctionRepository auctionRepository;
    @Autowired
    private UserRepository userRepository;

    public void insertNewComment(Comment comment, int auctionId) {
        comment.setAuctionId(auctionId);
        comment.setCreateDate(LocalDateTime.now());

        comment.setUserId(userService.getLoggedInUserId());
        comment.setName(userService.getLoggedInUserNameAndSurname());

        commentRepository.insertNewComment(comment);
    }

    public void insertAnonymousComment(Comment comment, int auctionId, String userEmail) {
        comment.setAuctionId(auctionId);
        comment.setCreateDate(LocalDateTime.now());

        comment.setName(userEmail);

        commentRepository.insertNewComment(comment);
    }

    public String saveAuctionComment(BindingResult bindingResult, int id, Comment auctionComment) {
        if (bindingResult.hasErrors()) {
            return "html/auction/auctionView";
        }
        if (auctionComment.getText().trim().isEmpty()){
            return "redirect:/auction/" + id + "/";
        }
        AuctionBody auctionBody = auctionRepository.findAuction(id);
        if (auctionBody.getStatus().toString().equals("ACTIVE"))
            insertNewComment(auctionComment, id);
        return "redirect:/auction/" + id + "/";
    }

    public String saveAnonymousAuctionComment(BindingResult bindingResult, UUID userUuid, Comment auctionComment) {
        if (bindingResult.hasErrors()) {
            return "html/auction/anonymousAuctionView";
        }

        AuctionBody auction;
        String email;
        String redirectUrl;

        if (auctionRepository.checkIfAnonymousAuctionOwner(userUuid)) {
            auction = auctionRepository.findAnonymousAuction(userUuid, UserEnum.OWNER);
            email = auctionRepository.findAnonymousOwnerEmailById(auction.getOwnerId());
            redirectUrl = "redirect:/" + "anonymousOwnerAuction/" + userUuid;
        } else {
            auction = auctionRepository.findAnonymousAuction(userUuid, UserEnum.INVITED);
            email = userRepository.getInvitedUserEmailByUUID(userUuid);
            redirectUrl = "redirect:/" + "anonymousAuction/" + userUuid;
        }

        if (auction.getStatus().toString().equals("ACTIVE"))
            insertAnonymousComment(auctionComment, auction.getId(), email);

        return redirectUrl;
    }
}