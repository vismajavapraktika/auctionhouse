package com.visma.seli.service.auction.bid;

import java.math.BigDecimal;

public class AuctionBidContext {
    private BigDecimal lastBidAmount;
    private Boolean loggedInUserIsAuctionOwner;
    private Integer amountOfBids;
    private BigDecimal step;

    public BigDecimal getLastBidAmount() {
        return lastBidAmount;
    }

    public void setLastBidAmount(BigDecimal lastBidAmount) {
        this.lastBidAmount = lastBidAmount;
    }

    public Boolean getLoggedInUserIsAuctionOwner() {
        return loggedInUserIsAuctionOwner;
    }

    public void setLoggedInUserIsAuctionOwner(Boolean loggedInUserIsAuctionOwner) {
        this.loggedInUserIsAuctionOwner = loggedInUserIsAuctionOwner;
    }

    public Integer getAmountOfBids() {
        return amountOfBids;
    }

    public void setAmountOfBids(Integer amountOfBids) {
        this.amountOfBids = amountOfBids;
    }

    public BigDecimal getStep() {
        return step;
    }

    public void setStep(BigDecimal step) {
        this.step = step;
    }
}
