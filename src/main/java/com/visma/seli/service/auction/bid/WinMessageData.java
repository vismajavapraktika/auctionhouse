package com.visma.seli.service.auction.bid;

import com.visma.seli.service.enums.WinnerMessageResponseStatusEnum;

import java.time.LocalDateTime;

public class WinMessageData {

    private Bid bid;
    private WinnerMessageResponseStatusEnum responseStatus;
    private LocalDateTime expirationTime;


    public WinMessageData(Bid bid, WinnerMessageResponseStatusEnum responseStatus, LocalDateTime expirationTime) {
        this.bid = bid;
        this.responseStatus = responseStatus;
        this.expirationTime = expirationTime;
    }

    public Bid getBid() {
        return bid;
    }

    public void setBid(Bid bid) {
        this.bid = bid;
    }

    public WinnerMessageResponseStatusEnum getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(WinnerMessageResponseStatusEnum responseStatus) {
        this.responseStatus = responseStatus;
    }

    public LocalDateTime getExpirationTime() {
        return expirationTime;
    }

    public void setExpirationTime(LocalDateTime expirationTime) {
        this.expirationTime = expirationTime;
    }
}
