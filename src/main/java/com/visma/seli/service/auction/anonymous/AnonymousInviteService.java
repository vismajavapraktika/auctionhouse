package com.visma.seli.service.auction.anonymous;

import com.visma.seli.config.properties.ApplicationProperties;
import com.visma.seli.controllers.customValidators.CustomAuctionValidators.AuctionInvitationsValidator;
import com.visma.seli.server.data.access.repository.database.auction.AuctionRepository;
import com.visma.seli.server.data.access.repository.database.auction.invitations.InvitationRepository;
import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.Invitation;
import com.visma.seli.service.captions.CaptionService;
import com.visma.seli.service.enums.UserEnum;
import com.visma.seli.service.mailing.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;

import java.util.List;
import java.util.UUID;

@Service
public class AnonymousInviteService {

    @Autowired
    private AuctionRepository auctionRepository;
    @Autowired
    private InvitationRepository invitationRepository;
    @Autowired
    private AuctionInvitationsValidator auctionInvitationsValidator;
    @Autowired
    private AnonymousAuctionService anonymousAuctionService;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private CaptionService captionService;
    @Autowired
    private ApplicationProperties applicationProperties;
    @Transactional(propagation = Propagation.REQUIRED)
    public String anonymousAuctionInvitation(UUID uuid, UserEnum userEnum, Model model) throws Exception {

        AuctionBody auctionBody = anonymousAuctionService.findAuction(uuid, userEnum);

        auctionInvitationsValidator.validateInvitation(auctionBody);

        if (userEnum.equals(UserEnum.OWNER)) {
            model.addAttribute("id", auctionBody.getAuctionIdUrlOwner());
        } else if (userEnum.equals(UserEnum.INVITED)) {
            model.addAttribute("id", uuid);
        }

        return "html/auction/invite";
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public String anonymousAuctionInvitationSaved(UUID uuid, AuctionBody auctionBody, Model model) throws Exception {

        boolean isAnonymousAuctionOwner = auctionRepository.checkIfAnonymousAuctionOwner(uuid);

        AuctionBody auction = anonymousAuctionService.getAuction(isAnonymousAuctionOwner, uuid);

        List<Invitation> newInvitations = auctionBody.getInvitations();

        auctionInvitationsValidator.validateInvitation(auction, newInvitations);

        auctionBody.setTitle(auction.getTitle());
        invitationRepository.insertInvitedAnonymous(newInvitations, auction.getId());
        invitationRepository.insertInvitedAnonymousToAnonymousUserTable(newInvitations);

        sendNotificationsForAnonymousAuctionUsers(auctionBody, uuid);

        model.addAttribute("id", uuid);

        return auctionURL(isAnonymousAuctionOwner, uuid);
    }

    private String auctionURL(boolean isAnonymousAuctionOwner, UUID uuid) {
        if (isAnonymousAuctionOwner) {
            return "redirect:/anonymousOwnerAuction/" + uuid;
        } else {
            return "redirect:/anonymousAuction/" + uuid;
        }
    }


    public void sendNotificationsForAnonymousAuctionUsers(AuctionBody auctionBody, UUID uuid) {
        for (Invitation invitation : auctionBody.getInvitations()) {
            if(invitation.getInvitedUuid() != null)
                notificationService.sendNotification(invitation.getInvitedUuid(), captionService.getCaption("invite.anonymous.auction.subject") + auctionBody.getTitle(),
                        captionService.getCaption("invite.anonymous.auction.text", applicationProperties.getUrl()), invitation.getEmail());
        }
    }
}
