package com.visma.seli.service.auction.bid;

import com.github.mkopylec.recaptcha.validation.RecaptchaValidator;
import com.github.mkopylec.recaptcha.validation.ValidationResult;
import com.visma.seli.server.data.access.repository.database.auction.AuctionRepository;
import com.visma.seli.server.data.access.repository.database.auction.bids.BidRepository;
import com.visma.seli.service.auction.AuctionBody;
import com.visma.seli.service.auction.AuctionService;
import com.visma.seli.service.auction.anonymous.AnonymousAuctionService;
import com.visma.seli.service.captions.CaptionService;
import com.visma.seli.service.enums.AuctionCurrencyEnum;
import com.visma.seli.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Component
public class AuctionBidService {
    @Autowired
    private AuctionRepository auctionRepository;
    @Autowired
    private AuctionService auctionService;
    @Autowired
    private BidRepository bidRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private CaptionService captionService;
    @Autowired
    private RecaptchaValidator recaptchaValidator;
    @Autowired
    private AnonymousAuctionService anonymousAuctionService;

    @Transactional(propagation = Propagation.REQUIRED)
    public AuctionBidContext createBidContext(int auctionId) {
        AuctionBidContext auctionBidContext = new AuctionBidContext();

        auctionBidContext.setLastBidAmount(getLastBidAmount(auctionId));
        auctionBidContext.setAmountOfBids(auctionService.getAmountOfBids(auctionId));
        auctionBidContext.setLoggedInUserIsAuctionOwner(auctionService.isLoggedInUserAuctionOwner(auctionId));
        auctionBidContext.setStep(getBidStep(auctionId));

        return auctionBidContext;
    }

    public String highestBidForUser(int auctionId){
    	if(auctionService.isLoggedInUserAuctionOwner(auctionId))
    		return "";

    	List<Bid> bidList = bidRepository.findUsersHighestBids(auctionId);
        for(Bid bid : bidList){
            if (bid.getUserId() == userService.getLoggedInUserId()){
                return captionService.getCaption("users.highest.bid.on.auction") + " " + bid.getAmount();
            }
        }
        
        return captionService.getCaption("users.highest.bid.on.auction.no.bid");
    }

    private BigDecimal getBidStep(int auctionId) {
        AuctionBody auction = auctionRepository.findAuction(auctionId);
        return AuctionCurrencyEnum.valueOf(auction.getCurrency().toString().toUpperCase()).getStep();
    }

    private BigDecimal getLastBidAmount(int auctionId) {
        Bid lastBid = bidRepository.findLastBid(auctionId);
        if (lastBid == null) {
            return BigDecimal.ZERO;
        }

        return lastBid.getAmount();
    }

    public String saveAuctionBid(int id, HttpServletRequest request, BindingResult bindingResult, Bid auctionBid, final RedirectAttributes redirectAttributes) {
        String redirectOnErrorTo = "redirect:/auction/" + id + "/";
        ValidationResult result = recaptchaValidator.validate(request);
        if (auctionService.isAuctionEnded(auctionRepository.findAuction(id)) || auctionService.isLoggedInUserAuctionOwner(id)) {
            return redirectOnErrorTo;
        }
        if (bindingResult.hasErrors()) {
            addRedirectAttributes(auctionBid, bindingResult, redirectAttributes);
            return redirectOnErrorTo;
        }
        if (result.isFailure()) {
            bindingResult.addError(new ObjectError("request", "You must check \"I'm not a robot\" field"));
            addRedirectAttributes(auctionBid, bindingResult, redirectAttributes);
            return redirectOnErrorTo;
        }
        auctionService.insertNewBid(id, auctionBid);
        return "redirect:/auction/" + id + "/";
    }

    private void addRedirectAttributes(Bid auctionBid, BindingResult bindingResult,
                                       final RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.auctionBid", bindingResult);
        redirectAttributes.addFlashAttribute("auctionBid", auctionBid);
    }

    public String saveAnonymousAuction(UUID userUuid, HttpServletRequest request, BindingResult bindingResult, Bid auctionBid, RedirectAttributes redirectAttributes) {
        String redirectOnErrorTo = "redirect:/anonymousAuction/" + userUuid + "/";
        ValidationResult result = recaptchaValidator.validate(request);
        if (auctionService.isAuctionEnded(auctionRepository.findAuction(auctionRepository.findAuctionIdByUUID(userUuid)))) {
            return redirectOnErrorTo;
        }
        if (bindingResult.hasErrors()) {
            addRedirectAttributes(auctionBid, bindingResult, redirectAttributes);
            return redirectOnErrorTo;
        }
        if (result.isFailure()) {
            bindingResult.addError(new ObjectError("request", "You must check \"I'm not a robot\" field"));
            addRedirectAttributes(auctionBid, bindingResult, redirectAttributes);
            return redirectOnErrorTo;
        }
        anonymousAuctionService.insertNewAnonymousBid(userUuid, auctionBid);
        return "redirect:/anonymousAuction/" + userUuid + "/";
    }
}
