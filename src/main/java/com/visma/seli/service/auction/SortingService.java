package com.visma.seli.service.auction;

import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;

import static com.visma.seli.service.enums.SortingEnum.*;

@Component
public class SortingService {
    public void sortEndDate(List<AuctionBody> listAuctions) {
        listAuctions.sort(Comparator.comparing(AuctionBody::getEndDate));
    }

    public void sortBy(String sortCriteria, List<AuctionBody> filteredList) {
        if (sortCriteria.equals(BY_END_TIME.toString())) {
            sortEndDate(filteredList);
        }

        if (sortCriteria.equals(BY_START_TIME.toString())) {
            sortStartDate(filteredList);
        }

        if (sortCriteria.equals(BY_TITLE.toString())) {
            sortTitle(filteredList);
        }

        if (sortCriteria.equals(BY_PRICE.toString())) {
            sortStartingPrice(filteredList);
        }
    }

    private void sortStartDate(List<AuctionBody> listAuctions) {
        listAuctions.sort(Comparator.comparing(AuctionBody::getStartDate));
    }

    private void sortTitle(List<AuctionBody> listAuctions) {
        listAuctions.sort(Comparator.comparing(e -> e.getTitle().toLowerCase()));
    }

    private void sortStartingPrice(List<AuctionBody> listAuctions) {
        listAuctions.sort(Comparator.comparing(AuctionBody::getStartingPrice));
    }
}
