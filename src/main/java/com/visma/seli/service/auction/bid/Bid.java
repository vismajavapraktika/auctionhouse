package com.visma.seli.service.auction.bid;

import com.visma.seli.controllers.customValidators.CustomBiddingValidators.ValidBidding;

import javax.validation.constraints.DecimalMax;
import java.math.BigDecimal;


@ValidBidding
public class Bid {
    private int id;
    private int auctionId;
    private int userId;
    @DecimalMax(value = "100000000", message = "Bid cannot be more than 100000000")
    private BigDecimal amount;
    private int biddingQuantity;

    public Bid() {
    }

    public Bid(int id, int auctionId, int userId, BigDecimal amount, int biddingQuantity) {
        this.id = id;
        this.auctionId = auctionId;
        this.userId = userId;
        this.amount = amount;
        this.biddingQuantity = biddingQuantity;
    }

    public Bid(int auctionId, int userId, BigDecimal amount) {
        this.auctionId = auctionId;
        this.userId = userId;
        this.amount = amount;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(int auctionId) {
        this.auctionId = auctionId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public int getBiddingQuantity() {
        return biddingQuantity;
    }

    public void setBiddingQuantity(int biddingQuantity) {
        this.biddingQuantity = biddingQuantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Bid bid = (Bid) o;

        if (getId() != bid.getId()) return false;
        if (getAuctionId() != bid.getAuctionId()) return false;
        if (getUserId() != bid.getUserId()) return false;
        if (getBiddingQuantity() != bid.getBiddingQuantity()) return false;
        return getAmount() != null ? getAmount().equals(bid.getAmount()) : bid.getAmount() == null;

    }
}
