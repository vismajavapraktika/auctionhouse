package com.visma.seli.service.auction;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(value="session", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class AnonymousUserData {
    private Integer anonymousUserId;

    public Integer getAnonymousUserId() {
        return anonymousUserId;
    }

    public void setAnonymousUserId(Integer anonymousUserId) {
        this.anonymousUserId = anonymousUserId;
    }
}
