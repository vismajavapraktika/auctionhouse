package com.visma.seli.service.auction;

import com.visma.seli.server.data.access.repository.database.user.logedInUser.UserRepository;
import com.visma.seli.service.user.UserBody;
import com.visma.seli.service.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuctionsFilterService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    public List<AuctionBody> filterAuctions(List<AuctionBody> listAuctions, String auctionOwnerNameAndSurname, List<String> currencies/*, PriceRange priceRange*/) {
        List<AuctionBody> filteredAuctions = filterByCurrency(currencies,
                new ArrayList<>(listAuctions));

//        filteredAuctions = filterByPriceRange(filteredAuctions, priceRange.minPrice, priceRange.maxPrice);
        filteredAuctions = filterByNameAndSurname(auctionOwnerNameAndSurname,
                filteredAuctions);
        return filteredAuctions;
    }

    public void addPriceRange(AuctionFilter filter, String priceRange) {
        String[] minAndMax = priceRange.split(",");

        int minPrice = Integer.parseInt(minAndMax[0]);
        int maxPrice = Integer.parseInt(minAndMax[1]);

        filter.setMinPrice(minPrice);
        filter.setMaxPrice(maxPrice);
    }

    private List<AuctionBody> filterByCurrency(List<String> currencies, List<AuctionBody> allAuctions) {
        List<AuctionBody> auctionsFilteredByCurrency = new ArrayList<AuctionBody>();
        if (currencies != null && !currencies.isEmpty()) {
            for (String currency : currencies) {
                auctionsFilteredByCurrency.addAll(allAuctions.stream().filter(p -> p.getCurrency().toString().equals(currency))
                        .collect(Collectors.toList()));
            }
        } else
            return allAuctions;

        return auctionsFilteredByCurrency;
    }

    private List<AuctionBody> filterByPriceRange(List<AuctionBody> filteredAuctions, int minPrice, int maxPrice) {
        if (minPrice != 0) {
            filteredAuctions = filteredAuctions.stream().filter(p -> p.getStartingPrice().intValue() >= minPrice)
                    .collect(Collectors.toList());
        }

        if (maxPrice != 0) {
            filteredAuctions = filteredAuctions.stream().filter(p -> p.getStartingPrice().intValue() <= maxPrice)
                    .collect(Collectors.toList());
        }
        return filteredAuctions;
    }

    private List<AuctionBody> filterByNameAndSurname(String auctionOwnerNameAndSurname, List<AuctionBody> filteredAuctions) {
        String[] querySplittedBySpace = auctionOwnerNameAndSurname.split(" ");
        if (auctionOwnerNameAndSurname != null && !auctionOwnerNameAndSurname.isEmpty()) {
            filteredAuctions = filteredAuctions.stream()
                .filter(p -> {
                    UserBody auctionOwner = userRepository.getUserById(p.getOwnerId());
                    return (auctionOwner != null && userService.nameOrSurnameContainsQueryWords(querySplittedBySpace, auctionOwner.getName(), auctionOwner.getSurname()));
                }).collect(Collectors.toList());
        }
        return filteredAuctions;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }
}
