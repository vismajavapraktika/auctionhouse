package com.visma.seli.service.auction;

public class AuctionFilter {

    private String currency;
    private int minPrice;
    private int maxPrice;
    private String auctionOwner;

    private String priceRange;

    public String getPriceRange() {
        return priceRange;
    }

    public void setPriceRange(String priceRange) {
        this.priceRange = priceRange;
    }

    public int getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(int minPrice) {
        this.minPrice = minPrice;
    }

    public int getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(int maxPrice) {
        this.maxPrice = maxPrice;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getAuctionOwner() {
        return auctionOwner;
    }

    public void setAuctionOwner(String auctionOwner) {
        this.auctionOwner = auctionOwner;
    }
}
