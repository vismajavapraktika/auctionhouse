package com.visma.seli.service.user.rating;

import com.visma.seli.server.data.access.repository.database.user.rating.UserRatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class UserRatingService {

    @Autowired
    private UserRatingRepository userRatingRepository;
    private static final Logger logger = Logger.getLogger(Class.class.getName());


    public void insertOrUpdate(float rating, int rater, int ratee, int auctionId) { // TODO: 2016-12-28 Change rating to only insert no update needed
        try {
            if  (userRatingRepository.checkIfNotRated(rater,ratee,auctionId))
                userRatingRepository.insertRating(rating, rater, ratee,auctionId);
            else
                userRatingRepository.updateRating(rating,rater,ratee,auctionId);
        } catch (DuplicateKeyException ex) {
            logger.log(Level.SEVERE,ex.toString(),ex);
        }
    }
}
