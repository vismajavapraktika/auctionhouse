package com.visma.seli.service.user;


import javax.validation.constraints.Pattern;

public class AnonymousUser {
    private int id;
    @Pattern(regexp = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", message = "Please enter valid email")
    private String email;
    @Pattern(regexp = "^(?:[0-9]{8}|)$", message = "Phone number must have 8 digits")
    private String phone;

    public AnonymousUser() {
    }

    public AnonymousUser(int id, String email, String phone) {
        this.id = id;
        this.email = email;
        this.phone = phone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
