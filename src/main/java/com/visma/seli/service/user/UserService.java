package com.visma.seli.service.user;

import com.visma.seli.server.data.access.repository.database.user.logedInUser.UserRepository;
import com.visma.seli.server.login.google.GoogleAuth;
import com.visma.seli.server.login.google.GoogleLoginInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private GoogleAuth googleAuth;

    public boolean isGoogleUserRegistered(final GoogleLoginInfo googleUser) {
        return userRepository.getUserByEmail(googleUser.getEmails().iterator().next().getValue()) != null;
    }

    public int getLoggedInUserId() {
        return userRepository.getUserByEmail(getLoggedInUserEmail()).getId();
    }

    public String getLoggedInUserEmail() {

        if (SecurityContextHolder.getContext() == null
                || SecurityContextHolder.getContext().getAuthentication() == null) {
            return "anonymousUser";
        }

        final GoogleLoginInfo googleUser = googleAuth.getGoogleUser();

        if (googleUser == null)
            return SecurityContextHolder.getContext().getAuthentication().getName();
        else if(userRepository.getUserByEmail(googleUser.getEmails().iterator().next().getValue()) == null) {
            return "anonymousUser";
        }else{
            return googleUser.getEmails().iterator().next().getValue();
        }
    }

    public String getLoggedInUserNameAndSurname(){
        if (SecurityContextHolder.getContext() == null
                || SecurityContextHolder.getContext().getAuthentication() == null) {
            return "anonymousUser";
        }

        final GoogleLoginInfo googleUser = googleAuth.getGoogleUser();

        if (googleUser == null) {
            return SecurityContextHolder.getContext().getAuthentication().getName();
        }
        else if(userRepository.getUserByEmail(googleUser.getEmails().iterator().next().getValue()) == null) {
            return "anonymousUser";
        }else {
            return getLoggedInUserName() + " " + getLoggedInUserSurname();
        }
    }

    private String getLoggedInUserName(){
        final GoogleLoginInfo googleUser = googleAuth.getGoogleUser();
        return googleUser.getName().getGivenName();
    }

    private String getLoggedInUserSurname(){
        final GoogleLoginInfo googleUser = googleAuth.getGoogleUser();
        return googleUser.getName().getFamilyName();
    }

    public boolean nameOrSurnameContainsQueryWords(String[] queryWords, String userName, String surname) {
        for(String qWord : queryWords) {
            if(qWord.length() < 3)
                continue;
            if (userName.toLowerCase().contains(qWord.toLowerCase()) || surname.toLowerCase().contains(qWord.toLowerCase())) {
                return true;
            }
        }
        return false;
    }
}
