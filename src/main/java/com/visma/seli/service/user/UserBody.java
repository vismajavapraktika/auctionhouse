package com.visma.seli.service.user;

import com.visma.seli.controllers.customValidators.CustomAuctionValidators.ValidEmail;
import com.visma.seli.controllers.customValidators.CustomUserValidators.TwoFieldsMatches;
import com.visma.seli.service.enums.UserTypes;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.Collection;



@TwoFieldsMatches.List({
        @TwoFieldsMatches(first = "password", second = "passwordConfirm", third = "userType")})
public class UserBody implements Authentication {
    private int id;
    @Pattern(regexp = "[a-zA-Z]{2,20}", message = "Your name must be between 2 and 20 letters long")
    private String name;
    @Pattern(regexp = "[a-zA-Z]{2,20}", message = "Your surname must be between 2 and 20 letters long")
    private String surname;
    @Pattern(regexp = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", message = "Please enter valid email")
    @NotNull
    @ValidEmail
    private String email;
    @Pattern(regexp = "^[0-9]{8}$", message = "Phone number must have 8 digits")
    private String phone;
    private String password;
    private String passwordConfirm;
    private int numberOfWonAuctions;
    private UserTypes userType;

    public UserBody(int id, String name, String surname, String email, String phone, String password) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.numberOfWonAuctions = 0;
    }
    public UserBody(int id, String name, String surname, String email, String phone, UserTypes userType) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.phone = phone;
        this.userType = userType;
        this.numberOfWonAuctions = 0;
    }

    public UserBody() {
    }

    public UserBody(UserTypes userType){
        this.userType = userType;
    }

    public UserBody(int id, String name, String surname, String email, String phone, String password, UserTypes userType) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.numberOfWonAuctions = 0;
        this.userType = userType;
    }

    public UserTypes getUserType() {
        return userType;
    }

    public void setUserType(UserTypes userType) {
        this.userType = userType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumberOfWonAuctions() {
        return numberOfWonAuctions;
    }

    public void setNumberOfWonAuctions(int numberOfWonAuctions) {
        this.numberOfWonAuctions = numberOfWonAuctions;
    }

    public int increaseNumberOfWonAuctions() {
        return ++this.numberOfWonAuctions;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public Object getCredentials() {
        return null;
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return null;
    }

    @Override
    public boolean isAuthenticated() {
        return false;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserBody userBody = (UserBody) o;

        if (getId() != userBody.getId()) return false;
        if (getNumberOfWonAuctions() != userBody.getNumberOfWonAuctions()) return false;
        if (getName() != null ? !getName().equals(userBody.getName()) : userBody.getName() != null) return false;
        if (getSurname() != null ? !getSurname().equals(userBody.getSurname()) : userBody.getSurname() != null)
            return false;
        if (getEmail() != null ? !getEmail().equals(userBody.getEmail()) : userBody.getEmail() != null) return false;
        if (getPhone() != null ? !getPhone().equals(userBody.getPhone()) : userBody.getPhone() != null) return false;
        if (getPassword() != null ? !getPassword().equals(userBody.getPassword()) : userBody.getPassword() != null)
            return false;
        return getPasswordConfirm() != null ? getPasswordConfirm().equals(userBody.getPasswordConfirm()) : userBody.getPasswordConfirm() == null;

    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getSurname() != null ? getSurname().hashCode() : 0);
        result = 31 * result + (getEmail() != null ? getEmail().hashCode() : 0);
        result = 31 * result + (getPhone() != null ? getPhone().hashCode() : 0);
        result = 31 * result + (getPassword() != null ? getPassword().hashCode() : 0);
        result = 31 * result + (getPasswordConfirm() != null ? getPasswordConfirm().hashCode() : 0);
        result = 31 * result + getNumberOfWonAuctions();
        return result;
    }
}
