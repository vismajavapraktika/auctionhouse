package com.visma.seli.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class PassHasher {

    @Autowired
    private PasswordEncoder passwordEncoder;

    public void hashThePass(UserBody user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
    }
}