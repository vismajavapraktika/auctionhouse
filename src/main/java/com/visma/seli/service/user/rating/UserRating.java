package com.visma.seli.service.user.rating;

public class UserRating {
    int id;
    int raterId;
    int rateeId;
    float rating;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRaterId() {
        return raterId;
    }

    public void setRaterId(int raterId) {
        this.raterId = raterId;
    }

    public int getRateeId() {
        return rateeId;
    }

    public void setRateeId(int rateeId) {
        this.rateeId = rateeId;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

}