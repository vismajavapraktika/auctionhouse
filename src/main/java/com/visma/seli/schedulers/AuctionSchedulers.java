package com.visma.seli.schedulers;

import com.visma.seli.server.data.access.repository.database.NotificationsRepository;
import com.visma.seli.server.data.access.repository.database.auction.AuctionRepository;
import com.visma.seli.server.data.access.repository.database.auction.bids.BidRepository;
import com.visma.seli.server.data.access.repository.database.auction.invitations.InvitationRepository;
import com.visma.seli.server.data.access.repository.database.pictures.PictureRepository;
import com.visma.seli.server.data.access.repository.database.user.logedInUser.UserRepository;
import com.visma.seli.service.Notifications;
import com.visma.seli.service.auction.*;
import com.visma.seli.service.captions.CaptionService;
import com.visma.seli.service.enums.AuctionStatusEnum;
import com.visma.seli.service.mailing.NotificationService;
import com.visma.seli.service.mailing.SendEmailToAnonymousUsers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

import static com.visma.seli.service.enums.AuctionStatusEnum.ACTIVE;
import static com.visma.seli.service.enums.AuctionStatusEnum.ENDED;
import static com.visma.seli.service.enums.AuctionTypeEnum.PRIVATE;
import static com.visma.seli.service.enums.AuctionTypeEnum.PUBLIC;
import static java.util.Arrays.asList;

@Component
public class AuctionSchedulers {

    private static final int ONE_HOUR = 3600000, ONE_WEEK = 7, TWELVE_HOURS = 43200000;
    private static final int NO_NUMBER = -1;
    @Autowired
    private SendEmailToAnonymousUsers sendEmailToAnonymousUsers;
    @Autowired
    private AuctionRepository auctionRepository;
    @Autowired
    private AuctionService auctionService;
    @Autowired
    private BidRepository bidRepository;
    @Autowired
    private WonAuctionService wonAuctionService;
    @Autowired
    private NotificationsRepository notificationsRepository;
    @Autowired
    private NotificationService notificationService;
    @Autowired
    private PictureRepository pictureRepository;
    @Autowired
    private CaptionService captionService;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private InvitationRepository invitationRepository;

    @Scheduled(fixedDelay = 1000)
    @Transactional(propagation = Propagation.REQUIRED)
    public void sendInfoWhenAuctionEnded() {
        List<AuctionBody> auctionBodyList = auctionRepository
                .findAllAuctionsWithTypesAndStatuses(asList(PRIVATE, PUBLIC), asList(ACTIVE));

        for (AuctionBody auction : auctionBodyList) {
            if (!auction.getIsAnonymous() && auctionService.isAuctionEnded(auction)) {
                auctionRepository.changeAuctionStatus(ENDED, auction.getId());
                auction.setStatus(ENDED);
                notificationsRepository.notificationChangeAfterAuctionStatusChange(auction);
                auctionService.insertAuctionWinners(bidRepository.findUsersHighestBids(auction.getId()));
                notificationService.sendNotification(NO_NUMBER, captionService.getCaption("notify.owner.auction.ended.subject"),
                        captionService.getCaption("notify.owner.auction.ended.text") + " " + auction.getTitle(),
                        userRepository.getUserById(auction.getOwnerId()).getEmail());
                wonAuctionService.doEndedAutionsMessaging();
            }
        }
    }

    @Scheduled(fixedDelay = 60000)
    @Transactional
    public void sendInfoWhenAnonymousAuctionEnded() {
        List<AuctionBody> auctionList = auctionRepository.findAll();
        if (!auctionList.isEmpty())
            for (AuctionBody auction : auctionList) {
                if (auction.getIsAnonymous() && !auction.getStatus().equals(AuctionStatusEnum.ENDED))
                    sendEmailToAnonymousUsers.sendEmailToAnonymousUsers(auction);
            }
    }

    @Scheduled(fixedDelay = ONE_HOUR)
    @Transactional
    public void checkTemporaryPictures() {
        List<Picture> temporaryPictures = pictureRepository.getTemporaryPicturesList();
        temporaryPictures.stream().filter(picturesList -> picturesList.getInsertDate().plusHours(24).isBefore(LocalDateTime.now())).forEach(picturesList -> {
            pictureRepository.deleteTemporaryPicture(picturesList.getPictureId());
        });
    }

    @Scheduled(fixedDelay = TWELVE_HOURS)
    @Transactional
    public void deleteUnusedNotifications() {
        List<Notifications> listOfAllNotifications = notificationsRepository.getAllNotifications();
        for (Notifications notification : listOfAllNotifications) {
            if (userRepository.getUserByEmail(notification.getEmail()) == null) {
                if (notification.getInsertDate().plusDays(ONE_WEEK).isBefore(LocalDateTime.now())) {
                    notificationsRepository.deleteNotification(notification.getId());
                }
            }
        }
    }

    @Scheduled(fixedDelay = TWELVE_HOURS)
    @Transactional
    public void deleteInvitationsOfUncreatedUsers() {
        List<Invitation> listOfAllInvitations = invitationRepository.getAllInvitations();
        for (Invitation invitation : listOfAllInvitations) {
            if (userRepository.getUserByEmail(invitation.getEmail()) == null) {
                if (invitation.getInsertDate().plusDays(ONE_WEEK).isBefore(LocalDateTime.now())) {
                    invitationRepository.deleteInvitation(invitation.getId());
                }
            }
        }
    }
}
