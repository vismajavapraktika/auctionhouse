function isEmpty(el) {
    var result = !$.trim(el.html());
    return result;
}

function showPicturesList() {
    if (!isEmpty($('#picture-name-list'))) {
        var pictureList = document.getElementById("added-pictures");
        pictureList.style.visibility = 'visible';
    }
}

function removePicture(i) {
    var pictureList = JSON.parse(localStorage.getItem(pictureLocalStorage));
    var pictureUUIDFromList = pictureList[i].pictureUUID;
    $.ajax({
        url: "/create-auction/remove-picture", data: {pictureUUID: pictureUUIDFromList}, success: function (result) {

            $("#div1").html(result);
        }
    });
    pictureList.splice(i, 1);
    localStorage.setItem(pictureLocalStorage, JSON.stringify(pictureList));
}

var lastInputField;

function addNewPicture() {
    lastInputField = $('#all-pictures input:last');
    $(lastInputField).on('change', function () {
        addNewPicture();
    });
    var input = document.createElement("input");
    input.type = "file";
    input.accept = ".jpg, .png, .jpeg";
    $(input).on('change', function () {
        addNewPicture();
    });
    $(input).insertAfter(lastInputField);
    lastInputField.hide();
    $('#all-pictures :input').each(function (index) {
        $(this).attr('name', 'pictureFiles[' + index + ']');
    });

    var files = document.querySelectorAll('input[type=file]');//.files;
    var file;
    var lengthOfFiles = files.length;
    lengthOfFiles = lengthOfFiles - 2;
    file = files[lengthOfFiles].files[0];
    var data = new FormData();
    data.append('picture', file);
    savePicture(data);
    showPicturesList();

}

function createInputPictureDeleteLink(input) {
    var a = document.createElement("a");
    a.title = 'delete';
    a.text = 'x';
    var li = document.createElement("li");
    $(li).text(input.val().replace(/C:\\fakepath\\/i, ''));
    $(li).append(a);

    var ul = document.createElement('ul');
    ul.appendChild(li);
    document.getElementById("picture-name-list").appendChild(ul);

    $(a).on('click', function () {
        removePicture($("#picture-name-list a").index(a));
        $(ul).remove();
        $('#all-pictures :input').get(0).style.display = 'none';
        $(input).remove();
        $('#all-pictures :input').each(function (index) {
            $(this).attr('name', 'pictureFiles[' + index + ']');
        });
    });

    return li;
}

var pictureLocalStorage = "pictureLocalStorageList";

function savePicture(file) { //ToDo clean localStorage
    $.ajax({
        type: 'POST',
        url: "/create-auction/picture-check",
        data: file,
        contentType: false,
        processData: false,
        success: function (result) {
            if (result != "") {
                var pictureList = getPictureList();
                pictureList.push(result);
                localStorage.setItem(pictureLocalStorage, JSON.stringify(pictureList));
                var li = createInputPictureDeleteLink(lastInputField);
                showPicturesList();
            } else {
                alert("picture must have jpg, png or jpeg format")
            }
        }
    });
}

function getPictureList() {
    var pictureList = JSON.parse(localStorage.getItem(pictureLocalStorage));
    return pictureList = pictureList == null ? [] : pictureList;
}

function pictureNamePrint() {
    var pictureList = getPictureList();

    if (pictureList != null) {
        var pictureNameListDiv = document.getElementById("picture-name-list");
        pictureList.forEach(function (picture) {

            var a = document.createElement("a");
            a.title = 'delete';
            a.text = 'x';
            var li = document.createElement("li");
            $(li).text(picture.pictureName);
            $(li).append(a);

            var ul = document.createElement('ul');
            ul.appendChild(li);

            document.getElementById("picture-name-list").appendChild(ul);

            $(a).click(function () {
                removePicture($("#picture-name-list a").index($(this)));
                $(ul).remove();
            });

        });
    }
    invitation.showInvList();
    showPicturesList();
}

function DoOnSubmit() {
    document.getElementById("uuid-List").value = localStorage.getItem(pictureLocalStorage);
}


function insertUUIDList() {
    var listOfPhotosUUID = JSON.parse(localStorage.getItem(pictureLocalStorage));
    var uuidListSelect = document.getElementById("uuidList");
    var option = document.createElement("option");
    var tmpPictureUUIDList = [];
    if (listOfPhotosUUID != null) {
        for (var i = 0; i < listOfPhotosUUID.length; i++) {
            tmpPictureUUIDList.push(listOfPhotosUUID[i].pictureUUID);
        }
        option.innerHTML = tmpPictureUUIDList.join();
        uuidListSelect.appendChild(option);
    }
}

function removeEmptyFields() {
    $('#all-pictures input:last').remove();
    $('#all-emails input:last').remove();
}

module.exports = {
    showPicturesList: function () {
        showPicturesList();
    },
    addNewPicture: function () {
        addNewPicture();
    },
    pictureNamePrint: function () {
        pictureNamePrint();
    },
    DoOnSubmit: function () {
        DoOnSubmit();
    },
    insertUUIDList: function () {
        insertUUIDList();
    },
    removeEmptyFields: function () {
        removeEmptyFields();
    },
};
