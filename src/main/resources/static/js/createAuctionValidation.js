function validateDate(email) {
    let re = /^20[0-9]{2}-(0[1-9])|(1[0-2])-([0-3][0-9]) [0-9]{2}:[0-9]{2}$/g;
    return re.test(email);
}

function validateTitle(title) {
    let re = /^([a-zA-Z0-9_\s-]){1,100}$/g;
    return re.test(title);
}

function validateDescription(title) {
    let re = /^([a-zA-Z0-9_\s-]){1,999}$/g;
    return re.test(title);
}

function validateStartingPrice(title) {
    let re = /^([0-9]){1,100000000}$/g;
    return re.test(title);
}

function validateStartDate(startDate) {
    let now = new Date();
    let startDateNow = new Date(startDate);
    let result = (startDateNow - now > 60000 * 60 * 24 * 7 || (startDateNow - now < 0) && (now - startDateNow > 60000 * 15));
    return result;
}

function validateEndDate(endDate, startDate) {
    let newStartDate = new Date(startDate);
    let newEndDate = new Date(endDate);
    let result = (newEndDate - newStartDate < (60000 * 60 * 24) || (newEndDate - newStartDate) > (60000*60*24*14)) ;
    return result;
}

$( document ).ready(function() {
    $("#title").blur(function () {
        if(!validateTitle($(this).val())){
            if($('#titleError').length == 0) {
                document.getElementById('title').setAttribute('class','field-error');
                $('<div class="error-message" style="color: red" id="titleError">Title must be between 1 and 100 characters</div>').insertAfter('#title');
            }
        }
        else {
            $('#titleError').remove();
            document.getElementById('title').setAttribute('class','');
        }
    });

    $("#description").blur(function () {
        if(!validateDescription($(this).val())){
            if($('#descriptionError').length == 0) {
                document.getElementById('description').setAttribute('class', 'field-error');
                $('<div class="error-message1" style="color: red" id="descriptionError">Description must be between 1 and 999 characters</div>').insertAfter('#description');
            }
        }
        else {
            $('#descriptionError').remove();
            document.getElementById('description').setAttribute('class','');
        }
    });

    $("#startingPrice").blur(function () {
        if(!validateStartingPrice($(this).val())){
            if($('#startingPriceError').length == 0) {
                document.getElementById('startingPrice').setAttribute('class', 'field-error');
                $('<div class="error-message1" style="color: red" id="startingPriceError">Please enter starting price<br/>KUDOS can only be integer' +
                    '<br/>From 1 to 100000000</div>').insertAfter('#startingPrice');
            }
        }
        else {
            $('#startingPriceError').remove();
            document.getElementById('startingPrice').setAttribute('class','');
        }
    });

	$("#startdate").blur(function(){
		if(!validateDate($(this).val())) {
            if ($('#startDateError').length == 0) {
                document.getElementById('startdate').setAttribute('class', 'form-control field-error');
                $('<div class="error-message" style="color:red" id="startDateError">Not a valid date format</div>').insertAfter('#datetimepicker2');
            }
        }
        else if (validateStartDate($(this).val())){
            $('#startDateError').remove();
            document.getElementById('startdate').setAttribute('class', 'form-control field-error');
            let timeNow = new Date();
            let weekFromNow = new Date(timeNow);
            weekFromNow.setDate(weekFromNow.getDate() + 7);
            $('<div class="error-message" style="color:red" id="startDateError">Start time must be between ' + timeNow.toDateString() + ' and ' + weekFromNow.toDateString() +
                '</div>').insertAfter('#datetimepicker2');
        }
        else {
            $('#startDateError').remove();
            document.getElementById('startdate').setAttribute('class','form-control');
        }
	});
	
	$("#enddate").blur(function(){
		if(!validateDate($(this).val())) {
            if ($('#endDateError').length == 0) {
                document.getElementById('enddate').setAttribute('class', 'form-control field-error');
                $('<div class="error-message" style="color:red" id="endDateError">Not a valid date format</div>').insertAfter('#datetimepicker3');
            }

        }
        else if (validateEndDate($(this).val(), $('#startdate').val())){
            $('#endDateError').remove();
            document.getElementById('enddate').setAttribute('class', 'form-control field-error');
            $('<div class="error-message" style="color:red" id="endDateError">End time must be 24 hours more then start date and can\'t be more then 2 weeks from start date' +
                '</div>').insertAfter('#datetimepicker3');
        }
        else {
            $('#endDateError').remove();
            document.getElementById('enddate').setAttribute('class','form-control');
        }
	});
});
