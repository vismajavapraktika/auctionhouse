function allowToBidForQuantityChangedAnonymous() {
    var allowToBidForQuantityInput = $('#allowToBidForQuantityInput');
    if (allowToBidForQuantityInput.prop('checked')) {
        $('#ItemQuantityRow').show();
    } else {
        $('#ItemQuantityRow').hide();
        document.getElementById("itemQuantity").value = 1;
    }
}

function changeStartingPriceStepsAnonymous() {
    var currencyType = $("#changeOnType").val();
    var tempStartingPriceValue = $('#startingPrice').val();

    if (document.getElementById("startingPrice") != null) {
        document.getElementById("startingPrice").removeAttribute("value");
        document.getElementById("startingPrice").value = tempStartingPriceValue;
    }
    if (currencyType === "KUDOS") {
        document.getElementById('startingPrice').step = '1';
    } else {
        document.getElementById('startingPrice').step = '0.01';
    }
}

var calculateTotalPriceAnonymous = function () {
    document.getElementById("totalPrice").value = (document
        .getElementById("startingPrice").value * 1 + document
        .getElementById("startingPrice").value
    * (document.getElementById("itemQuantity").value - 1))
};


module.exports = {
    changeStartingPriceStepsAnonymous: function () {
        changeStartingPriceStepsAnonymous();
    },
    calculateTotalPriceAnonymous: function () {
        calculateTotalPriceAnonymous();
    },
    allowToBidForQuantityChangedAnonymous: function () {
        allowToBidForQuantityChangedAnonymous();
    },
};