
setInterval(getNotificationNumber, 10000);

function getNotificationNumber() {
    $.ajax({
        url: "/notify-user-about-won-auctions/", success: function (result) {
            $("#notification_badge").text(result);
            if (result == "") {
                if (document.getElementById("notification_badge") != null) {
                    document.getElementById("notification_badge").style.visibility = "hidden";
                }
            } else {
                if (document.getElementById("notification_badge") != null) {
                    document.getElementById("notification_badge").style.visibility = "visible";
                }
            }
        }
    });
}
function getNotificationList() {
    $.ajax({
        url: "/notifications/getList/", success: function (result) {
            var notificationsBodyDiv = document.getElementById("notificationDiv");
            notificationsBodyDiv.setAttribute('style','height:300px;'); //
            if (notificationsBodyDiv.hasChildNodes()){
                $("#notificationDiv").empty();
            }
            var notificationPanelDiv = document.createElement('div');
            notificationPanelDiv.setAttribute('class','notification-body; display: block;');
            var notificationEachDiv = document.createElement('div');
            
            if(result == "")
        	{
            	addNoNewNotificationsMessage(notificationPanelDiv);
                notificationsBodyDiv.setAttribute('style','height:50px');
            }
            
            for (var i = 0; i < result.length; i++) {
                var notificationClassRow = document.createElement('div');
                    notificationClassRow.setAttribute('class', 'notification-listing');
                var notificationClassCol = document.createElement('div');
                var notificationImgSpan = document.createElement('span');
                if (result[i].type.toString() === 'INVITATION') {
                    notificationImgSpan.setAttribute('class', 'spanNotification vismaicon vismaicon-add2-circle');
                }else if (result[i].type.toString() === 'DELETED') {
                    notificationImgSpan.setAttribute('class', 'spanNotification vismaicon vismaicon-delete');
                }else if (result[i].type.toString() === 'AUCTION_WINNER') {
                    notificationImgSpan.setAttribute('class', 'spanNotification vismaicon vismaicon-phone');
                }else if (result[i].type.toString() === 'UPDATED'){
                    notificationImgSpan.setAttribute('class', 'spanNotification vismaicon vismaicon-upload');
                }else if (result[i].type.toString() === 'OVERBID'){
                    notificationImgSpan.setAttribute('class', 'spanNotification vismaicon vismaicon-arrow-up-circle');
                }
                var notificationA = document.createElement('a');
                notificationA.innerHTML = result[i].title;
                notificationA.setAttribute('onclick', "notification.redirectURL(" + i + "); ");
                notificationClassCol.setAttribute('class', 'col-notification');
                notificationClassCol.appendChild(notificationImgSpan);
                notificationClassCol.appendChild(notificationA);
                notificationClassRow.appendChild(notificationClassCol);
                notificationEachDiv.appendChild(notificationClassRow);
                notificationPanelDiv.appendChild(notificationEachDiv);
            }
            notificationsBodyDiv.appendChild(notificationPanelDiv);
        }
    })
}

function addNoNewNotificationsMessage(notificationPanelDiv){
	var notificationClassCol = document.createElement('div');
	
	var notificationA = document.createElement('ul');
	notificationA.setAttribute('style','padding-top: 15px;');
	notificationA.innerHTML = "No unread notifications";
	notificationClassCol.setAttribute('class', 'col-notification');
	notificationClassCol.appendChild(notificationA);
	notificationPanelDiv.appendChild(notificationClassCol);
}

/**
 * @return {string}
 */
function RedirectURL(i) {
    var resultId = 0;
    $.ajax({
        url: "/notifications/getList/", data: {notificationId: i}, success: function (result) {
            resultId = result[i]["notificationId"];
            window.location.href = "/auction/" + resultId + "/";
            ChangeNotification(result[i]["id"]);
        }
    });
}

function ChangeNotification(i) {
    $.ajax({
        url: "/notifications/getList/changeNotificationStatus/",
        data: {notificationId: i},
        success: function (result) {

        }
    })
}

$(document).ready(function () {

    // ANIMATEDLY DISPLAY THE NOTIFICATION COUNTER.
    $('#noti_Counter')
        .css({opacity: 0})
        .text('7')              // ADD DYNAMIC VALUE (YOU CAN EXTRACT DATA FROM DATABASE OR XML).
        .css({top: '-10px'})
        .animate({top: '-2px', opacity: 1}, 500);

    $('#noti_Button').click(function () {

//                // TOGGLE (SHOW OR HIDE) NOTIFICATION WINDOW.
        $('#notifications').fadeToggle('fast', 'linear', function () {
            // if ($('#notifications').is(':hidden')) {
            // }
        });
//
        $('#noti_Counter').fadeOut('slow');                 // HIDE THE COUNTER.

        return false;
    });

    // HIDE NOTIFICATIONS WHEN CLICKED ANYWHERE ON THE PAGE.
    $(document).click(function () {
        $('#notifications').hide();

        // CHECK IF NOTIFICATION COUNTER IS HIDDEN.
        if ($('#noti_Counter').is(':hidden')) {
            // CHANGE BACKGROUND COLOR OF THE BUTTON.
            $('#noti_Button').css('background-color', '#2E467C');
        }
    });

    $('#notifications').click(function () {
        return false;       // DO NOTHING WHEN CONTAINER IS CLICKED.
    });
});


module.exports = {
    getNotificationNumber: function () {
        getNotificationNumber();
    },
    getNotificationList: function () {
        getNotificationList();
    },
    redirectURL: function (i) {
        RedirectURL(i);
    },
};