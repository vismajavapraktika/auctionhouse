function highestBid(auctionId,auctionViewBidsText,auctionViewHihestBidderText) {

    $.ajax({
        url: "/auction/refresh-bid-information/",
        data: {id: auctionId},
        success: function (result) {
            if  (result[0] == ""){
                document.getElementById("highest-bid").innerHTML = "0";
            }else {
                if (document.getElementById("highest-bid") != null)
                    document.getElementById("highest-bid").innerHTML = result[0];
            }
            if (document.getElementById("amount-of-bids") != null)
                document.getElementById("amount-of-bids").innerHTML = '[' + result[1] + ' ' + auctionViewBidsText + ']';
            if (result[2] != ("")) {
                if (document.getElementById("highest-bidder-information") != null)
                    document.getElementById("highest-bidder-information").innerHTML = auctionViewHihestBidderText + ' ' + result[2];
            }
            if (document.getElementById("highest-user-bid") != null)
                document.getElementById("highest-user-bid").innerHTML = result[3];
        }
    });
}

function trimComments(comment){
    document.getElementById('addComment').innerHTML = comment.trim();
}


module.exports = {
    highestBid: function (auctionId,auctionViewBidsText,auctionViewHihestBidderText) {
        highestBid(auctionId,auctionViewBidsText,auctionViewHihestBidderText)
    },
    trimComments: function (comment) {
        trimComments(comment);
    }
};
