function timer() {

	count = count - 1;
	if (count == -1) {
		clearInterval(counter);
		return;
	}

	var seconds = count % 60;
	var minutes = Math.floor(count / 60);
	var hours = Math.floor(minutes / 60);
	var days = Math.floor(hours / 24);
	hours = hours - (days * 24);
	minutes %= 60;
	hours %= 60;

	if (document.getElementById("auction-time-remaining")) {
		if (days > 2) {
			document.getElementById("auction-time-remaining").innerHTML = "Time left: " + (days + 1) + " days";
		} else {
			document.getElementById("auction-time-remaining").innerHTML = "Time left: " + days + " days " + hours + " hours " + minutes + " minutes and " + seconds + " seconds left";
		}
	}
}

module.exports = {
	timer: function () {
		timer();
    }
};