function defaultPictureSize(pictureIn) {
    if (pictureIn != null) {
        var pictureHeight = pictureIn.naturalHeight;
        var pictureWidth = pictureIn.naturalWidth;

        if (pictureHeight > pictureWidth) {
            pictureIn.style.height = "400px";
        }
        else if (pictureWidth > pictureHeight) {
            pictureIn.style.width = "400px";
            if (pictureHeight < 400) {
                var topBotMargen = 400 - pictureHeight;
                pictureIn.style.marginTop = topBotMargen / 2 + "px";
                pictureIn.style.marginBottom = topBotMargen / 2 + "px";
            }
            else {
                pictureHeight = pictureHeight * 400 / pictureWidth;
                var topBotMargen = 400 - pictureHeight;
                pictureIn.style.marginTop = topBotMargen / 2 + "px";
                pictureIn.style.marginBottom = topBotMargen / 2 + "px";
            }
        }
        else {
            pictureIn.style.width = "400px";
            pictureIn.style.height = "400px";
        }
    }
}

function picturesListForSize(numberOfPictures){
    var i;
    for (i = 0; i < numberOfPictures; i++){
        defaultPictureSize(document.getElementById("carouselPictureId"+i));
    }
}

module.exports = {
    defaultPictureSize: function (pictureIn) {
        defaultPictureSize(pictureIn);
    },
    picturesListForSize: function (numberOfPictures) {
        picturesListForSize(numberOfPictures);
    }
};


