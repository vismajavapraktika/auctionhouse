function changeStartingPriceStepsAuctionCreationMain() {
    var currencyType = $("#changeOnType").val();
    var tempStartingPriceValue = $('#startingPrice').val();
    var tempBuyoutValue = $('#buyOutPrice').val();

    if (document.getElementById("startingPrice") != null) {
        document.getElementById("startingPrice").removeAttribute("value");
        document.getElementById("startingPrice").value = tempStartingPriceValue;
    }
    if (document.getElementById("buyOutPrice") != null) {
        document.getElementById("buyOutPrice").removeAttribute("value");
        document.getElementById("buyOutPrice").value = tempBuyoutValue;
    }
    if (document.getElementById("startingPrice") != null) {
        if (currencyType === "KUDOS") {
            document.getElementById('startingPrice').step = '1';
            document.getElementById('buyOutPrice').step = '1';
        } else {
            document.getElementById('startingPrice').step = '0.01';
            if (document.getElementById('buyOutPrice') != null)
                document.getElementById('buyOutPrice').step = '0.01';
        }
    }
}

var calculateTotalPrice = function () {
    if (document.getElementById("totalPrice") != null)
    document.getElementById("totalPrice").value = (document
        .getElementById("startingPrice").value * 1 + document
        .getElementById("startingPrice").value
    * ((document.getElementById("itemQuantity").value == '' ? 1 : document.getElementById("itemQuantity").value) - 1))
};

function showOrHideBuyoutDependingOnQuantity() {
    var currencyType = $("#changeOnType").val();
    if ($('#allowToBidForQuantityInput').prop('checked')) {
        $('#buyOutForm').hide();
        $('#allowBuyOutRow').hide();
        $('#allowBuyOut').attr("checked", false);
        $('#buyOutPrice').val('');
    } else {
        if (currencyType === "KUDOS") {
            $('#buyOutForm').hide();
            $('#allowBuyOutRow').hide();
            $('#allowBuyOut').attr("checked", false);
            $('#buyOutPrice').val('');
        } else {
            $('#allowBuyOutRow').show();
            $('#buyOutForm').hide();
        }
    }
}

function showOrHideBuyoutDependingOnCurrency() {
    var currencyType = $("#changeOnType").val();

    if (currencyType === "KUDOS") {
        $('#buyOutForm').hide();
        $('#allowBuyOutRow').hide();
        $('#allowBuyOut').attr("checked", false);
    }

    if (currencyType === "EUR") {
        $('#buyOutForm').show();
        $('#allowBuyOutRow').show();
        showOrHideBuyoutDependingOnQuantity();
    }
    $('#buyOutPrice').val('');
}

function allowToBidForQuantityChanged() {
    if ($('#allowToBidForQuantityInput').prop('checked')) {
        $('#ItemQuantityRow').show();
    } else {
        if (document.getElementById("itemQuantity") != null) {
            $('#ItemQuantityRow').hide();
            document.getElementById("itemQuantity").value = '';
            calculateTotalPrice();
        }
    }
}

function allowBuyOutChanged() {
    if ($('#allowBuyOut').prop('checked')) {
        $('#buyOutForm').show();
    }
    else {
        $('#buyOutForm').hide();
        $('#buyOutPrice').val(null);
    }
}

function initBuyout() {
    var buyOutInput = $('#allowBuyOut');

    allowBuyOutChanged();

    buyOutInput.on('change', function () {
        allowBuyOutChanged();
    });
}

module.exports = {
    changeStartingPriceStepsAuction: function () {
        changeStartingPriceStepsAuctionCreationMain();
    },
    showOrHideBuyoutDependingOnCurrency: function () {
        showOrHideBuyoutDependingOnCurrency();
    },
    showOrHideBuyoutDependingOnQuantity: function () {
        showOrHideBuyoutDependingOnQuantity()
    },
    allowToBidForQuantityChanged: function () {
        allowToBidForQuantityChanged();
    },
    allowBuyOutChanged: function () {
        allowBuyOutChanged();
    },
    initBuyout: function () {
        initBuyout();
    },
    calculateTotalPrice: function () {
        calculateTotalPrice();
    }
};