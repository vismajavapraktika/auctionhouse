function changeStartingPriceSteps() {
    var currencyType = $("#changeOnType").val();
    var tempStartingPriceValue = $('#startingPriceSteps').val();

    if (document.getElementById("startingPriceSteps") != null) {
        document.getElementById("startingPriceSteps").removeAttribute("value");
        document.getElementById("startingPriceSteps").value = tempStartingPriceValue;
    }
    if (document.getElementById('startingPriceSteps') != null) {
        if (currencyType === "KUDOS") {
            document.getElementById('startingPriceSteps').step = '1';
        } else {
            document.getElementById('startingPriceSteps').step = '0.01';
        }
    }
}
function changeEndDate() {
    document.getElementById("enddate").type = "text";
    document.getElementById("enddate").removeAttribute("readonly");
    document.getElementById("changeEndDateButton").hidden = "true";
    document.getElementById("acceptChangeEndDate").removeAttribute("hidden");
    document.getElementById("uselessButton").style.visibility = "visible";
}
function acceptChangeEndDate() {
    document.getElementById("changeEndDateButton").removeAttribute("hidden");
    document.getElementById("acceptChangeEndDate").hidden = "true";
    document.getElementById("enddate").readOnly = "true";
    document.getElementById("uselessButton").style.visibility = "hidden";
}

module.exports = {
    changeStartingPriceSteps: function () {
        changeStartingPriceSteps();
    },
    changeEndDate: function () {
        changeEndDate();
    },
    acceptChangeEndDate: function () {
        acceptChangeEndDate();
    },
};

