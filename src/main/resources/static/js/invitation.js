function addNewEmail() {
    var lastInputField = $('#all-emails input').not(':button').last();
    if (lastInputField.val() == '')
        return;
    if(!validateEmail(lastInputField.val()))
    {
    	alert('Not a valid email address');
    	return;
    }
    if ($("#invited-emails ul li[text='" + lastInputField.val() + "']").length > 0) {
        alert('email already exists');
        return;
    }
    var input = document.createElement("input");
    input.type = "email";
    $(input).insertAfter(lastInputField);

    createInputDeleteLink(lastInputField);

    lastInputField.hide();
    $("#invited-emails ul").val($(lastInputField).val());
    $('#all-emails input').not(':button').each(function (index) {
        $(this).attr('name', 'invitations[' + index + '].email');
    });
}

function createInputDeleteLink(lastInputField) {

    var a = document.createElement("a");
    a.title = 'delete';
    a.text = 'x';
    var li = document.createElement("li");
    $(li).text(lastInputField.val());
    $(li).append(a);
    $("#invited-emails ul").prepend($(li))

   $(a).on('click', function () {
        $(li).remove();
        $('#all-emails :input').get(0).style.display = 'none';
        $(lastInputField).remove();
        $('#all-emails input').not(':button').each(function (index) {
            $(this).attr('name', 'invitations[' + index + '].email');
        });
    });

    return li;
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function removeFromList() {
    document.getElementById("invitations").innerHTML = "";
    $('#all-emails input').not(':last').remove();
    $('#all-emails :input').each(function (index) {
        $(this).attr('name', 'invitations[' + index + ']');
    });
    document.getElementById("invited-emails").style.visibility="hidden";
}

function hideInvList() {
    var invitedEmails = document.getElementById("invited-emails");
    invitedEmails.style.visibility = 'hidden';
}

function showInvList() {
    if ($('#invitations').val()) {
        var invitedEmails = document.getElementById("invited-emails");
        invitedEmails.style.visibility = 'visible';
    }
}


module.exports = {
    addNewEmail: function () {
        addNewEmail();
    },
    createInputDeleteLink: function (lastInputField) {
        createInputDeleteLink(lastInputField)
    },
    removeFromList: function () {
        removeFromList();
    },
    hideInvList: function () {
        hideInvList();
    },
    showInvList: function () {
        showInvList();
    },
};