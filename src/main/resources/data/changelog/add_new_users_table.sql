CREATE TABLE public.test_user
			(
			id INT,
			name VARCHAR(20),
			surname VARCHAR(20),
			email VARCHAR(50) PRIMARY KEY,
			phone INT,
			password VARCHAR(60),
			enabled BOOLEAN,
			user_role VARCHAR(45),
			user_type VARCHAR(50)
			);

CREATE UNIQUE INDEX test_user_email_uindex ON public.test_user (email);
