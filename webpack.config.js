const path = require('path');
const webpack = require('webpack');

const sourcePath = path.join(__dirname, './');
const staticsPath = path.join(__dirname, './src\\main\\resources\\static\\dist');

module.exports = function (env) {
    const nodeEnv = env && env.prod ? 'production' : 'development';
    const isProd = nodeEnv == 'production';

    const plugins = [
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendor',
            minChunks: 2,
            filename: 'bundle.js'
        }),
        new webpack.NamedModulesPlugin(),
    ];

    if (isProd){
        plugins.push(
            new webpack.LoaderOptionsPlugin({
                minimize: true,
                debug: false,
            }),
            new webpack.optimize.UglifyJsPlugin({
                compress: {
                    warnings: false,
                    screw_ie8: true,
                    conditionals: true,
                    unused: true,
                    comparisons: true,
                    sequences: true,
                    dead_code: true,
                    evaluate: true,
                    if_return: true,
                    join_vars: true,
                },
                output: {
                    comments: false,
                },
            })
        )
    } else {
        plugins.push(
            new webpack.HotModuleReplacementPlugin()
        );
    }
    return {
        devtool: isProd ? 'source-map' : 'source-map',
        context: sourcePath,
        entry: {
            header: './src\\main\\resources\\static\\webpack-js\\header.js',
            pictureDefaultSize: './src\\main\\resources\\static\\js\\pictureDefaultSize.js',
            auctionViewJS: './src\\main\\resources\\static\\js\\auction-view.js',
            auctionTimer: './src\\main\\resources\\static\\js\\auctionTimer.js',
            addRemovePicture: './src\\main\\resources\\static\\js\\addRemovePicture.js',
            auctionCreateMain: './src\\main\\resources\\static\\js\\auctionCreationMain.js',
            anonymousAuctionCreateMain: './src\\main\\resources\\static\\js\\anonymous-auction-create-main.js',
            invitation: './src\\main\\resources\\static\\js\\invitation.js',
            notification: './src\\main\\resources\\static\\js\\notifications.js',
            editAuctionMain: './src\\main\\resources\\static\\js\\editAuctionMain',
        },
        output: {
            path: staticsPath,
            filename: '[name].bundle.js',
            // chunkFilename: "[name]_[chunkhash:20].js",
            libraryTarget: 'var',
            library: '[name]',
        },
        module: {
            rules: [
                {
                    test: /\.html$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'file-loader',
                        query: {
                            name: '[name].[ext]'
                        },
                    },
                },
                {
                    test: /\.css$/,
                    exclude: /node_modules/,
                    loader: 'style-loader!css-loader'
                },
                {
                    test: /\.(png|gif)$/,
                    loader: 'file-loader'
                },
                {
                    test: /\.woff$/,
                    loader: 'url-loader?limit=65000&mimetype=application/font-woff'
                },
                {
                    test: /\.woff2$/,
                    loader: 'url-loader?limit=65000&mimetype=application/font-woff2'
                },
                {
                    test: /\.[ot]tf$/,
                    loader: 'url-loader?limit=65000&mimetype=application/octet-stream'
                },
                {
                    test: /\.eot$/,
                    loader: 'url-loader?limit=65000&mimetype=application/vnd.ms-fontobject'
                },
                {
                    test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                    loader: "url-loader?limit=1000000&mimetype=image/svg+xml"
                },
                {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: [
                        'babel-loader?presets[]=es2015'
                    ],
                },
            ],
        },
        resolve: {
            extensions: ['.webpack-loader.js', '.web-loader.js', '.loader.js', '.js', '.jsx'],
            modules: [
                path.resolve(__dirname, 'node_modules'),
                sourcePath
            ]
        },

        externals:[{
            "jquery": "jQuery"},
            require('webpack-require-http'),
            // require("font-awesome-webpack!./font-awesome.config"),
        ],

        plugins,

        performance: isProd && {
            maxAssetSize: 10000,
            maxEntrypointSize: 30000,
            hints: 'warning',
        },

        devServer: {
            contentBase: './client',
            historyApiFallback: true,
            port: 8080,
            compress: isProd,
            inline: !isProd,
            hot: !isProd,
            stats: {
                assets: true,
                children: false,
                chunks: false,
                hash: false,
                modules: false,
                publicPath: false,
                timings: true,
                version: false,
                warnings: true,
                colors: {
                    green: '\u001b[32m',
                }
            },
        }
    };
};
