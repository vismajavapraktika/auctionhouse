# Visma Auction #

Auction house created by Visma.

### What is this repository for? ###

* Version Release.1.9.1

### How do I get set up? ###

#### Summary of set up
* Download and install `node.js`
* Run `npm init` (in terminal in application directory)
* Install needed components (`webpack`, its loaders and so on). You can do it by running: 
```
npm i webpack babel-core babel-loader babel-preset-es2015 
babel-preset-stage-0 buble-loader compression-webpack-plugin 
cross-env css-loader fallback-loader file-loader font-awesome-webpack 
html-webpack-plugin image-webpack-loader react-svg-loader rimraf 
style-loader svg-loader svgo svgo-loader url-loader webpack-dev-server 
webpack-require-http webpack-svgstore-plugin --save-dev
```
* Create `Postgresql` database:
    * url: `jdbc:postgresql://localhost:5432/SeLi DB`
    * username: `postgres`
    * password: `postgres`
* All dependencies are in `pom.xml`

### Contribution guidelines ###

* Writing tests
* Code review

### Who do I talk to? ###

* Repo owner or admin
Lukas.Karmanovas@visma.com
* Other community or team contact
Justas.Rutkauskas@visma.com