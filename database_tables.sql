﻿CREATE TABLE all_users
(
  id INTEGER DEFAULT nextval('all_users_id_seq'::regclass) PRIMARY KEY NOT NULL,
  type VARCHAR(20) NOT NULL
);
CREATE TABLE anonymous_user
(
  id INTEGER DEFAULT nextval('anonymous_user_id_seq'::regclass) NOT NULL,
  phone VARCHAR(8),
  user_email VARCHAR(50) NOT NULL
);
CREATE TABLE auction_pictures
(
  auction_id INTEGER NOT NULL,
  photo_name VARCHAR(150) NOT NULL,
  bytes BYTEA NOT NULL,
  id INTEGER DEFAULT nextval('auction_pictures_id_seq'::regclass) NOT NULL
);
CREATE TABLE auctions
(
  id INTEGER DEFAULT nextval('auctions_id_seq'::regclass) PRIMARY KEY NOT NULL,
  title VARCHAR(50) NOT NULL,
  type VARCHAR(10) NOT NULL,
  description TEXT NOT NULL,
  start_time TIMESTAMP NOT NULL,
  end_time TIMESTAMP NOT NULL,
  quantity INTEGER DEFAULT 1,
  starting_price NUMERIC(10,2) NOT NULL,
  currency VARCHAR(5) NOT NULL,
  status VARCHAR(15) NOT NULL,
  owner_id INTEGER NOT NULL,
  allow_bid_for_quantity BOOLEAN,
  buy_out_price NUMERIC(10,2) NOT NULL,
  uuid VARCHAR(36),
  quantity_left INTEGER DEFAULT 0 NOT NULL,
  link VARCHAR(500),
  is_anonymous BOOLEAN
);
CREATE TABLE bid
(
  id INTEGER DEFAULT nextval('bid_id_seq'::regclass) PRIMARY KEY NOT NULL,
  auction_id INTEGER,
  user_id INTEGER,
  amount NUMERIC(10,2),
  quantity INTEGER
);
CREATE TABLE bids_winners
(
  id INTEGER DEFAULT nextval('bids_winners_id_seq'::regclass) PRIMARY KEY NOT NULL,
  auction_id INTEGER,
  user_id INTEGER,
  amount NUMERIC(10,2),
  quantity INTEGER,
  expiration_time TIMESTAMP,
  message_status VARCHAR(15)
);
CREATE TABLE comments
(
  id INTEGER DEFAULT nextval('comments_id_seq'::regclass) PRIMARY KEY NOT NULL,
  auction_id INTEGER,
  user_id INTEGER,
  text TEXT NOT NULL,
  created TIMESTAMP NOT NULL,
  name VARCHAR(50) NOT NULL
);
CREATE TABLE google_users
(
  id INTEGER PRIMARY KEY NOT NULL,
  name VARCHAR(20) NOT NULL,
  surname VARCHAR(20) NOT NULL,
  email VARCHAR(50) NOT NULL,
  phone VARCHAR(8),
  enabled BOOLEAN,
  user_role VARCHAR(45) DEFAULT 'USER'::character varying NOT NULL
);
CREATE TABLE invitations
(
  id INTEGER DEFAULT nextval('invitations_id_seq'::regclass) NOT NULL,
  auction_id INTEGER,
  email VARCHAR(100) NOT NULL,
  email_sent BOOLEAN DEFAULT false,
  status VARCHAR(20) NOT NULL
);
CREATE TABLE invitations_anonymous
(
  id INTEGER DEFAULT nextval('invitations_anonymous_id_seq'::regclass) NOT NULL,
  auction_id INTEGER NOT NULL,
  invited_email VARCHAR(45) NOT NULL,
  uuid VARCHAR(36),
  user_id INTEGER,
  notified_about_auction_end BOOLEAN
);
CREATE TABLE notifications
(
  id INTEGER DEFAULT nextval('notifications_id_seq'::regclass) PRIMARY KEY NOT NULL,
  type VARCHAR(20) NOT NULL,
  email VARCHAR(50),
  new_notification BOOLEAN NOT NULL,
  auction_id INTEGER
);
CREATE TABLE notified_anonymous_auction_owners
(
  user_id INTEGER NOT NULL,
  auction_id INTEGER PRIMARY KEY NOT NULL
);
CREATE TABLE temporary_photos
(
  bytes BYTEA NOT NULL,
  user_id INTEGER NOT NULL,
  photo_uuid UUID NOT NULL,
  picture_name VARCHAR(500),
  id INTEGER DEFAULT nextval('temporary_photos_id_seq'::regclass) NOT NULL,
  insert_date TIMESTAMP
);
CREATE TABLE user_rating
(
  id INTEGER DEFAULT nextval('user_rating_id_seq'::regclass) PRIMARY KEY NOT NULL,
  rater_id INTEGER,
  ratee_id INTEGER,
  rating NUMERIC(10,1),
  auction_id INTEGER
);
CREATE TABLE users
(
  id INTEGER PRIMARY KEY NOT NULL,
  name VARCHAR(20) NOT NULL,
  surname VARCHAR(20) NOT NULL,
  email VARCHAR(50) NOT NULL,
  phone VARCHAR(8),
  password VARCHAR(60) NOT NULL,
  enabled BOOLEAN,
  user_role VARCHAR(45) DEFAULT 'USER'::character varying NOT NULL
);
ALTER TABLE anonymous_user ADD FOREIGN KEY (id) REFERENCES all_users (id);
CREATE UNIQUE INDEX anonymous_user_id_key ON anonymous_user (id);
CREATE UNIQUE INDEX anonymous_user_user_email_key ON anonymous_user (user_email);
ALTER TABLE auction_pictures ADD FOREIGN KEY (auction_id) REFERENCES auctions (id);
ALTER TABLE bid ADD FOREIGN KEY (auction_id) REFERENCES auctions (id);
ALTER TABLE bid ADD FOREIGN KEY (user_id) REFERENCES all_users (id);
ALTER TABLE bids_winners ADD FOREIGN KEY (auction_id) REFERENCES auctions (id);
ALTER TABLE bids_winners ADD FOREIGN KEY (user_id) REFERENCES all_users (id);
ALTER TABLE comments ADD FOREIGN KEY (auction_id) REFERENCES auctions (id);
ALTER TABLE google_users ADD FOREIGN KEY (id) REFERENCES all_users (id);
CREATE UNIQUE INDEX google_users_email_key ON google_users (email);
ALTER TABLE invitations ADD FOREIGN KEY (auction_id) REFERENCES auctions (id);
ALTER TABLE invitations_anonymous ADD FOREIGN KEY (auction_id) REFERENCES auctions (id);
ALTER TABLE invitations_anonymous ADD FOREIGN KEY (user_id) REFERENCES all_users (id);
ALTER TABLE notifications ADD FOREIGN KEY (auction_id) REFERENCES auctions (id);
ALTER TABLE notified_anonymous_auction_owners ADD FOREIGN KEY (user_id) REFERENCES anonymous_user (id);
ALTER TABLE notified_anonymous_auction_owners ADD FOREIGN KEY (auction_id) REFERENCES auctions (id);
ALTER TABLE temporary_photos ADD FOREIGN KEY (user_id) REFERENCES all_users (id);
CREATE UNIQUE INDEX photo_uuid_unique ON temporary_photos (photo_uuid);
ALTER TABLE user_rating ADD FOREIGN KEY (rater_id) REFERENCES all_users (id);
ALTER TABLE user_rating ADD FOREIGN KEY (ratee_id) REFERENCES all_users (id);
ALTER TABLE user_rating ADD FOREIGN KEY (auction_id) REFERENCES auctions (id);
ALTER TABLE users ADD FOREIGN KEY (id) REFERENCES all_users (id);
CREATE UNIQUE INDEX users_email_key ON users (email);


-- Alter notification table to have insert_date
ALTER TABLE public.notifications ADD insert_date TIMESTAMP NOT NULL;

-- Alter invitation table to have insert_date
ALTER TABLE public.invitations ADD insert_date TIMESTAMP NOT NULL;